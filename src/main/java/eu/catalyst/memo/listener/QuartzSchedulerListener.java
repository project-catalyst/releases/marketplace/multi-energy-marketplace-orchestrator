package eu.catalyst.memo.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import eu.catalyst.memo.utilities.Utilities;
import eu.catalyst.memo.quartz.SchedulerJobBillingFlexMarketSession;
import eu.catalyst.memo.quartz.SchedulerJobClearFlexMarketSession;
import eu.catalyst.memo.quartz.SchedulerJobCompleteFlexMarketSession;
import eu.catalyst.memo.quartz.SchedulerJobEndAllMarketSession;
import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.model.SystemParameter;
import eu.catalyst.memo.quartz.SchedulerJobStartAllMarketSession;
import eu.catalyst.memo.jpa.SystemParameterFacade;

public class QuartzSchedulerListener implements ServletContextListener {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(QuartzSchedulerListener.class);
	private static final long sleepTime = 5000;

	public void contextDestroyed(ServletContextEvent arg0) {
		//
	}

	public void contextInitialized(ServletContextEvent arg0) {

		TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
		TimeZone.setDefault(utcTimeZone);

		// execute Login operation to retrieve Token for next Rest Calls
		executeLogin();

		JobDetail jobStartAllMarketSession = JobBuilder.newJob(SchedulerJobStartAllMarketSession.class)
				.withIdentity("jobStartAllMarketSession", "group1").build();

		JobDetail jobEndAllMarketSession = JobBuilder.newJob(SchedulerJobEndAllMarketSession.class)
				.withIdentity("jobEndAllMarketSession", "group1").build();

		JobDetail jobClearFlexMarketSession = JobBuilder.newJob(SchedulerJobClearFlexMarketSession.class)
				.withIdentity("jobClearFlexMarketSession", "group1").build();

		JobDetail jobCompleteFlexMarketSession = JobBuilder.newJob(SchedulerJobCompleteFlexMarketSession.class)
				.withIdentity("jobCompleteFlexMarketSession", "group1").build();

		JobDetail jobBillingFlexMarketSession = JobBuilder.newJob(SchedulerJobBillingFlexMarketSession.class)
				.withIdentity("jobBillingFlexMarketSession", "group1").build();

		try {

			Trigger triggerStartAllMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerStartAllMarketSession", "group1")
					.withSchedule(CronScheduleBuilder.cronSchedule("5 * * * * ?")).build();

			Trigger triggerEndAllMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerEndAllMarketSession", "group1").withSchedule(
							// CronScheduleBuilder.cronSchedule("0/40 * * * * ?"))
							CronScheduleBuilder.cronSchedule("15 * * * * ?"))
					// CronScheduleBuilder.cronSchedule("0 15 * * * ?"))
					.build();

			Trigger triggerClearFlexMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerClearFlexMarketSession", "group1").withSchedule(
							// CronScheduleBuilder.cronSchedule("0/50 * * * * ?"))
							CronScheduleBuilder.cronSchedule("25  * * * * ?"))
					// CronScheduleBuilder.cronSchedule("0 25 * * * ?"))
					.build();

//			Properties prop = new Properties();
//			try {
//				prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
//			} catch (IOException e) {
//				e.printStackTrace();
//			}

			//String FlexSessionsCompleteTime = prop.getProperty("FlexSessionsCompleteTime");
			//Get Parameters from SystemParameter Table
			String FlexSessionsCompleteTime = SystemParameterFacade.findEntityByName(Utilities.PARAM_FLEX_SESSION_COMPLETE_TIME).getValue();
			
			
			String FlexSessionsCompleteTimeHH = FlexSessionsCompleteTime.substring(0, 2);
			String FlexSessionsCompleteTimeMM = FlexSessionsCompleteTime.substring(3);
			String cronScheduleCompleteFlexMarketSession = "0 " + FlexSessionsCompleteTimeMM + " "
					+ FlexSessionsCompleteTimeHH + " * * ?";
			log.debug("cronScheduleCompleteAllMarketSession: " + cronScheduleCompleteFlexMarketSession);

			Trigger triggerCompleteFlexMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerCompleteFlexMarketSession", "group1")
					.withSchedule(CronScheduleBuilder.cronSchedule(cronScheduleCompleteFlexMarketSession))
					// CronScheduleBuilder.cronSchedule("0 59 23 * * ?"))
					// CronScheduleBuilder.cronSchedule("0 10 10 * * ?"))
					.build();

			//String FlexSessionsBillingTime = prop.getProperty("FlexSessionsBillingTime");
			//Get Parameters from SystemParameter Table
			String FlexSessionsBillingTime = SystemParameterFacade.findEntityByName(Utilities.PARAM_FLEX_SESSION_BILLING_TIME).getValue();
			
			String FlexSessionsBillingTimeHH = FlexSessionsBillingTime.substring(0, 2);
			String FlexSessionsBillingTimeMM = FlexSessionsBillingTime.substring(3);
			String cronScheduleBillingFlexMarketSession = "0 " + FlexSessionsBillingTimeMM + " "
					+ FlexSessionsBillingTimeHH + " * * ?";
			log.debug("cronScheduleBillingFlexMarketSession: " + cronScheduleBillingFlexMarketSession);

			Trigger triggerBillingFlexMarketSession = TriggerBuilder.newTrigger()
					.withIdentity("triggerBillingFlexMarketSession", "group1")
					.withSchedule(CronScheduleBuilder.cronSchedule(cronScheduleBillingFlexMarketSession))
					// CronScheduleBuilder.cronSchedule("0 59 23 * * ?"))
					// CronScheduleBuilder.cronSchedule("0 10 10 * * ?"))
					.build();

			Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(jobStartAllMarketSession, triggerStartAllMarketSession);
			scheduler.scheduleJob(jobEndAllMarketSession, triggerEndAllMarketSession);
			scheduler.scheduleJob(jobClearFlexMarketSession, triggerClearFlexMarketSession);
			scheduler.scheduleJob(jobCompleteFlexMarketSession, triggerCompleteFlexMarketSession);
			scheduler.scheduleJob(jobBillingFlexMarketSession, triggerBillingFlexMarketSession);

		} catch (SchedulerException e) {
			e.printStackTrace();
		}

	}

	private void executeLogin() {
		log.debug("-> executeLogin");

		Utilities.setTokenMEMOComponents();
		Utilities.setTokenMCMMBMComponents(Utilities.MCM_COMPONENT_NAME);
		Utilities.setTokenMCMMBMComponents(Utilities.MBM_COMPONENT_NAME);

		log.debug("<- executeLogin");
	}

//	private void setTokenMEMOComponents() {
//		log.debug("-> setTokenMEMOComponents");
//
//		List<MarketplaceComponent> myMarketplaceComponentList = new ArrayList<MarketplaceComponent>();
//		myMarketplaceComponentList = MarketplaceComponentFacade.findEntitiesByComponent(Utilities.IB_COMPONENT_NAME);
//
//		for (MarketplaceComponent myMarketplaceComponent : myMarketplaceComponentList) {
//
//			// retrieve USER and PASSWORD for request payload
//			String credentialUser = myMarketplaceComponent.getUsername();
//			String credentialPassword = myMarketplaceComponent.getPassword();
//
//			// create request payload
//			JsonObject jsonRequestPayload = new JsonObject();
//			jsonRequestPayload.addProperty("username", credentialUser);
//			jsonRequestPayload.addProperty("password", credentialPassword);
//
//			// execute Restful Call POST "/tokens/ to informationbroker
//			String responsePayload = "";
//
//			// Repeats until the service responds correctly
//
//			boolean repeat_call_service = true;
//
//			do {
//				try {
//
//					String informationBrokerServerUrl = myMarketplaceComponent.getUrl();
//					URL url = new URL(informationBrokerServerUrl + Utilities.PATH_POST_TOKENS);
//					log.debug("Rest url: " + url.toString());
//
//					HttpURLConnection connService = (HttpURLConnection) url.openConnection();
//					connService.setDoOutput(true);
//					connService.setRequestMethod("POST");
//					connService.setRequestProperty("Content-Type", "application/json");
//					connService.setRequestProperty("Accept", "application/json");
//
//					String requestPayload = jsonRequestPayload.toString();
//					log.debug("requestPayload: " + requestPayload);
//
//					OutputStream os = connService.getOutputStream();
//					os.write(requestPayload.getBytes());
//					os.flush();
//
//					if (connService.getResponseCode() == HttpURLConnection.HTTP_OK) {
//						repeat_call_service = false;
//					}
//
//					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
//
//					responsePayload = br.readLine();
//
//					log.debug("responsePayload: " + responsePayload);
//
//					br.close();
//					connService.disconnect();
//				} catch (ConnectException ex) {
//					log.error("ConnectException : " + ex);
//					repeat_call_service = true;
//					try {
//						Thread.sleep(sleepTime);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					// ex.printStackTrace();
//				} catch (Exception ex) {
//					log.error("Exception : " + ex);
//					ex.printStackTrace();
//				}
//
//			} while (repeat_call_service);
//
//			// parsing JSON Response payload to extract "token" property
//			JsonObject responseJson = (JsonObject) new JsonParser().parse(responsePayload);
//			String responseToken = responseJson.get("token").toString().replace("\"", "");
//			log.debug("responseToken: " + responseToken);
//
//			Utilities.putToken(myMarketplaceComponent.getType(), myMarketplaceComponent.getForm(),
//					Utilities.MEMO_COMPONENT_NAME, responseToken);
//		}
//
//		log.debug("<- setTokenMEMOComponents");
//	}
//
//	private void setTokenMCMMBMComponents(String component) {
//		log.debug("-> setTokenMCMMBMComponents");
//		List<MarketplaceComponent> myMarketplaceComponentList = new ArrayList<MarketplaceComponent>();
//		myMarketplaceComponentList = MarketplaceComponentFacade.findEntitiesByComponent(component);
//
//		for (MarketplaceComponent myMarketplaceComponent : myMarketplaceComponentList) {
//
//			// retrieve USER and PASSWORD for request payload
//			String credentialUser = myMarketplaceComponent.getUsername();
//			String credentialPassword = myMarketplaceComponent.getPassword();
//
//			// create request payload
//			JsonObject jsonRequestPayload = new JsonObject();
//			jsonRequestPayload.addProperty("username", credentialUser);
//			jsonRequestPayload.addProperty("password", credentialPassword);
//
//			// execute Restful Call POST "/tokens/ to informationbroker
//			String responsePayload = "";
//
//			// Repeats until the service responds correctly
//
//			boolean repeat_call_service = true;
//
//			do {
//				try {
//
//					MarketplaceComponent myMarketplaceComponentIB = MarketplaceComponentFacade.getMarketplaceComponent(
//							myMarketplaceComponent.getType(), myMarketplaceComponent.getForm(),
//							Utilities.IB_COMPONENT_NAME);
//
//					String informationBrokerServerUrl = myMarketplaceComponentIB.getUrl();
//					URL url = new URL(informationBrokerServerUrl + Utilities.PATH_POST_TOKENS);
//					log.debug("Rest url: " + url.toString());
//
//					HttpURLConnection connService = (HttpURLConnection) url.openConnection();
//					connService.setDoOutput(true);
//					connService.setRequestMethod("POST");
//					connService.setRequestProperty("Content-Type", "application/json");
//					connService.setRequestProperty("Accept", "application/json");
//
//					String requestPayload = jsonRequestPayload.toString();
//					log.debug("requestPayload: " + requestPayload);
//
//					OutputStream os = connService.getOutputStream();
//					os.write(requestPayload.getBytes());
//					os.flush();
//
//					if (connService.getResponseCode() == HttpURLConnection.HTTP_OK) {
//						repeat_call_service = false;
//					}
//
//					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
//
//					responsePayload = br.readLine();
//
//					log.debug("responsePayload: " + responsePayload);
//
//					br.close();
//					connService.disconnect();
//				} catch (ConnectException ex) {
//					log.error("ConnectException : " + ex);
//					repeat_call_service = true;
//					try {
//						Thread.sleep(sleepTime);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					// ex.printStackTrace();
//				} catch (Exception ex) {
//					log.error("Exception : " + ex);
//					ex.printStackTrace();
//				}
//
//			} while (repeat_call_service);
//
//			// parsing JSON Response payload to extract "token" property
//			JsonObject responseJson = (JsonObject) new JsonParser().parse(responsePayload);
//			String responseToken = responseJson.get("token").toString().replace("\"", "");
//			log.debug("responseToken: " + responseToken);
//
//			Utilities.putToken(myMarketplaceComponent.getType(),
//					           myMarketplaceComponent.getForm(),
//					           component,
//					           responseToken);
//		}
//
//		log.debug("<- setTokenMCMMBMComponents");
//
//	}

//	private void executeLogin() {
//		log.debug("-> executeLogin");
//
//		// load config.properties
//		Properties prop = new Properties();
//		try {
//			prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		// retrieve PATH of Rest Service to call
////		String pathPostTokens = prop.getProperty("pathPostTokens");
////		log.debug("pathPostTokens: " + pathPostTokens);
//
//		// retrieve USER and PASSWORD for request payload
//		String credentialUser = prop.getProperty("credentialUser");
//		log.debug("credentialUser: " + credentialUser);
//		String credentialPassword = prop.getProperty("credentialPassword");
//		log.debug("credentialPassword: " + credentialPassword);
//
//		// create request payload
//		JsonObject jsonRequestPayload = new JsonObject();
//		jsonRequestPayload.addProperty("username", credentialUser);
//		jsonRequestPayload.addProperty("password", credentialPassword);
//
//		List<MarketplaceComponent> myMarketplaceComponentList = new ArrayList<MarketplaceComponent>();
//		myMarketplaceComponentList = MarketplaceComponentFacade.findEntitiesByComponent("IB");
//
//		for (MarketplaceComponent myMarketplaceComponent : myMarketplaceComponentList) {
//
//			// execute Restful Call POST "/tokens/ to informationbroker
//			String responsePayload = "";
//
//			// Repeats until the service responds correctly
//
//			boolean repeat_call_service = true;
//
//			do {
//				try {
//
//					String informationBrokerServerUrl = myMarketplaceComponent.getUrl();
//					URL url = new URL(informationBrokerServerUrl + Utilities.PATH_POST_TOKENS);
//					log.debug("Rest url: " + url.toString());
//
//					HttpURLConnection connService = (HttpURLConnection) url.openConnection();
//					connService.setDoOutput(true);
//					connService.setRequestMethod("POST");
//					connService.setRequestProperty("Content-Type", "application/json");
//					connService.setRequestProperty("Accept", "application/json");
//
//					String requestPayload = jsonRequestPayload.toString();
//					log.debug("requestPayload: " + requestPayload);
//
//					OutputStream os = connService.getOutputStream();
//					os.write(requestPayload.getBytes());
//					os.flush();
//
//					if (connService.getResponseCode() == HttpURLConnection.HTTP_OK) {
//						repeat_call_service = false;
//					}
//
//					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
//
//					responsePayload = br.readLine();
//
//					log.debug("responsePayload: " + responsePayload);
//
//					br.close();
//					connService.disconnect();
//				} catch (ConnectException ex) {
//					log.error("ConnectException : " + ex);
//					repeat_call_service = true;
//					try {
//						Thread.sleep(sleepTime);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					// ex.printStackTrace();
//				} catch (Exception ex) {
//					log.error("Exception : " + ex);
//					ex.printStackTrace();
//				}
//
//			} while (repeat_call_service);
//
//			// parsing JSON Response payload to extract "token" property
//			JsonObject responseJson = (JsonObject) new JsonParser().parse(responsePayload);
//			String responseToken = responseJson.get("token").toString().replace("\"", "");
//			log.debug("responseToken: " + responseToken);
//
//			Utilities.putTokenIB(myMarketplaceComponent.getType(), myMarketplaceComponent.getForm(), responseToken);
//		}
//
//		log.debug("<- executeLogin");
//	}

}
