package eu.catalyst.memo.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.payload.MarketplaceComponentOLD;

@Stateless
@Path("/userMarkets/{component}/{username}/")
public class Login {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Login.class);

	public Login() {

	}

	@GET
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public List<MarketplaceComponentOLD> getUserMarkets(@PathParam("component") String component, @PathParam("username") String username) {

		log.debug("-> getUserMarkets");
		log.debug("component: " + component);
		log.debug("username: " + username);

		List<MarketplaceComponentOLD> myMarketplaceComponentList = new ArrayList<MarketplaceComponentOLD>();
		myMarketplaceComponentList = MarketplaceComponentFacade.findEntitiesOldByComponentUsername(component, username);
		log.debug("<- getUserMarkets");
		return myMarketplaceComponentList;
	}

}
