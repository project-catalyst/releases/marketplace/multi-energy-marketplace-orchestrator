package eu.catalyst.memo.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import eu.catalyst.memo.jpa.ConstraintFacade;
import eu.catalyst.memo.jpa.CorrelationFacade;
import eu.catalyst.memo.jpa.MarketactionCorrelationFacade;
import eu.catalyst.memo.model.Constraint;
import eu.catalyst.memo.model.Correlation;
import eu.catalyst.memo.model.MarketactionCorrelation;
import eu.catalyst.memo.payload.CorrelationActionNEW;
import eu.catalyst.memo.payload.CorrelationActionOLD;
import eu.catalyst.memo.payload.CorrelationListResponseGET;
import eu.catalyst.memo.payload.CorrelationRequestPOST;
import eu.catalyst.memo.payload.CorrelationRequestPUTDELETE;
import eu.catalyst.memo.payload.CorrelationResponseGET;
import eu.catalyst.memo.payload.UsernameRequest;

@Stateless
@Path("/constraints/")
public class ConstraintREST {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ConstraintREST.class);

	public ConstraintREST() {

	}

	@GET
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public List<Constraint> getConstraints() {

		log.debug("-> getConstraints");
		List<Constraint> myConstraintList = new ArrayList<Constraint>();
		myConstraintList = ConstraintFacade.findAllEntities();

		log.debug("<- getConstraints");
		return myConstraintList;
	}

}
