package eu.catalyst.memo.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;

import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.jpa.MarketplaceParticipantFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.model.MarketplaceParticipant;
import eu.catalyst.memo.payload.ActionStatus;
import eu.catalyst.memo.payload.ActionType;
import eu.catalyst.memo.payload.ActiveMarketActionItemGET;
import eu.catalyst.memo.payload.ActiveMarketActionItemRequestPOST;
import eu.catalyst.memo.payload.ActiveMarketActionItemResponsePOST;
import eu.catalyst.memo.payload.MarketAction;
import eu.catalyst.memo.payload.MarketActionCounterOffer;
import eu.catalyst.memo.payload.MarketResultsItem;
import eu.catalyst.memo.payload.MarketSession;
import eu.catalyst.memo.utilities.Utilities;

@Stateless
@Path("/marketResults/")
public class MarketResults {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MarketResults.class);

	public MarketResults() {

	}

	@GET
	@Path("{marketplaceForm}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public List<MarketResultsItem> getMarketResults(@PathParam("marketplaceForm") String marketplaceForm,
			@PathParam("timeframe") String timeframe) {

		log.debug("-> getMarketResults");
		log.debug("marketplaceForm:" + marketplaceForm);
		log.debug("timeframe:" + timeframe);

		List<MarketResultsItem> myMarketResultsItemList = new ArrayList<MarketResultsItem>();

		String form = Utilities.normalizeForm(marketplaceForm);

		// estrai URL del MarketplaceComponent
		List<MarketplaceComponent> myMarketplaceComponentListIB = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentListIB = MarketplaceComponentFacade.findEntitiesByFormComponent(form,
				Utilities.IB_COMPONENT_NAME);

		if (!myMarketplaceComponentListIB.isEmpty()) {
			String ibURL = myMarketplaceComponentListIB.get(0).getUrl();
			String type = myMarketplaceComponentListIB.get(0).getType();
			// estrai token relativo all IB
			String ibToken = Utilities.getToken(type, form, Utilities.MEMO_COMPONENT_NAME);

			try {
				int marketplaceId = Utilities.invokeGetMarketplaceByFormTimeframe(ibURL, ibToken, marketplaceForm,
						timeframe);

				// get all session of marketplaceId sorted by id
				// extract last item (verify the it is completed)

				// get all market actions of marketplaceId and sessionId
				// get all counteroffers of of marketplaceId and sessionId

				List<MarketSession> myAllSessionsList = Utilities.invokeGetAllSessions(ibURL, ibToken, marketplaceId);

				int lastSessionIndex = myAllSessionsList.size();
				if (lastSessionIndex > 0) {
					MarketSession lastMarketSession = myAllSessionsList.get(lastSessionIndex - 1);
					if (lastMarketSession.getSessionStatusid().getStatus().equals(Utilities.COMPLETED)) {
						Integer lastMarketSessionId = lastMarketSession.getId();

						List<MarketAction> myMarketActionList = Utilities.invokeGetMarketActionsByMarketPlaceSessionId(
								ibURL, ibToken, marketplaceId, lastMarketSessionId);

						List<MarketActionCounterOffer> myMarketActionCounterOfferList = Utilities
								.invokeGetMarketActionCounterOfferByMarketPlaceSessionId(ibURL, ibToken, marketplaceId,
										lastMarketSessionId);

						for (MarketAction myMarketAction : myMarketActionList) {

							MarketResultsItem myMarketResultsItem = new MarketResultsItem();

							myMarketResultsItem.setId(myMarketAction.getId());
							myMarketResultsItem.setActionStartTime(myMarketAction.getActionStartTime());
							myMarketResultsItem.setActionEndTime(myMarketAction.getActionEndTime());
							myMarketResultsItem.setValue(myMarketAction.getValue());
							myMarketResultsItem.setUom(myMarketAction.getUom());
							myMarketResultsItem.setPrice(myMarketAction.getPrice());
							myMarketResultsItem.setDeliveryPoint(myMarketAction.getDeliveryPoint());

							List<ActionType> myActionTypeList = Utilities.invokeGetActionTypeList(ibURL, ibToken);
							for (ActionType myActionType : myActionTypeList) {
								if (myActionType.getId() == myMarketAction.getActionTypeid()) {
									myMarketResultsItem.setActionType(myActionType.getType());
								}
							}

							ActionStatus myActionStatus = Utilities.invokeGetActionStatus(ibURL, ibToken,
									myMarketAction.getStatusid());
							myMarketResultsItem.setStatus(myActionStatus.getStatus());

							myMarketResultsItem.setClearingValue(new BigDecimal(0));
							for (MarketActionCounterOffer myMarketActionCounterOffer : myMarketActionCounterOfferList) {
								if (myMarketActionCounterOffer.getMarketAction_Bid_id().equals(myMarketAction.getId())
										|| myMarketActionCounterOffer.getMarketAction_Offer_id()
												.equals(myMarketAction.getId())) {
									myMarketResultsItem
											.setClearingValue(myMarketActionCounterOffer.getExchangedValue());
								}
							}

							myMarketResultsItem.setClearingPrice(lastMarketSession.getClearingPrice());

							myMarketResultsItemList.add(myMarketResultsItem);

						}

					}

				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		log.debug("<- getMarketResults");
		return myMarketResultsItemList;
	}

}
