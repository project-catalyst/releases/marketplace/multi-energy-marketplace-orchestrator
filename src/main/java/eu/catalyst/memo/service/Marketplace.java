package eu.catalyst.memo.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.payload.MarketplaceComponentOLD;
import eu.catalyst.memo.payload.MarketplaceComponentOLD2;
import eu.catalyst.memo.payload.MarketplaceComponentsRegistration;
import eu.catalyst.memo.payload.ComponentMPCMRegistration;
import eu.catalyst.memo.utilities.Utilities;

@Stateless
@Path("/marketplaces/")
public class Marketplace {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Marketplace.class);

	public Marketplace() {

	}

	@GET
	@Path("{component}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public List<MarketplaceComponentOLD2> getMarketplaceComponentListByComponent(
			@PathParam("component") String component) {

		log.debug("-> getMarketplaceComponentListByComponent");
		log.debug("component" + component);

		List<MarketplaceComponentOLD2> myMarketplaceComponentList = new ArrayList<MarketplaceComponentOLD2>();
		myMarketplaceComponentList = MarketplaceComponentFacade.findEntitiesOldByComponent(component);

		log.debug("<- getMarketplaceComponentListByComponent");
		return myMarketplaceComponentList;
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void setMarketplaceComponentsList(MarketplaceComponentsRegistration marketplaceComponentsRegistration) {

		log.debug("-> setMarketplaceComponentsList");
		log.debug("marketplaceComponent: " + marketplaceComponentsRegistration);

		MarketplaceComponentFacade.insertUpdateMarketplaceComponent(marketplaceComponentsRegistration.getType(),
				marketplaceComponentsRegistration.getForm(), Utilities.IB_COMPONENT_NAME,
				marketplaceComponentsRegistration.getComponents().getIb().getUrl(),
				marketplaceComponentsRegistration.getComponents().getIb().getUsername(),
				marketplaceComponentsRegistration.getComponents().getIb().getPassword());

		MarketplaceComponentFacade.insertUpdateMarketplaceComponent(marketplaceComponentsRegistration.getType(),
				marketplaceComponentsRegistration.getForm(), Utilities.MCM_COMPONENT_NAME,
				marketplaceComponentsRegistration.getComponents().getMcm().getUrl(),
				marketplaceComponentsRegistration.getComponents().getMcm().getUsername(),
				marketplaceComponentsRegistration.getComponents().getMcm().getPassword());

		MarketplaceComponentFacade.insertUpdateMarketplaceComponent(marketplaceComponentsRegistration.getType(),
				marketplaceComponentsRegistration.getForm(), Utilities.MBM_COMPONENT_NAME,
				marketplaceComponentsRegistration.getComponents().getMbm().getUrl(),
				marketplaceComponentsRegistration.getComponents().getMbm().getUsername(),
				marketplaceComponentsRegistration.getComponents().getMbm().getPassword());
		
		Utilities.setTokenMEMOSingleComponent(marketplaceComponentsRegistration.getType(),
				marketplaceComponentsRegistration.getForm(), Utilities.IB_COMPONENT_NAME,
				marketplaceComponentsRegistration.getComponents().getIb().getUrl(),
				marketplaceComponentsRegistration.getComponents().getIb().getUsername(),
				marketplaceComponentsRegistration.getComponents().getIb().getPassword());
		
		Utilities.setTokenMCMMBMSingleComponent(marketplaceComponentsRegistration.getType(),
				marketplaceComponentsRegistration.getForm(), Utilities.MCM_COMPONENT_NAME,
				marketplaceComponentsRegistration.getComponents().getIb().getUrl(),
				marketplaceComponentsRegistration.getComponents().getMcm().getUsername(),
				marketplaceComponentsRegistration.getComponents().getMcm().getPassword());
		
		Utilities.setTokenMCMMBMSingleComponent(marketplaceComponentsRegistration.getType(),
				marketplaceComponentsRegistration.getForm(), Utilities.MBM_COMPONENT_NAME,
				marketplaceComponentsRegistration.getComponents().getIb().getUrl(),
				marketplaceComponentsRegistration.getComponents().getMbm().getUsername(),
				marketplaceComponentsRegistration.getComponents().getMbm().getPassword());

		

		log.debug("<- setMarketplaceComponentsList");
		return;
	}

	@POST
	@Path("component/mpcm/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void setComponentMPCM(ComponentMPCMRegistration componentMPCMRegistration) {

		log.debug("-> setComponentMPCM");
		log.debug("componentMPCMRegistration: " + componentMPCMRegistration);

		MarketplaceComponentFacade.setMarketplaceComponent(
				componentMPCMRegistration.getType(),
				componentMPCMRegistration.getForm(),
				Utilities.MPCM_COMPONENT_NAME,
				componentMPCMRegistration.getComponent().getMpcm().getUrl(),
				componentMPCMRegistration.getComponent().getMpcm().getUsername(),
				componentMPCMRegistration.getComponent().getMpcm().getPassword());

		log.debug("<- setComponentMPCM");
		return;
	}

}
