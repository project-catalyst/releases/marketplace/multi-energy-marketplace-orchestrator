package eu.catalyst.memo.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;

import eu.catalyst.memo.jpa.ConstraintFacade;
import eu.catalyst.memo.jpa.CorrelationFacade;
import eu.catalyst.memo.jpa.MarketactionCorrelationFacade;
import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.Constraint;
import eu.catalyst.memo.model.Correlation;
import eu.catalyst.memo.model.MarketactionCorrelation;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.payload.ActionType;
import eu.catalyst.memo.payload.CorrelatedMarketActionsItem;
import eu.catalyst.memo.payload.CorrelatedMarketActionsRequest;
import eu.catalyst.memo.payload.CorrelatedMarketActionsResponse;
import eu.catalyst.memo.payload.CorrelationActionNEW;
import eu.catalyst.memo.payload.MarketSession;
import eu.catalyst.memo.utilities.Utilities;

@Stateless
@Path("/correlatedMarketActions/")
public class CorrelatedMarkeActions {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CorrelatedMarkeActions.class);

	public CorrelatedMarkeActions() {

	}

	@GET
	@Path("{marketActorName}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public List<CorrelatedMarketActionsResponse> getCorrelatedMarkeActions(
			@PathParam("marketActorName") String marketActorName, @PathParam("timeframe") String timeframe) {

		log.debug("-> getCorrelatedMarkeActions");
		log.debug("marketActorName:" + marketActorName);
		log.debug("timeframe:" + timeframe);

		List<CorrelatedMarketActionsResponse> myCorrelatedMarketActionsResponseList = new ArrayList<CorrelatedMarketActionsResponse>();

		// read all row from table Correlation
		List<Correlation> myCorrelationList = CorrelationFacade.findEntitiesByTimeframeUsername(timeframe,
				marketActorName);

		for (Correlation myCorrelation : myCorrelationList) {

			CorrelatedMarketActionsResponse myCorrelatedMarketActionsResponse = new CorrelatedMarketActionsResponse();

			myCorrelatedMarketActionsResponse.setId(myCorrelation.getId());

			myCorrelation.getConstraintId();
			// convert myCorrelation.getConstraintId() to String;

			myCorrelatedMarketActionsResponse.setConstraintType(
					ConstraintFacade.getConstraint(myCorrelation.getConstraintId()).getDescription());

			List<CorrelatedMarketActionsItem> myCorrelatedMarketActionsItemList = new ArrayList<CorrelatedMarketActionsItem>();

			List<MarketactionCorrelation> myMarketactionCorrelationList = MarketactionCorrelationFacade
					.findByCorrelationId(myCorrelation.getId());

			for (MarketactionCorrelation myMarketactionCorrelation : myMarketactionCorrelationList) {
				CorrelatedMarketActionsItem myCorrelatedMarketActionsItem = new CorrelatedMarketActionsItem();
				myCorrelatedMarketActionsItem.setActionId(myMarketactionCorrelation.getActionId());

				String mymarketplaceForm = "";
				// compute mymarketplaceForm by
				// myMarketactionCorrelation.getInformationBrokerId()
				// Session.form
				Integer ibId = myMarketactionCorrelation.getInformationBrokerId();
				MarketplaceComponent myMarketplaceComponent = MarketplaceComponentFacade.getMarketplaceComponent(ibId);
				String type = myMarketplaceComponent.getType();
				String form = myMarketplaceComponent.getForm();
				String ibToken = Utilities.getToken(type, form, Utilities.MEMO_COMPONENT_NAME);

				try {
					MarketSession myMarketSession = Utilities.invokeGetSession(myMarketplaceComponent.getUrl(), ibToken,
							myMarketactionCorrelation.getSessionId());
					mymarketplaceForm = myMarketSession.getFormid().getForm();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				myCorrelatedMarketActionsItem.setMarketplaceForm(mymarketplaceForm);

				myCorrelatedMarketActionsItemList.add(myCorrelatedMarketActionsItem);
			}

			myCorrelatedMarketActionsResponse.setMarketActions(myCorrelatedMarketActionsItemList);

			myCorrelatedMarketActionsResponseList.add(myCorrelatedMarketActionsResponse);

		}

		log.debug("<- getCorrelatedMarkeActions");
		return myCorrelatedMarketActionsResponseList;
	}

	@POST
	@Path("{marketActorName}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public CorrelatedMarketActionsResponse postCorrelatedMarkeActions(
			@PathParam("marketActorName") String marketActorName, @PathParam("timeframe") String timeframe,
			CorrelatedMarketActionsRequest correlatedMarketActionsRequest) {

		log.debug("-> postCorrelatedMarkeActions");
		log.debug("marketActorName:" + marketActorName);
		log.debug("timeframe:" + timeframe);
		log.debug("correlatedMarketActionsRequest:" + new Gson().toJson(correlatedMarketActionsRequest));

		List<CorrelatedMarketActionsItem> myMarketActions = correlatedMarketActionsRequest.getMarketActions();

		List<MarketSession> myActiveSessionsList = null;
		Integer myCorrelationId = 0;

		for (CorrelatedMarketActionsItem myCorrelatedMarketActionsItem : myMarketActions) {

			Integer ibId = 0;
			// Integer sessionId = 0;

			String marketplaceForm = myCorrelatedMarketActionsItem.getMarketplaceForm();
			String form = Utilities.normalizeForm(marketplaceForm);

			// estrai URL del MarketplaceComponent
			List<MarketplaceComponent> myMarketplaceComponentListIB = new ArrayList<MarketplaceComponent>();
			myMarketplaceComponentListIB = MarketplaceComponentFacade.findEntitiesByFormComponent(form,
					Utilities.IB_COMPONENT_NAME);

			if (!myMarketplaceComponentListIB.isEmpty()) {
				String ibURL = myMarketplaceComponentListIB.get(0).getUrl();
				String type = myMarketplaceComponentListIB.get(0).getType();

				// estrai token relativo all IB
				String ibToken = Utilities.getToken(type, form, Utilities.MEMO_COMPONENT_NAME);

				int marketplaceid;
				try {
					marketplaceid = Utilities.invokeGetMarketplaceByFormTimeframe(ibURL, ibToken, marketplaceForm,
							timeframe);
					myActiveSessionsList = Utilities.invokeGetActiveSessions(ibURL, ibToken, marketplaceid);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (!myActiveSessionsList.isEmpty()) {
					Integer sessionId = myActiveSessionsList.get(0).getId();
					Integer formId = myActiveSessionsList.get(0).getFormid().getId();

					ibId = myMarketplaceComponentListIB.get(0).getId();

					log.debug("myCorrelationId:" + myCorrelationId);
					if (myCorrelationId == 0) {
						List<Constraint> myConstraintList = ConstraintFacade.findAllEntities();
						Integer constraintId = 0;
						for (Constraint myConstraint : myConstraintList) {
							if (myConstraint.getDescription()
									.equals(correlatedMarketActionsRequest.getConstraintType())) {
								constraintId = myConstraint.getId();
							}
						}

						Long timestamp = new Date().getTime();
						Correlation myCorrelation = CorrelationFacade.setCorrelation(constraintId, timeframe, timestamp);
						log.debug("before - myCorrelationId:" + myCorrelationId);
						myCorrelationId = myCorrelation.getId();
						log.debug("after - myCorrelationId:" + myCorrelationId);
					}

					// Insert a new row into table MarketactionCorrelation
					MarketactionCorrelationFacade.setMarketactionCorrelation(marketActorName, ibId, sessionId,
							myCorrelatedMarketActionsItem.getActionId(), myCorrelationId);
				}
			}

		}

		CorrelatedMarketActionsResponse myCorrelatedMarketActionsResponse = null;
		if (myActiveSessionsList != null && !myActiveSessionsList.isEmpty()) {

			myCorrelatedMarketActionsResponse = new CorrelatedMarketActionsResponse();
			// compute id from myCorrelation.getId() of new Correlation
			myCorrelatedMarketActionsResponse.setId(myCorrelationId);
			myCorrelatedMarketActionsResponse.setConstraintType(correlatedMarketActionsRequest.getConstraintType());
			myCorrelatedMarketActionsResponse.setMarketActions(myMarketActions);

		}
		log.debug("<- postCorrelatedMarkeActions");
		return myCorrelatedMarketActionsResponse;
	}

}
