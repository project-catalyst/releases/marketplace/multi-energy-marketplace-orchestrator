package eu.catalyst.memo.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import eu.catalyst.memo.jpa.ConstraintFacade;
import eu.catalyst.memo.jpa.CorrelationFacade;
import eu.catalyst.memo.jpa.MarketactionCorrelationFacade;
import eu.catalyst.memo.model.Constraint;
import eu.catalyst.memo.model.Correlation;
import eu.catalyst.memo.model.MarketactionCorrelation;
import eu.catalyst.memo.payload.CorrelationActionNEW;
import eu.catalyst.memo.payload.CorrelationActionOLD;
import eu.catalyst.memo.payload.CorrelationListResponseGET;
import eu.catalyst.memo.payload.CorrelationRequestPOST;
import eu.catalyst.memo.payload.CorrelationRequestPUTDELETE;
import eu.catalyst.memo.payload.CorrelationResponseGET;
import eu.catalyst.memo.payload.UsernameRequest;

@Stateless
@Path("/marketplace/")
public class CorrelatingActions {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CorrelatingActions.class);

	public CorrelatingActions() {

	}

	@GET
	@Path("all/coupleble/actor/actions/coupled/{timeframe}/{username}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public CorrelationListResponseGET getHistoricalCorrelatedMarketsActions(@PathParam("timeframe") String timeframe,
			@PathParam("username") String username) {

		log.debug("-> getHistoricalCorrelatedMarketsActions");
		log.debug("timeframe: " + timeframe);
		log.debug("username: " + username);

		CorrelationListResponseGET myCorrelationListResponseGET = new CorrelationListResponseGET();
		myCorrelationListResponseGET.setUsername(username);

		List<CorrelationResponseGET> myCorrelationResponseGETList = new ArrayList<CorrelationResponseGET>();

		// read all row from table Correlation
		List<Correlation> myCorrelationList = CorrelationFacade.findEntitiesByTimeframeUsername(timeframe, username);

		for (Correlation myCorrelation : myCorrelationList) {

			CorrelationResponseGET myCorrelationResponseGET = new CorrelationResponseGET();

			myCorrelationResponseGET.setCorrelationId(myCorrelation.getId());
			myCorrelationResponseGET.setConstraintId(myCorrelation.getConstraintId());
			myCorrelationResponseGET.setTimeframe(myCorrelation.getTimeframe());
			myCorrelationResponseGET.setTimestamp(myCorrelation.getTimestamp());

			List<CorrelationActionOLD> myCorrelationActionOLDList = new ArrayList<CorrelationActionOLD>();

			List<MarketactionCorrelation> myMarketactionCorrelationList = MarketactionCorrelationFacade
					.findByCorrelationId(myCorrelation.getId());

			for (MarketactionCorrelation myMarketactionCorrelation : myMarketactionCorrelationList) {
				CorrelationActionOLD myCorrelationActionOLD = new CorrelationActionOLD();
				myCorrelationActionOLD.setId(myMarketactionCorrelation.getId());
				myCorrelationActionOLD.setInformationBrokerId(myMarketactionCorrelation.getInformationBrokerId());
				myCorrelationActionOLD.setSessionId(myMarketactionCorrelation.getSessionId());
				myCorrelationActionOLD.setActionId(myMarketactionCorrelation.getActionId());
				myCorrelationActionOLDList.add(myCorrelationActionOLD);
			}

			myCorrelationResponseGET.setActions(myCorrelationActionOLDList);
			myCorrelationResponseGETList.add(myCorrelationResponseGET);
		}

		myCorrelationListResponseGET.setCorrelations(myCorrelationResponseGETList);

		log.debug("<- getHistoricalCorrelatedMarketsActions");
		return myCorrelationListResponseGET;
	}

	@GET
	@Path("all/coupleble/actor/actions/coupled/{timeframe}/{start}/{end}/{username}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public CorrelationListResponseGET getCorrelatedMarketsActions(@PathParam("timeframe") String timeframe,
			@PathParam("start") Long start, @PathParam("end") Long end, @PathParam("username") String username) {

		log.debug("-> getCorrelatedMarketsActions");
		log.debug("timeframe: " + timeframe);
		log.debug("start: " + start);
		log.debug("end: " + end);
		log.debug("username: " + username);
		
		CorrelationListResponseGET myCorrelationListResponseGET = new CorrelationListResponseGET();
		myCorrelationListResponseGET.setUsername(username);

		List<CorrelationResponseGET> myCorrelationResponseGETList = new ArrayList<CorrelationResponseGET>();

		// read all row from table Correlation
		List<Correlation> myCorrelationList = CorrelationFacade.findEntitiesByTimeframeStartEndUsername(timeframe, start, end, username);

		for (Correlation myCorrelation : myCorrelationList) {

			CorrelationResponseGET myCorrelationResponseGET = new CorrelationResponseGET();

			myCorrelationResponseGET.setCorrelationId(myCorrelation.getId());
			myCorrelationResponseGET.setConstraintId(myCorrelation.getConstraintId());
			myCorrelationResponseGET.setTimeframe(myCorrelation.getTimeframe());
			myCorrelationResponseGET.setTimestamp(myCorrelation.getTimestamp());

			List<CorrelationActionOLD> myCorrelationActionOLDList = new ArrayList<CorrelationActionOLD>();

			List<MarketactionCorrelation> myMarketactionCorrelationList = MarketactionCorrelationFacade
					.findByCorrelationId(myCorrelation.getId());

			for (MarketactionCorrelation myMarketactionCorrelation : myMarketactionCorrelationList) {
				CorrelationActionOLD myCorrelationActionOLD = new CorrelationActionOLD();
				myCorrelationActionOLD.setId(myMarketactionCorrelation.getId());
				myCorrelationActionOLD.setInformationBrokerId(myMarketactionCorrelation.getInformationBrokerId());
				myCorrelationActionOLD.setSessionId(myMarketactionCorrelation.getSessionId());
				myCorrelationActionOLD.setActionId(myMarketactionCorrelation.getActionId());
				myCorrelationActionOLDList.add(myCorrelationActionOLD);
			}

			myCorrelationResponseGET.setActions(myCorrelationActionOLDList);
			myCorrelationResponseGETList.add(myCorrelationResponseGET);
		}

		myCorrelationListResponseGET.setCorrelations(myCorrelationResponseGETList);

		log.debug("<- getCorrelatedMarketsActions");
		return myCorrelationListResponseGET;
	}

	@POST
	@Path("all/coupleble/actor/actions/coupled/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void postCorrelatedMarketsActions(CorrelationRequestPOST myCorrelationRequestPOST) {

		log.debug("-> postCorrelatedMarketsActions");

		// Insert a new row into table Correlation setting constraint_id to
		// myCorrelationRequestPOST.constraintId
		Correlation myCorrelation = CorrelationFacade.setCorrelation(myCorrelationRequestPOST.getConstraintId(),
				myCorrelationRequestPOST.getTimeframe(), myCorrelationRequestPOST.getTimestamp());
		Integer myCorrelationId = myCorrelation.getId();

		for (CorrelationActionNEW actionNew : myCorrelationRequestPOST.getActions()) {

			// Insert a new row into table MarketactionCorrelation
			MarketactionCorrelationFacade.setMarketactionCorrelation(myCorrelationRequestPOST.getUsername(),
					actionNew.getInformationBrokerId(), actionNew.getSessionId(), actionNew.getActionId(),
					myCorrelationId);

		}

		log.debug("<- postCorrelatedMarketsActions");
		return;
	}

	@PUT
	@Path("all/coupleble/actor/actions/coupled/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void putCorrelatedMarketsActions(CorrelationRequestPUTDELETE myCorrelationRequestPutDelete) {

		log.debug("-> putCorrelatedMarketsActions");

		for (CorrelationActionOLD actionOld : myCorrelationRequestPutDelete.getActions()) {
			// Update rows of table MarketactionCorrelation where correlation_id =
			// myCorrelationRequestPutDelete.correlationId

			MarketactionCorrelationFacade.updateActionOld(actionOld.getId(), actionOld.getInformationBrokerId(),
					actionOld.getSessionId(), actionOld.getActionId());

		}

		// Update row from table Correlation where id =
		// myCorrelationRequestPutDelete.correlationId
		CorrelationFacade.updateConstraintId(myCorrelationRequestPutDelete.getCorrelationId(),
				myCorrelationRequestPutDelete.getConstraintId(), myCorrelationRequestPutDelete.getTimeframe(),
				myCorrelationRequestPutDelete.getTimestamp());

		log.debug("<- putCorrelatedMarketsActions");
		return;
	}

	@DELETE
	@Path("all/coupleble/actor/actions/coupled/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void deleteCorrelatedMarketsActions(CorrelationRequestPUTDELETE myCorrelationRequestPutDelete) {

		log.debug("-> deleteCorrelatedMarketsActions");

		// Delete rows from table MarketactionCorrelation where correlation_id =
		// myCorrelationRequestPutDelete.correlationId
		MarketactionCorrelationFacade.deleteByCorrelationId(myCorrelationRequestPutDelete.getCorrelationId());

		// Delete row from table Correlation where id =
		// myCorrelationRequestPutDelete.correlationId
		CorrelationFacade.deleteById(myCorrelationRequestPutDelete.getCorrelationId());

		log.debug("<- deleteCorrelatedMarketsActions");
		return;

	}
}
