package eu.catalyst.memo.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.gson.Gson;

import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.jpa.MarketplaceParticipantFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.model.MarketplaceParticipant;
import eu.catalyst.memo.payload.ActionStatus;
import eu.catalyst.memo.payload.ActionType;
import eu.catalyst.memo.payload.ActiveMarketActionItemGET;
import eu.catalyst.memo.payload.ActiveMarketActionItemRequestPOST;
import eu.catalyst.memo.payload.ActiveMarketActionItemResponsePOST;
import eu.catalyst.memo.payload.MarketAction;
import eu.catalyst.memo.payload.MarketSession;
import eu.catalyst.memo.utilities.Utilities;

@Stateless
@Path("/marketActions/")
public class ActiveMarketActions {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ActiveMarketActions.class);

	public ActiveMarketActions() {

	}

	@GET
	@Path("{marketActorName}/{marketplaceForm}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public List<ActiveMarketActionItemGET> getActiveMarketActions(@PathParam("marketActorName") String marketActorName,
			@PathParam("marketplaceForm") String marketplaceForm, @PathParam("timeframe") String timeframe) {

		log.debug("-> getActiveMarketActions");
		log.debug("marketActorName:" + marketActorName);
		log.debug("marketplaceForm:" + marketplaceForm);
		log.debug("timeframe:" + timeframe);

		List<ActiveMarketActionItemGET> myActiveMarketActionItemGETList = new ArrayList<ActiveMarketActionItemGET>();

		String form = Utilities.normalizeForm(marketplaceForm);

		// estrai URL del MarketplaceComponent
		List<MarketplaceComponent> myMarketplaceComponentListIB = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentListIB = MarketplaceComponentFacade.findEntitiesByFormComponent(form,
				Utilities.IB_COMPONENT_NAME);

		if (!myMarketplaceComponentListIB.isEmpty()) {
			String ibURL = myMarketplaceComponentListIB.get(0).getUrl();
			String type = myMarketplaceComponentListIB.get(0).getType();
			// estrai token relativo all IB
			String ibToken = Utilities.getToken(type, form, Utilities.MEMO_COMPONENT_NAME);

			try {
				int marketplaceid = Utilities.invokeGetMarketplaceByFormTimeframe(ibURL, ibToken, marketplaceForm,
						timeframe);
				List<MarketSession> myActiveSessionsList = Utilities.invokeGetActiveSessions(ibURL, ibToken,
						marketplaceid);

				if (!myActiveSessionsList.isEmpty()) {
					Integer marketSessionId = myActiveSessionsList.get(0).getId();
					List<MarketAction> myMarketActionList = Utilities.invokeGetMarketActionsByMarketPlaceSessionId(
							ibURL, ibToken, marketplaceid, marketSessionId);

					for (MarketAction myMarketAction : myMarketActionList) {

						ActiveMarketActionItemGET myActiveMarketActionItemGET = new ActiveMarketActionItemGET();
						myActiveMarketActionItemGET.setId(myMarketAction.getId());
						myActiveMarketActionItemGET.setDate(myMarketAction.getDate());
						myActiveMarketActionItemGET.setActionStartTime(myMarketAction.getActionStartTime());
						myActiveMarketActionItemGET.setActionEndTime(myMarketAction.getActionEndTime());
						myActiveMarketActionItemGET.setValue(myMarketAction.getValue());
						myActiveMarketActionItemGET.setUom(myMarketAction.getUom());
						myActiveMarketActionItemGET.setPrice(myMarketAction.getPrice());
						myActiveMarketActionItemGET.setDeliveryPoint(myMarketAction.getDeliveryPoint());

						List<ActionType> myActionTypeList = Utilities.invokeGetActionTypeList(ibURL, ibToken);
						for (ActionType myActionType : myActionTypeList) {
							if (myActionType.getId() == myMarketAction.getActionTypeid()) {
								myActiveMarketActionItemGET.setActionType(myActionType.getType());
							}
						}

						ActionStatus myActionStatus = Utilities.invokeGetActionStatus(ibURL, ibToken,
								myMarketAction.getStatusid());
						myActiveMarketActionItemGET.setStatus(myActionStatus.getStatus());

						myActiveMarketActionItemGETList.add(myActiveMarketActionItemGET);

					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		log.debug("<- getActiveMarketActions");
		return myActiveMarketActionItemGETList;
	}

	@POST
	@Path("{marketActorName}/{marketplaceForm}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public List<ActiveMarketActionItemResponsePOST> postActiveMarketActions(
			@PathParam("marketActorName") String marketActorName, @PathParam("marketplaceForm") String marketplaceForm,
			@PathParam("timeframe") String timeframe,
			List<ActiveMarketActionItemRequestPOST> myActiveMarketActionItemRequestPOSTList) {

		log.debug("-> postActiveMarketActions");
		log.debug("marketActorName:" + marketActorName);
		log.debug("marketplaceForm:" + marketplaceForm);
		log.debug("timeframe:" + timeframe);
		log.debug("myActiveMarketActionItemRequestPOST:" + new Gson().toJson(myActiveMarketActionItemRequestPOSTList));

		List<ActiveMarketActionItemResponsePOST> myActiveMarketActionItemResponsePOSTList = new ArrayList<ActiveMarketActionItemResponsePOST>();

		String form = Utilities.normalizeForm(marketplaceForm);

		// estrai URL del MarketplaceComponent
		List<MarketplaceComponent> myMarketplaceComponentListIB = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentListIB = MarketplaceComponentFacade.findEntitiesByFormComponent(form,
				Utilities.IB_COMPONENT_NAME);

		if (!myMarketplaceComponentListIB.isEmpty()) {
			String ibURL = myMarketplaceComponentListIB.get(0).getUrl();
			String type = myMarketplaceComponentListIB.get(0).getType();

			// estrai token relativo all IB
			String ibToken = Utilities.getToken(type, form, Utilities.MEMO_COMPONENT_NAME);

			try {
				int marketplaceid = Utilities.invokeGetMarketplaceByFormTimeframe(ibURL, ibToken, marketplaceForm,
						timeframe);
				List<MarketSession> myActiveSessionsList = Utilities.invokeGetActiveSessions(ibURL, ibToken,
						marketplaceid);

				if (!myActiveSessionsList.isEmpty()) {
					Integer marketSessionId = myActiveSessionsList.get(0).getId();
					Integer formId = myActiveSessionsList.get(0).getFormid().getId();

					Integer ibId = myMarketplaceComponentListIB.get(0).getId();

					MarketplaceParticipant myMarketplaceParticipant = MarketplaceParticipantFacade
							.getMarketplaceParticipant(marketActorName, ibId);

					List<MarketAction> myMarketActionRequestList = new ArrayList<MarketAction>();

					List<ActionType> myActionTypeList = Utilities.invokeGetActionTypeList(ibURL, ibToken);
					List<ActionStatus> myActionStatusList = Utilities.invokeGetActionStatusList(ibURL, ibToken);

					// set myMarketActionRequestList converting myActiveMarketActionItemRequestPOST

					for (ActiveMarketActionItemRequestPOST myActiveMarketActionItemRequestPOST : myActiveMarketActionItemRequestPOSTList) {

						MarketAction myMarketAction = new MarketAction();
						myMarketAction.setDate(myActiveMarketActionItemRequestPOST.getDate());
						myMarketAction.setActionStartTime(myActiveMarketActionItemRequestPOST.getActionStartTime());
						myMarketAction.setActionEndTime(myActiveMarketActionItemRequestPOST.getActionEndTime());
						myMarketAction.setValue(myActiveMarketActionItemRequestPOST.getValue());
						myMarketAction.setUom(myActiveMarketActionItemRequestPOST.getUom());
						myMarketAction.setPrice(myActiveMarketActionItemRequestPOST.getPrice());
						myMarketAction.setDeliveryPoint(Utilities.DOP);
						myMarketAction.setMarketSessionid(marketSessionId);
						myMarketAction.setMarketActorid(myMarketplaceParticipant.getActorId());
						myMarketAction.setFormid(formId);

						for (ActionType myActionType : myActionTypeList) {
							if (myActionType.getType()
									.equalsIgnoreCase(myActiveMarketActionItemRequestPOST.getActionType())) { // "bid",
																												// "offer"
								myMarketAction.setActionTypeid(myActionType.getId());
							}
						}

						for (ActionStatus myActionStatus : myActionStatusList) {
							if (myActionStatus.getStatus().equalsIgnoreCase(Utilities.UNCHECKED)) { // "uncheched"
								myMarketAction.setStatusid(myActionStatus.getId());
							}
						}

						myMarketActionRequestList.add(myMarketAction);
					}

					List<MarketAction> myMarketActionResponseList = Utilities
							.invokePostMarketActionsByMarketPlaceSessionId(ibURL, ibToken, marketplaceid,
									marketSessionId, myMarketActionRequestList);

					// set myActiveMarketActionItemResponsePOST converting myMarketActionRequestList

					for (MarketAction myMarketAction : myMarketActionResponseList) {

						ActiveMarketActionItemResponsePOST myActiveMarketActionItemResponsePOST = new ActiveMarketActionItemResponsePOST();

						myActiveMarketActionItemResponsePOST.setId(myMarketAction.getId());
						myActiveMarketActionItemResponsePOST.setDate(myMarketAction.getDate());
						myActiveMarketActionItemResponsePOST.setActionStartTime(myMarketAction.getActionStartTime());
						myActiveMarketActionItemResponsePOST.setActionEndTime(myMarketAction.getActionEndTime());
						myActiveMarketActionItemResponsePOST.setValue(myMarketAction.getValue());
						myActiveMarketActionItemResponsePOST.setUom(myMarketAction.getUom());
						myActiveMarketActionItemResponsePOST.setPrice(myMarketAction.getPrice());

						for (ActionType myActionType : myActionTypeList) {
							if (myActionType.getId() == myMarketAction.getActionTypeid()) {
								myActiveMarketActionItemResponsePOST.setActionType(myActionType.getType());
							}
						}

						ActionStatus myActionStatus = Utilities.invokeGetActionStatus(ibURL, ibToken,
								myMarketAction.getStatusid());

						myActiveMarketActionItemResponsePOST.setStatus(myActionStatus.getStatus());

						myActiveMarketActionItemResponsePOSTList.add(myActiveMarketActionItemResponsePOST);

					}

				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		log.debug("<- postActiveMarketActions");
		return myActiveMarketActionItemResponsePOSTList;
	}

}
