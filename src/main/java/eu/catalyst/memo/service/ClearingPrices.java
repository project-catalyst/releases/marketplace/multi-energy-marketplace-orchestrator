package eu.catalyst.memo.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import eu.catalyst.memo.payload.ClearingPricePreviousDaysItem;
import eu.catalyst.memo.payload.ClearingPriceTempItem;
import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.payload.ClearingPriceItem;
import eu.catalyst.memo.utilities.Utilities;
import java.util.Date;
import java.util.GregorianCalendar;

@Stateless
@Path("/clearingPricesPreviousDay/")
public class ClearingPrices {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ClearingPrices.class);

	public ClearingPrices() {

	}

	@GET
	@Path("{marketActorName}/{marketplaceForm}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	// example:
	// /clearingPricesPreviusDay/participant_el_mc/marketActorName/electric_energy/intra_day/
	// <marketActorName>: username of the participant using the connector
	// <marketplaceForm>: {electric_energy, thermal_energy,it_load,
	// congestion_management, reactive_power_compensation,
	// spinning_reserve,scheduling}
	// <timeframe>: {intra_day, day_ahead}
	// Returns a list of 24 clearing prices each relative to an interval of one
	// hour.

	public List<ClearingPriceItem> getClearingPricesPreviousDay(@PathParam("marketActorName") String marketActorName,
			@PathParam("marketplaceForm") String marketplaceForm, @PathParam("timeframe") String timeframe) {

		log.debug("-> getClearingPricesPreviousDay");
		log.debug("marketActorName:" + marketActorName);
		log.debug("marketplaceForm:" + marketplaceForm);
		log.debug("timeframe:" + timeframe);

		List<ClearingPriceItem> myClearingPriceList = new ArrayList<ClearingPriceItem>();
		List<ClearingPriceTempItem> myClearingPriceTempList = new ArrayList<ClearingPriceTempItem>();

		String form = Utilities.normalizeForm(marketplaceForm);

		// estrai URL del MarketplaceComponent
		List<MarketplaceComponent> myMarketplaceComponentListIB = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentListIB = MarketplaceComponentFacade.findEntitiesByFormComponent(form,
				Utilities.IB_COMPONENT_NAME);

		if (!myMarketplaceComponentListIB.isEmpty()) {
			String ibURL = myMarketplaceComponentListIB.get(0).getUrl();
			String type = myMarketplaceComponentListIB.get(0).getType();
			// estrai token relativo all IB
			String ibToken = Utilities.getToken(type, form, Utilities.MEMO_COMPONENT_NAME);

			// long milliseconds = new Date().getTime();
			GregorianCalendar gc = new GregorianCalendar();
			// gc.add(Calendar.DAY_OF_YEAR, -1);
			gc.set(Calendar.HOUR_OF_DAY, 0);
			gc.set(Calendar.MINUTE, 0);
			gc.set(Calendar.SECOND, 0);
			gc.set(Calendar.MILLISECOND, 0);
			long milliseconds = gc.getTimeInMillis();

			// call service of IB-x
			// "/marketplace/{marketPlaceID}/form/{marketForm}/reference-prices-previous-days/date/{millisecondsTimestamp}/"

			try {
				int marketplaceid = Utilities.invokeGetMarketplaceByFormTimeframe(ibURL, ibToken, marketplaceForm,
						timeframe);
				List<ClearingPricePreviousDaysItem> myClearingPricesPreviousDays = Utilities
						.invokeGetClearingPricesPreviousDays(ibURL, ibToken, marketplaceid, marketplaceForm,
								milliseconds);

				// Date dayDate = myClearingPricesPreviousDays.get(0).getValidityStartTime();
				// fill the 24 items in the array with price zeros
				for (int slot = 0; slot < 24; slot++) {
					// crea uno slot vuoto
					ClearingPriceTempItem myClearingPriceTempItem = new ClearingPriceTempItem();
					myClearingPriceTempItem.initializeSlot(slot);
					// inserisce lo slot nella lista
					myClearingPriceTempList.add(myClearingPriceTempItem);
				}

				for (ClearingPricePreviousDaysItem myClearingPricePreviousDaysItem : myClearingPricesPreviousDays) {
					// determino l'indice inferiore inferiore = validityStartTime.HH
					// determino l'indice superiore = validityEndTime.HH-1 se validityEndTime.MM =
					// 00 and validityEndTime.SS = 00;
					// altrimenti validityEndTime.HH
					// per tutti gli item di indice € inf e sup:
					//
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(myClearingPricePreviousDaysItem.getStartTime());
					int slotMin = calendar.get(Calendar.HOUR_OF_DAY);
					calendar.setTime(myClearingPricePreviousDaysItem.getEndTime());
					int slotMax = calendar.get(Calendar.HOUR_OF_DAY);
					if (calendar.get(Calendar.MINUTE) == 0 && calendar.get(Calendar.SECOND) == 0
							&& calendar.get(Calendar.MILLISECOND) == 0) {
						slotMax--;
					}
					for (int slot = slotMin; slot <= slotMax; slot++) {
						BigDecimal actualPrice = myClearingPriceTempList.get(slot).getClearingPriceSum();
						BigDecimal sumPrice = actualPrice.add(myClearingPricePreviousDaysItem.getPrice());
//						if (myPrice.compareTo(BigDecimal.ZERO) == 0) {
//							myClearingPriceList.get(slot).setClearingPrice(newPrice);
//						} else {
//							BigDecimal maxPrice = myPrice.max(newPrice);
//							myClearingPriceList.get(slot).setClearingPrice(maxPrice);
//						}
						myClearingPriceTempList.get(slot).setClearingPriceSum(sumPrice);
						myClearingPriceTempList.get(slot).incCount();
					}
				}

				// compute arithmetic average for each slot
				for (ClearingPriceTempItem myClearingPriceTempItem : myClearingPriceTempList) {
					ClearingPriceItem myClearingPriceItem = new ClearingPriceItem();
					myClearingPriceItem.setValidityStartTime(myClearingPriceTempItem.getValidityStartTime());
					myClearingPriceItem.setValidityEndTime(myClearingPriceTempItem.getValidityEndTime());
					if (myClearingPriceTempItem.getClearingPriceSum().compareTo(BigDecimal.ZERO) == 0) {
						myClearingPriceItem.setClearingPrice(BigDecimal.ZERO);
					} else {
						BigDecimal quotient = new BigDecimal(myClearingPriceTempItem.getCount());
						myClearingPriceItem.setClearingPrice(myClearingPriceTempItem.getClearingPriceSum()
								.divide(quotient, Utilities.SCALE_BIG_DECIMAL, RoundingMode.HALF_UP));

					}

					myClearingPriceList.add(myClearingPriceItem);
				}

				// search first Price not zero
				BigDecimal priceCurrent = BigDecimal.ZERO;
				for (int slot = 0; slot < 24 && priceCurrent.compareTo(BigDecimal.ZERO) == 0; slot++) {
					if (myClearingPriceList.get(slot).getClearingPrice().compareTo(BigDecimal.ZERO) > 0) {
						priceCurrent = myClearingPriceList.get(slot).getClearingPrice();
					}
				}

				// fill the gap in the array
				for (ClearingPriceItem myClearingPriceItem : myClearingPriceList) {
					if (myClearingPriceItem.getClearingPrice().compareTo(BigDecimal.ZERO) == 0) {
						myClearingPriceItem.setClearingPrice(priceCurrent);
					} else {
						priceCurrent = myClearingPriceItem.getClearingPrice();
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// return the array complete
		log.debug("<- getClearingPricesPreviousDay");
		return myClearingPriceList;
	}

}
