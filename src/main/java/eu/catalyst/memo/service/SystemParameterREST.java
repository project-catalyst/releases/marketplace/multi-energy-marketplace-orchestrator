package eu.catalyst.memo.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import eu.catalyst.memo.jpa.SystemParameterFacade;
import eu.catalyst.memo.model.SystemParameter;

@Stateless
@Path("/systemparameter/")
public class SystemParameterREST {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SystemParameterREST.class);

	public SystemParameterREST() {

	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public SystemParameter create(SystemParameter entity) {

		return SystemParameterFacade.setSystemParameter(entity);
	}

	@PUT
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void edit(SystemParameter entity) {
		SystemParameterFacade.updateSystemParameter(entity);
	}

	@DELETE
	@Path("{id}/")
	public void remove(@PathParam("id") Integer id) {
		SystemParameterFacade.deleteSystemParameter(id);
	}

	@GET
	@Path("{id}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public SystemParameter findId(@PathParam("id") Integer id) {
		return SystemParameterFacade.getSystemParameter(id);
	}

	@GET
	@Path("name/{name}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public SystemParameter findName(@PathParam("name") String name) {
		return SystemParameterFacade.findEntityByName(name);
	}

	@GET
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public List<SystemParameter> findAll() {
		return SystemParameterFacade.findAllEntities();
	}

}
