package eu.catalyst.memo.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.payload.ActiveSessionItem;
import eu.catalyst.memo.payload.MarketSession;
import eu.catalyst.memo.payload.ReferencePricePreviousDaysItem;
import eu.catalyst.memo.utilities.Utilities;

@Stateless
@Path("/activeSessions/")
public class ActiveSessions {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ActiveSessions.class);

	public ActiveSessions() {

	}

	@GET
	@Path("{marketActorName}/{marketplaceForm}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })

	public List<ActiveSessionItem> getActiveSessions(@PathParam("marketActorName") String marketActorName,
			@PathParam("marketplaceForm") String marketplaceForm, @PathParam("timeframe") String timeframe) {

		log.debug("-> getActiveSessions");
		log.debug("marketActorName:" + marketActorName);
		log.debug("marketplaceForm:" + marketplaceForm);
		log.debug("timeframe:" + timeframe);

		List<ActiveSessionItem> myActiveSessionItemList = new ArrayList<ActiveSessionItem>();

		String form = Utilities.normalizeForm(marketplaceForm);

		// estrai URL del MarketplaceComponent
		List<MarketplaceComponent> myMarketplaceComponentListIB = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentListIB = MarketplaceComponentFacade.findEntitiesByFormComponent(form,
				Utilities.IB_COMPONENT_NAME);

		if (!myMarketplaceComponentListIB.isEmpty()) {
			String ibURL = myMarketplaceComponentListIB.get(0).getUrl();
			String type = myMarketplaceComponentListIB.get(0).getType();
			// estrai token relativo all IB
			String ibToken = Utilities.getToken(type, form, Utilities.MEMO_COMPONENT_NAME);

			try {
				int marketplaceid = Utilities.invokeGetMarketplaceByFormTimeframe(ibURL, ibToken, marketplaceForm,
						timeframe);
				List<MarketSession> myActiveSessionsList = Utilities.invokeGetActiveSessions(ibURL, ibToken,
						marketplaceid);
				for (MarketSession myMarketSession : myActiveSessionsList) {

					ActiveSessionItem myActiveSessionItem = new ActiveSessionItem();
					myActiveSessionItem.setSessionStartTime(myMarketSession.getSessionStartTime());
					myActiveSessionItem.setSessionEndTime(myMarketSession.getSessionEndTime());
					myActiveSessionItem.setDeliveryStartTime(myMarketSession.getDeliveryStartTime());
					myActiveSessionItem.setDeliveryEndTime(myMarketSession.getDeliveryEndTime());
					myActiveSessionItemList.add(myActiveSessionItem);

				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// return the array complete
		log.debug("<- getActiveSessions");
		return myActiveSessionItemList;
	}

}
