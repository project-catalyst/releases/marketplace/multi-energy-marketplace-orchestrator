package eu.catalyst.memo.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.jpa.MarketplaceParticipantFacade;
import eu.catalyst.memo.jpa.ParticipantFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.model.MarketplaceParticipant;
import eu.catalyst.memo.model.Participant;
import eu.catalyst.memo.payload.RegistrationRequest;
import eu.catalyst.memo.payload.ActorId;
import eu.catalyst.memo.payload.ParticipantAID;
import eu.catalyst.memo.utilities.Utilities;

@Stateless
@Path("/marketparticipant/")
public class ParticipantRegistration {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ParticipantRegistration.class);

	public ParticipantRegistration() {

	}

	@POST
	@Path("request/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void postRegistrationRequest(RegistrationRequest registrationRequest) {

		log.debug("-> postRegistrationRequest");
		log.debug("registrationRequest: " + registrationRequest);

		// save participant information on the Participant table

		Participant myParticipant = new Participant();

		myParticipant.setFirstName(registrationRequest.getFirstName());
		myParticipant.setLastName(registrationRequest.getLastName());
		myParticipant.setUsername(registrationRequest.getUsername());
		myParticipant.setEmail(registrationRequest.getEmail());
		myParticipant.setPhone(registrationRequest.getPhone());
		myParticipant.setVat(registrationRequest.getVat());
		myParticipant.setCompanyName(registrationRequest.getCompanyName());
		myParticipant.setActorTypeId(registrationRequest.getActorTypeId());
		myParticipant.setStatus(registrationRequest.getStatus());

		ParticipantFacade.setParticipant(myParticipant);

		// for each market included in the marketRegistered field
		for (Integer marketRegistered : registrationRequest.getMarketRegistered()) {

			// reads from the Component Marketplace table the relative IB url and
			MarketplaceComponent myMarketplaceComponent = MarketplaceComponentFacade
					.getMarketplaceComponent(marketRegistered);

			if (myMarketplaceComponent != null) {
				// invokes the service requestAID() (url:/marketparticipant/details/)
				try {
					String informationBrokerServerUrl = myMarketplaceComponent.getUrl();
					URL url = new URL(informationBrokerServerUrl + "/marketparticipant/details/");
					log.debug("Start - Rest url: " + url.toString());
					HttpURLConnection connService = (HttpURLConnection) url.openConnection();
					connService.setDoOutput(true);
					connService.setRequestMethod("POST");
					connService.setRequestProperty("Content-Type", "application/json");
					connService.setRequestProperty("Accept", "application/json");
//					String keyIB = myMarketplaceComponent.getType() + "," + myMarketplaceComponent.getForm();
//					String tokenIB = Utilities.TOKEN_AUTHORIZATION_IB.get(keyIB);
					String tokenMEMO = Utilities.getToken(myMarketplaceComponent.getType(),
							myMarketplaceComponent.getForm(), Utilities.MEMO_COMPONENT_NAME);
					log.debug("token MEMO: " + tokenMEMO);
					connService.setRequestProperty("Authorization", tokenMEMO);

					// -----------------------------------------------------------------------
					GsonBuilder gb = new GsonBuilder();
					Gson gson = gb.create();
					String record = gson.toJson(myParticipant);
					log.debug("Rest Request: " + record);
					OutputStream os = connService.getOutputStream();
					os.write(record.getBytes());
					int myResponseCode = connService.getResponseCode();
					log.debug("HTTP error code :" + myResponseCode);
					// ---------------------------------------
					// receives the actorID info
					ActorId myActorId = new ActorId();

					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
					Type listType = new TypeToken<ActorId>() {
					}.getType();
					gb = new GsonBuilder();
					gson = gb.create();
					myActorId = gson.fromJson(br, listType);
					log.debug("payload received: " + gson.toJson(myActorId));
					os.flush();
					os.close();
					br.close();
					connService.disconnect();
					// ---------------------------------------
					// Adds it to the Participant info
					// save the correlation information of the markets on the table
					// MarketplaceParticipant
					MarketplaceParticipantFacade.setMarketplaceParticipant(registrationRequest.getUsername(),
							myMarketplaceComponent.getId(), myActorId.getActorId());
					// ---------------------------------------
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		log.debug("<- postRegistrationRequest");
		return;
	}

//	@PUT
//	@Path("request/")
//	@Consumes({ "application/json", "application/xml" })
//	@Produces({ "application/json", "application/xml" })
//	public void putPostValidationRequest(RegistrationRequest registrationRequest) {
//
//		log.debug("-> putPostValidationRequest");
//		log.debug("registrationRequest: " + registrationRequest);
//
//		// save participant information on the Participant table
//
//		ParticipantFacade.modifyParticipant(registrationRequest.getFirstName(),
//				registrationRequest.getLastName(), registrationRequest.getUsername(),
//				registrationRequest.getEmail(), registrationRequest.getPhone(), registrationRequest.getVat(),
//				registrationRequest.getCompanyName(), registrationRequest.getActorTypeId(),
//				registrationRequest.getStatus());
//
//		ParticipantAID myParticipantAID = new ParticipantAID();
//		myParticipantAID.setFirstName(registrationRequest.getFirstName());
//		myParticipantAID.setLastName(registrationRequest.getLastName());
//		myParticipantAID.setUsername(registrationRequest.getUsername());
//		myParticipantAID.setEmail(registrationRequest.getEmail());
//		myParticipantAID.setPhone(registrationRequest.getPhone());
//		myParticipantAID.setVat(registrationRequest.getVat());
//		myParticipantAID.setCompanyName(registrationRequest.getCompanyName());
//		myParticipantAID.setActorTypeId(registrationRequest.getActorTypeId());
//		myParticipantAID.setStatus(registrationRequest.getStatus());
//
//		// for each market included in the marketRegistered field
//		for (Integer marketRegistered : registrationRequest.getMarketRegistered()) {
//
//			// reads from the Component Marketplace table the relative IB url and
//			MarketplaceComponent myMarketplaceComponent = MarketplaceComponentFacade
//					.getMarketplaceComponent(marketRegistered);
//
//			// invokes the service requestAID() (url:/marketparticipant/details/)
//			try {
//				String informationBrokerServerUrl = myMarketplaceComponent.getUrl();
//				URL url = new URL(informationBrokerServerUrl + "/marketparticipant/details/");
//				log.debug("Start - Rest url: " + url.toString());
//				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
//				connService.setDoOutput(true);
//				connService.setRequestMethod("PUT");
//				connService.setRequestProperty("Content-Type", "application/json");
//				connService.setRequestProperty("Accept", "application/json");
////				String keyIB = myMarketplaceComponent.getType() + "," + myMarketplaceComponent.getForm();
////				String tokenIB = Utilities.TOKEN_AUTHORIZATION_IB.get(keyIB);
//				String tokenMEMO = Utilities.getToken(myMarketplaceComponent.getType(),
//						myMarketplaceComponent.getForm(), Utilities.MEMO_COMPONENT_NAME);
//				log.debug("token MEMO: " + tokenMEMO);
//				connService.setRequestProperty("Authorization", tokenMEMO);
//
//				// -----------------------------------------------------------------------
//				GsonBuilder gb = new GsonBuilder();
//				Gson gson = gb.create();
//				MarketplaceParticipant myMarketplaceParticipant = MarketplaceParticipantFacade
//						.getMarketplaceParticipant(registrationRequest.getUsername(), marketRegistered);
//				myParticipantAID.setActorId(myMarketplaceParticipant.getActorId());
//				// setting myParticipantAID
//				String record = gson.toJson(myParticipantAID);
//				log.debug("Rest Request: " + record);
//				OutputStream os = connService.getOutputStream();
//				os.write(record.getBytes());
//				int myResponseCode = connService.getResponseCode();
//				log.debug("HTTP error code :" + myResponseCode);
//				// ---------------------------------------
//				os.flush();
//				os.close();
//				connService.disconnect();
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//		}
//
//		log.debug("<- putPostValidationRequest");
//		return;
//	}

	@GET
	@Path("all/requests/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public List<RegistrationRequest> getRegistrationRequest() {

		log.debug("-> getRegistrationRequest");

		List<RegistrationRequest> myListRegistrationRequest = new ArrayList<RegistrationRequest>();

		// legge tutte le righe della tabella Participant
		List<Participant> myListParticipant = ParticipantFacade.findAllEntities();

		for (Participant myParticipant : myListParticipant) {

			List<Integer> myMarketRegistered = new ArrayList<Integer>();
			// per ogni riga legge le righe della tabella MarkeplaceParticipant in join con
			// Username
			// estrai la lista dei campi ib_id e crea un vettore
			List<MarketplaceParticipant> myListMarketplaceParticipant = MarketplaceParticipantFacade
					.findEntitiesByUsername(myParticipant.getUsername());
			// crea un nuovo elemento della lista ListRegistrationRequest e lo inserisce
			for (MarketplaceParticipant myMarketplaceParticipant : myListMarketplaceParticipant) {
				myMarketRegistered.add(myMarketplaceParticipant.getIbId());
			}

			RegistrationRequest myRegistrationRequest = new RegistrationRequest();
			myRegistrationRequest.setFirstName(myParticipant.getFirstName());
			myRegistrationRequest.setLastName(myParticipant.getLastName());
			myRegistrationRequest.setUsername(myParticipant.getUsername());
			myRegistrationRequest.setEmail(myParticipant.getEmail());
			myRegistrationRequest.setPhone(myParticipant.getPhone());
			myRegistrationRequest.setVat(myParticipant.getVat());
			myRegistrationRequest.setCompanyName(myParticipant.getCompanyName());
			myRegistrationRequest.setActorTypeId(myParticipant.getActorTypeId());
			myRegistrationRequest.setMarketRegistered(myMarketRegistered);
			myRegistrationRequest.setStatus(myParticipant.getStatus());
			myListRegistrationRequest.add(myRegistrationRequest);
		}

		log.debug("<- getRegistrationRequest");
		return myListRegistrationRequest;
	}
}
