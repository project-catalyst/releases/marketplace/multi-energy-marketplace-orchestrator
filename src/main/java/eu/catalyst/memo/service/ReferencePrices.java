package eu.catalyst.memo.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import eu.catalyst.memo.payload.ReferencePricePreviousDaysItem;
import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.payload.ReferencePriceItem;
import eu.catalyst.memo.utilities.Utilities;
import java.util.Date;
import java.util.GregorianCalendar;

@Stateless
@Path("/referencePricesPreviousDay/")
public class ReferencePrices {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReferencePrices.class);

	public ReferencePrices() {

	}

	@GET
	@Path("{marketActorName}/{marketplaceForm}/{timeframe}/")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	// example:
	// /referencePricesPreviusDay/participant_el_mc/marketActorName/electric_energy/intra_day/
	// <marketActorName>: username of the participant using the connector
	// <marketplaceForm>: {electric_energy, thermal_energy,it_load,
	// congestion_management, reactive_power_compensation,
	// spinning_reserve,scheduling}
	// <timeframe>: {intra_day, day_ahead}
	// Returns a list of 24 reference prices each relative to an interval of one
	// hour.

	public List<ReferencePriceItem> getReferencePricesPreviousDay(@PathParam("marketActorName") String marketActorName,
			@PathParam("marketplaceForm") String marketplaceForm, @PathParam("timeframe") String timeframe) {

		log.debug("-> getReferencePricesPreviousDay");
		log.debug("marketActorName:" + marketActorName);
		log.debug("marketplaceForm:" + marketplaceForm);
		log.debug("timeframe:" + timeframe);

		List<ReferencePriceItem> myReferencePriceList = new ArrayList<ReferencePriceItem>();

		String form = Utilities.normalizeForm(marketplaceForm);

		// estrai URL del MarketplaceComponent
		List<MarketplaceComponent> myMarketplaceComponentListIB = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentListIB = MarketplaceComponentFacade.findEntitiesByFormComponent(form,
				Utilities.IB_COMPONENT_NAME);

		if (!myMarketplaceComponentListIB.isEmpty()) {
			String ibURL = myMarketplaceComponentListIB.get(0).getUrl();
			String type = myMarketplaceComponentListIB.get(0).getType();
			// estrai token relativo all IB
			String ibToken = Utilities.getToken(type, form, Utilities.MEMO_COMPONENT_NAME);

			// long milliseconds = new Date().getTime();
			GregorianCalendar gc = new GregorianCalendar();
			// gc.add(Calendar.DAY_OF_YEAR, -1);
			gc.set(Calendar.HOUR_OF_DAY, 0);
			gc.set(Calendar.MINUTE, 0);
			gc.set(Calendar.SECOND, 0);
			gc.set(Calendar.MILLISECOND, 0);
			long milliseconds = gc.getTimeInMillis();

			// call service of IB-x
			// "/marketplace/{marketPlaceID}/form/{marketForm}/reference-prices-previous-days/date/{millisecondsTimestamp}/"

			try {
				int marketplaceid = Utilities.invokeGetMarketplaceByFormTimeframe(ibURL, ibToken, marketplaceForm,
						timeframe);
				List<ReferencePricePreviousDaysItem> myReferencePricesPreviousDays = Utilities
						.invokeGetReferencePricesPreviousDays(ibURL, ibToken, marketplaceid, marketplaceForm,
								milliseconds);

				// Date dayDate = myReferencePricesPreviousDays.get(0).getValidityStartTime();

				// fill the 24 items in the array with price zeros
				for (int slot = 0; slot < 24; slot++) {
					// crea uno slot vuoto
					ReferencePriceItem myReferencePriceItem = new ReferencePriceItem();
					myReferencePriceItem.initializeSlot(slot);
					// inserisce lo slot nella lista
					myReferencePriceList.add(myReferencePriceItem);
				}

				for (ReferencePricePreviousDaysItem myReferencePricePreviousDaysItem : myReferencePricesPreviousDays) {
					// determino l'indice inferiore inferiore = validityStartTime.HH
					// determino l'indice superiore = validityEndTime.HH-1 se validityEndTime.MM =
					// 00 and validityEndTime.SS = 00;
					// altrimenti validityEndTime.HH
					// per tutti gli item di indice € inf e sup:
					//
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(myReferencePricePreviousDaysItem.getValidityStartTime());
					int slotMin = calendar.get(Calendar.HOUR_OF_DAY);
					calendar.setTime(myReferencePricePreviousDaysItem.getValidityEndTime());
					int slotMax = calendar.get(Calendar.HOUR_OF_DAY);
					if (calendar.get(Calendar.MINUTE) == 0 && calendar.get(Calendar.SECOND) == 0
							&& calendar.get(Calendar.MILLISECOND) == 0) {
						slotMax--;
					}
					for (int slot = slotMin; slot <= slotMax; slot++) {
						BigDecimal myPrice = myReferencePriceList.get(slot).getReferencePrice();
						BigDecimal newPrice = myReferencePricePreviousDaysItem.getPrice();
						if (myPrice.compareTo(BigDecimal.ZERO) == 0) {
							myReferencePriceList.get(slot).setReferencePrice(newPrice);
						} else {
							BigDecimal maxPrice = myPrice.max(newPrice);
							myReferencePriceList.get(slot).setReferencePrice(maxPrice);
						}
					}
				}

				// search first Price not zero
				BigDecimal priceCurrent = BigDecimal.ZERO;
				for (int slot = 0; slot < 24 && priceCurrent.compareTo(BigDecimal.ZERO) == 0; slot++) {
					if (myReferencePriceList.get(slot).getReferencePrice().compareTo(BigDecimal.ZERO) > 0) {
						priceCurrent = myReferencePriceList.get(slot).getReferencePrice();
					}
				}

				// fill the gap in the array
				for (ReferencePriceItem myReferencePriceItem : myReferencePriceList) {
					if (myReferencePriceItem.getReferencePrice().compareTo(BigDecimal.ZERO) == 0) {
						myReferencePriceItem.setReferencePrice(priceCurrent);
					} else {
						priceCurrent = myReferencePriceItem.getReferencePrice();
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// return the array complete
		log.debug("<- getReferencePricesPreviousDay");
		return myReferencePriceList;
	}

}
