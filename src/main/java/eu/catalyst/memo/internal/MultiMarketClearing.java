package eu.catalyst.memo.internal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.memo.jpa.ConstraintFacade;
import eu.catalyst.memo.jpa.MarketactionCorrelationFacade;
import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.Constraint;
import eu.catalyst.memo.model.MarketactionCorrelation;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.payload.MarketAction;
import eu.catalyst.memo.payload.MarketSession;
import eu.catalyst.memo.payload.PayloadMEMO2MBMNotFlex;
import eu.catalyst.memo.payload.PayloadMEMO2MCMNotFlex;
import eu.catalyst.memo.payload.PrioritisedAction;
import eu.catalyst.memo.utilities.Utilities;

public class MultiMarketClearing {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MultiMarketClearing.class);

	public MultiMarketClearing() {

	}

	static public void executeMultiMarketClearing(List<MarketSession> marketSessionNotFlexibilityList) {

		log.debug("-> executeMultiMarketClearing");

		// Riceve la lista delle sessioni chiuse di mercati not flexibility

		// Effettua uno split delle sessioni per Time Variant ("intra_day", "day_ahead")

		List<MarketSession> marketSessionIntraDayList = new ArrayList<MarketSession>();

		List<MarketSession> marketSessionDayAheadList = new ArrayList<MarketSession>();

		for (Iterator<MarketSession> iterator = marketSessionNotFlexibilityList.iterator(); iterator.hasNext();) {
			MarketSession myMarketSession = iterator.next();
			String myTimeframeType = myMarketSession.getMarketplaceid().getMarketServiceTypeid().getTimeFrameid()
					.getType();
			log.debug("myTimeframeType: " + myTimeframeType);
			if (myTimeframeType.equals(Utilities.TIMEFRAME_TYPE_INTRA_DAY)) {
				marketSessionIntraDayList.add(myMarketSession);
			} else if (myTimeframeType.equals(Utilities.TIMEFRAME_TYPE_DAY_AHEAD)) {
				marketSessionDayAheadList.add(myMarketSession);
			}
		}

		// chiama executeMultiMarketClearingTimeVariant per ciascuna lista di sessioni
		if (!marketSessionIntraDayList.isEmpty()) {
			executeMultiMarketClearingTimeVariant(marketSessionIntraDayList);
		}

		if (!marketSessionDayAheadList.isEmpty()) {
			executeMultiMarketClearingTimeVariant(marketSessionDayAheadList);
		}

		log.debug("<- executeMultiMarketClearing");
		return;
	}

	static public void executeMultiMarketClearingTimeVariant(List<MarketSession> marketSessionList) {

		log.debug("-> executeMultiMarketClearingTimeVariant");

		// Prepara la lista di PayloadMemo2MCM
		List<PayloadMEMO2MCMNotFlex> myPayloadMEMO2MCMNotFlexlist = new ArrayList<PayloadMEMO2MCMNotFlex>();

		// Effettua il ciclo per ciascuna sessione di mercato
		for (Iterator<MarketSession> iterator = marketSessionList.iterator(); iterator.hasNext();) {
			MarketSession myMarketSession = iterator.next();

			PayloadMEMO2MCMNotFlex myPayloadMEMO2MCMNotFlex = new PayloadMEMO2MCMNotFlex();

			String myMarketServiceType = myMarketSession.getMarketplaceid().getMarketServiceTypeid().getType();
			String mySessionform = myMarketSession.getFormid().getForm();

			log.debug("myMarketServiceType: " + myMarketServiceType);
			log.debug("mySessionform: " + mySessionform);

			// estrai URL del MarketplaceComponent
			List<MarketplaceComponent> myMarketplaceComponentListMCM = new ArrayList<MarketplaceComponent>();
			myMarketplaceComponentListMCM = MarketplaceComponentFacade
					.findEntitiesByTypeFormComponent(myMarketServiceType, mySessionform, Utilities.MCM_COMPONENT_NAME);
			String marketClearingManagerUrl = myMarketplaceComponentListMCM.get(0).getUrl();

			// estrai URL del MarketplaceComponent
			List<MarketplaceComponent> myMarketplaceComponentListIB = new ArrayList<MarketplaceComponent>();
			myMarketplaceComponentListIB = MarketplaceComponentFacade
					.findEntitiesByTypeFormComponent(myMarketServiceType, mySessionform, Utilities.IB_COMPONENT_NAME);
			String informationBrokerServerUrl = myMarketplaceComponentListIB.get(0).getUrl();
			Integer informationBrokerId = myMarketplaceComponentListIB.get(0).getId();

			// estrai token relativo all IB
			String tokenMEMO = Utilities.getToken(myMarketServiceType, mySessionform, Utilities.MEMO_COMPONENT_NAME);

			String tokenMCM = Utilities.getToken(myMarketServiceType, mySessionform, Utilities.MCM_COMPONENT_NAME);

			myPayloadMEMO2MCMNotFlex.setMarketSession(myMarketSession);

			myPayloadMEMO2MCMNotFlex.setInformationBrokerServerUrl(informationBrokerServerUrl);
			myPayloadMEMO2MCMNotFlex.setInformationBrokerId(informationBrokerId);
			myPayloadMEMO2MCMNotFlex.setTokenMEMO(tokenMEMO);
			myPayloadMEMO2MCMNotFlex.setTokenMCM(tokenMCM);
			myPayloadMEMO2MCMNotFlex.setMarketClearingManagerUrl(marketClearingManagerUrl);
			myPayloadMEMO2MCMNotFlex.setMarketSession(myMarketSession);

			// Chiede al IBi (relativo al mercato corrente) tutte le marketaction valide per
			// la sessione corrente
			List<MarketAction> validActionsOfSessionList = invokeGetgetValidActionsOfSession(myPayloadMEMO2MCMNotFlex);

			// Elabora tutte le azioni e inserisce la priorità considerando la cardinalità
			// dei constraint (numero di marketaction correlate)
			List<PrioritisedAction> myprioritisedActionsList = calculatePrioritisedActionsList(
					validActionsOfSessionList, myPayloadMEMO2MCMNotFlex);
			myPayloadMEMO2MCMNotFlex.setPrioritisedActionsList(myprioritisedActionsList);

			// Ordina le azioni in funzione della priorità assegnata (lo fa il clearing
			// internamente !!)

			// Invoca il servizio di Clearing al MCMi relativo al mercato corrente passando
			// la lista delle marketaction prioritizzate
			// ritorna la lista delle marketaction cleared, la lista delle counteroffer e il
			// clearingprice
			PayloadMEMO2MCMNotFlex outputPayloadMEMO2MCMNotFlex = invokeClearingProcess(myPayloadMEMO2MCMNotFlex);

			// aggiorna myPayloadMEMO2MCMNotFlex con le info da outputPayloadMEMO2MCM
			myPayloadMEMO2MCMNotFlex
					.setPrioritisedActionsList(outputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList());
			myPayloadMEMO2MCMNotFlex
					.setMarketActionCounterOfferList(outputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList());
			myPayloadMEMO2MCMNotFlex.setClearingPrice(outputPayloadMEMO2MCMNotFlex.getClearingPrice());

			// segna la sessione come cleared (potrebe farlo il clearing ?)
			// outputPayloadMEMO2MCMNotFlex.getMarketSession().getSessionStatusid().setId(4);
			myPayloadMEMO2MCMNotFlex.setMarketSession(outputPayloadMEMO2MCMNotFlex.getMarketSession());

			// accoda myPayloadMEMO2MCMNotFlexlist aggiornato
			myPayloadMEMO2MCMNotFlexlist.add(myPayloadMEMO2MCMNotFlex);

			// Fine Ciclo (per ciascuna sessione di mercato)
		}

		// ------------------------------------------------------------------

		// Esegue la verifica dei constraint relativi alle marketaction cleared
		checkConstraints(myPayloadMEMO2MCMNotFlexlist);

		// ------------------------------------------------------------------
		// Esegui il ciclo mentre esiste almeno una marketsession not cleared
		// Effettua il ciclo per tutte le marketsession not cleared
		while (existSessionToClearing(myPayloadMEMO2MCMNotFlexlist)) {
			for (Iterator<PayloadMEMO2MCMNotFlex> iterator = myPayloadMEMO2MCMNotFlexlist.iterator(); iterator
					.hasNext();) {
				PayloadMEMO2MCMNotFlex myPayloadMEMO2MCMNotFlex = iterator.next();
				if (myPayloadMEMO2MCMNotFlex.getMarketSession().getSessionStatusid().getId() == 6) { // to clearing)

					// * Invoca il servizio di Clearing al MCMi relativo al mercato corrente
					// passando la lista delle marketaction prioritizzate
					// * ritorna la lista delle marketaction cleared, la lista delle counteroffer e
					// il clearingprice
					PayloadMEMO2MCMNotFlex outputPayloadMEMO2MCMNotFlex = invokeClearingProcess(
							myPayloadMEMO2MCMNotFlex);
					myPayloadMEMO2MCMNotFlex
							.setPrioritisedActionsList(outputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList());
					myPayloadMEMO2MCMNotFlex.setMarketActionCounterOfferList(
							outputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList());
					myPayloadMEMO2MCMNotFlex.setClearingPrice(outputPayloadMEMO2MCMNotFlex.getClearingPrice());
					myPayloadMEMO2MCMNotFlex.setMarketSession(outputPayloadMEMO2MCMNotFlex.getMarketSession());
				}

			}
			// Esegue la verifica dei constraint relativi alle marketaction cleared
			// Rimuove le marketaction che violano i constraint
			// Identifica le marketsession non ancora cleared e quelle cleared
			checkConstraints(myPayloadMEMO2MCMNotFlexlist);
		}

		normalizeMarketActions(myPayloadMEMO2MCMNotFlexlist);

		// Effettua il ciclo per ciascuna marketsession
		for (Iterator<PayloadMEMO2MCMNotFlex> iterator = myPayloadMEMO2MCMNotFlexlist.iterator(); iterator.hasNext();) {
			PayloadMEMO2MCMNotFlex myPayloadMEMO2MCMNotFlex = iterator.next();

			// Preleva i risultati finali del processo di Clearing del mercato corrente
			// Invoca il servizio di PostClearing al MCMi relativo al mercato corrente

			PayloadMEMO2MCMNotFlex outputPayloadMEMO2MCMNotFlex = invokePostClearingProcess(myPayloadMEMO2MCMNotFlex);

			PayloadMEMO2MBMNotFlex inputPayloadMEMO2MBMNotFlex = new PayloadMEMO2MBMNotFlex();
			inputPayloadMEMO2MBMNotFlex
					.setInformationBrokerServerUrl(outputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl());
			inputPayloadMEMO2MBMNotFlex.setTokenMEMO(outputPayloadMEMO2MCMNotFlex.getTokenMEMO());

			// set tokenMBM
			String myType = outputPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid().getMarketServiceTypeid()
					.getType();
			String myForm = outputPayloadMEMO2MCMNotFlex.getMarketSession().getFormid().getForm();
			String tokenMBM = Utilities.getToken(myType, myForm, Utilities.MBM_COMPONENT_NAME);
			inputPayloadMEMO2MBMNotFlex.setTokenMBM(tokenMBM);

			// estrai URL del MarketplaceComponent
			List<MarketplaceComponent> myMarketplaceComponentListMBM = new ArrayList<MarketplaceComponent>();
			String myMarketServiceType = outputPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid()
					.getMarketServiceTypeid().getType();
			String mySessionform = outputPayloadMEMO2MCMNotFlex.getMarketSession().getFormid().getForm();
			myMarketplaceComponentListMBM = MarketplaceComponentFacade
					.findEntitiesByTypeFormComponent(myMarketServiceType, mySessionform, Utilities.MBM_COMPONENT_NAME);
			String marketBillingManagerUrl = myMarketplaceComponentListMBM.get(0).getUrl();
			inputPayloadMEMO2MBMNotFlex.setMarketBillingManagerUrl(marketBillingManagerUrl);

			inputPayloadMEMO2MBMNotFlex
					.setPrioritisedActionsList(outputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList());
			inputPayloadMEMO2MBMNotFlex
					.setMarketActionCounterOfferList(outputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList());
			inputPayloadMEMO2MBMNotFlex.setMarketSession(outputPayloadMEMO2MCMNotFlex.getMarketSession());
			inputPayloadMEMO2MBMNotFlex.setClearingPrice(outputPayloadMEMO2MCMNotFlex.getClearingPrice());
			// invoca la fase di BillingProcess

			// Invoca il servizio di Billing al MBMi relativo al mercato corrente
			invokeBillingProcess(inputPayloadMEMO2MBMNotFlex);

		}

		// Fine Ciclo ( per ciascuna marketsession)

		log.debug("<- executeMultiMarketClearingTimeVariant");
	}

	public static boolean existSessionToClearing(List<PayloadMEMO2MCMNotFlex> inputPayloadMEMO2MCMNotFlexlist) {
		log.debug("-> existSessionToClearing");
		log.debug("inputPayloadMEMO2MCMNotFlexlist: " + new Gson().toJson(inputPayloadMEMO2MCMNotFlexlist));
		boolean returnValue = false;
		for (Iterator<PayloadMEMO2MCMNotFlex> iterator = inputPayloadMEMO2MCMNotFlexlist.iterator(); iterator
				.hasNext();) {
			PayloadMEMO2MCMNotFlex myPayloadMEMO2MCMNotFlex = iterator.next();
			if (myPayloadMEMO2MCMNotFlex.getMarketSession().getSessionStatusid().getId() == 6) { // to clearing)

				// reset gli status di tutte le marketaction allo stato valid (2) escluse quelle
				// con stato (20)
				List<PrioritisedAction> myprioritisedActionsList = myPayloadMEMO2MCMNotFlex.getPrioritisedActionsList();
				for (Iterator<PrioritisedAction> iterator2 = myprioritisedActionsList.iterator(); iterator2
						.hasNext();) {
					PrioritisedAction myPrioritisedAction = iterator2.next();
					if (myPrioritisedAction.getMarketaction().getStatusid() != 20) {
						myPrioritisedAction.getMarketaction().setStatusid(2); // valid to re-clearing
					}
					returnValue = true;
					// break;
				}
			}
		}
		log.debug("returnValue: " + new Gson().toJson(returnValue));
		log.debug("<- existSessionToClearing");
		return returnValue;
	}

	public static List<PayloadMEMO2MCMNotFlex> checkConstraints(
			List<PayloadMEMO2MCMNotFlex> inputPayloadMEMO2MCMNotFlexlist) {
		log.debug("-> checkConstraints");
		log.debug("inputPayloadMEMO2MCMNotFlexlist: " + new Gson().toJson(inputPayloadMEMO2MCMNotFlexlist));
		List<PayloadMEMO2MCMNotFlex> outputPayloadMEMO2MCMNotFlexlist = new ArrayList<PayloadMEMO2MCMNotFlex>();
		// Esegue la verifica dei constraint relativi alle marketaction cleared
		// Rimuove le marketaction che violano i constraint e segna la marketsession
		// relativa come non ancora cleared

		// Ciclo su tutte le marketsession in inputPayloadMEMO2MCMNotFlexlist
		// estrae PrioritisedActionsList
		for (Iterator<PayloadMEMO2MCMNotFlex> iterator = inputPayloadMEMO2MCMNotFlexlist.iterator(); iterator
				.hasNext();) {
			PayloadMEMO2MCMNotFlex myPayloadMEMO2MCMNotFlex = iterator.next();
			log.debug("myPayloadMEMO2MCMNotFlex: " + new Gson().toJson(myPayloadMEMO2MCMNotFlex));
			// Ciclo su ogni marketaction
			List<PrioritisedAction> myprioritisedActionsList = myPayloadMEMO2MCMNotFlex.getPrioritisedActionsList();
			for (Iterator<PrioritisedAction> iterator2 = myprioritisedActionsList.iterator(); iterator2.hasNext();) {
				PrioritisedAction myPrioritisedAction = iterator2.next();
				log.debug("myPrioritisedAction: " + new Gson().toJson(myPrioritisedAction));
				// Se lo stato è rejected (10)
				log.debug("myPrioritisedAction.getMarketaction().getStatusid(): "
						+ new Gson().toJson(myPrioritisedAction.getMarketaction().getStatusid()));
				if (myPrioritisedAction.getMarketaction().getStatusid().equals(new Integer(10))) { // rejected
					// verifica se esiste una correlazione
					MarketactionCorrelation myMarketactionCorrelation = MarketactionCorrelationFacade
							.findEntityByIbSessionAction(myPayloadMEMO2MCMNotFlex.getInformationBrokerId(),
									myPayloadMEMO2MCMNotFlex.getMarketSession().getId(),
									myPrioritisedAction.getMarketaction().getId());
					// Se esiste
					log.debug("myMarketactionCorrelation: " + new Gson().toJson(myMarketactionCorrelation));
					if (myMarketactionCorrelation != null) {
						// verifica il tipo di constraint
						// Se è di tipo "ALL OR NOTHING"
						Constraint myConstraint = ConstraintFacade
								.findEntityByCorrelationId(myMarketactionCorrelation.getCorrelationId());
						log.debug("myConstraint.getDescription(): " + new Gson().toJson(myConstraint.getDescription()));
						if (myConstraint.getDescription().equals(Utilities.CONSTRAINT_AND)) {
							// estrae la lista di marketcorrelation per findEntityByIbSessionAction(IB, SID,
							// AID)
							List<MarketactionCorrelation> myMarketactionCorrelationList = MarketactionCorrelationFacade
									.findByCorrelationId(myMarketactionCorrelation.getCorrelationId());
							// Ciclo per ogni elemento della lista
							for (Iterator<MarketactionCorrelation> iterator3 = myMarketactionCorrelationList
									.iterator(); iterator3.hasNext();) {
								MarketactionCorrelation myMarketactionCorrelation3 = iterator3.next();
								log.debug(
										"myMarketactionCorrelation3: " + new Gson().toJson(myMarketactionCorrelation3));
								// Se il SID è diverso da quello corrente
								log.debug("myMarketactionCorrelation3.getSessionId(): "
										+ new Gson().toJson(myMarketactionCorrelation3.getSessionId()));
								log.debug("myPayloadMEMO2MCMNotFlex.getMarketSession().getId(): "
										+ new Gson().toJson(myPayloadMEMO2MCMNotFlex.getMarketSession().getId()));
								log.debug("myMarketactionCorrelation3.getInformationBrokerId(): "
										+ new Gson().toJson(myMarketactionCorrelation3.getInformationBrokerId()));
								log.debug("myPayloadMEMO2MCMNotFlex.getInformationBrokerId(): "
										+ new Gson().toJson(myPayloadMEMO2MCMNotFlex.getInformationBrokerId()));
//								if (!myMarketactionCorrelation3.getSessionId().equals(myPayloadMEMO2MCMNotFlex.getMarketSession().getId())) {
								// Se il SID è diverso da quello corrente  AND i mercati sono diversi
								if (!myMarketactionCorrelation3.getSessionId().equals(myPayloadMEMO2MCMNotFlex.getMarketSession().getId())
										|| !myMarketactionCorrelation3.getInformationBrokerId().equals(myPayloadMEMO2MCMNotFlex.getInformationBrokerId())) {
									// Cerca nella lista inputPayloadMEMO2MCMNotFlexlist la sessione con SID attuale
									for (Iterator<PayloadMEMO2MCMNotFlex> iterator4 = inputPayloadMEMO2MCMNotFlexlist
											.iterator(); iterator4.hasNext();) {
										PayloadMEMO2MCMNotFlex myPayloadMEMO2MCMNotFlex4 = iterator4.next();
										log.debug("myPayloadMEMO2MCMNotFlex4: "
												+ new Gson().toJson(myPayloadMEMO2MCMNotFlex4));
										log.debug("myMarketactionCorrelation3.getSessionId(): "
												+ new Gson().toJson(myMarketactionCorrelation3.getSessionId()));
										log.debug("myPayloadMEMO2MCMNotFlex4.getMarketSession().getId(): " + new Gson()
												.toJson(myPayloadMEMO2MCMNotFlex4.getMarketSession().getId()));
										log.debug("myMarketactionCorrelation3.getInformationBrokerId(): "
												+ new Gson().toJson(myMarketactionCorrelation3.getInformationBrokerId()));
										log.debug("myPayloadMEMO2MCMNotFlex4.getInformationBrokerId(): " + new Gson()
												.toJson(myPayloadMEMO2MCMNotFlex4.getInformationBrokerId()));
										if (myMarketactionCorrelation3.getSessionId().equals(myPayloadMEMO2MCMNotFlex4.getMarketSession().getId())
												&& myMarketactionCorrelation3.getInformationBrokerId().equals(myPayloadMEMO2MCMNotFlex4.getInformationBrokerId())) {
											// Cerca nella lista PrioritisedActionsList la market action con AID attuale
											List<PrioritisedAction> myprioritisedActionsList4 = myPayloadMEMO2MCMNotFlex4
													.getPrioritisedActionsList();
											for (Iterator<PrioritisedAction> iterator5 = myprioritisedActionsList4
													.iterator(); iterator5.hasNext();) {
												PrioritisedAction myPrioritisedAction5 = iterator5.next();
												log.debug("myPrioritisedAction5: "
														+ new Gson().toJson(myPrioritisedAction5));
												log.debug("myPrioritisedAction5.getMarketaction().getId(): "
														+ new Gson().toJson(
																myPrioritisedAction5.getMarketaction().getId()));
												log.debug("myMarketactionCorrelation3.getActionId(): "
														+ new Gson().toJson(myMarketactionCorrelation3.getActionId()));
												if (myPrioritisedAction5.getMarketaction().getId().equals(myMarketactionCorrelation3.getActionId())) {
													// Se lo stato è diverso da rejected (10)
													// if (myPrioritisedAction5.getMarketaction().getStatusid() != 10) {
													// // rejected
													// aggiorna lo stato a rejected (10)
													myPrioritisedAction5.getMarketaction().setStatusid(20); // Finally
																											// Rejected
													log.debug("myPrioritisedAction5.getMarketaction(): " + new Gson()
															.toJson(myPrioritisedAction5.getMarketaction()));
													// aggiorna lo stato della sessione attuale con "to clearing"
													// (6) fittizia
													myPayloadMEMO2MCMNotFlex4.getMarketSession().getSessionStatusid()
															.setId(6);
													log.debug("myPayloadMEMO2MCMNotFlex4.getMarketSession(): "
															+ new Gson().toJson(
																	myPayloadMEMO2MCMNotFlex4.getMarketSession()));
													; // "to clearing" fittizia
														// }

												}
											}

										}
									}
								}
							}
						}

						// aggiorna lo stato della marketaction corrente a (10) "Finally Rejected"
						myPrioritisedAction.getMarketaction().setStatusid(20);
						log.debug("myPrioritisedAction.getMarketaction(): "
								+ new Gson().toJson(myPrioritisedAction.getMarketaction()));
						// aggiorna lo stato della session corrente a (6) "to clearing
						myPayloadMEMO2MCMNotFlex.getMarketSession().getSessionStatusid().setId(6);
						log.debug("myPayloadMEMO2MCMNotFlex.getMarketSession(): "
								+ new Gson().toJson(myPayloadMEMO2MCMNotFlex.getMarketSession()));
					}
				}

			}
		}
		log.debug("outputPayloadMEMO2MCMNotFlexlist: " + new Gson().toJson(outputPayloadMEMO2MCMNotFlexlist));
		log.debug("<- checkConstraints");
		return outputPayloadMEMO2MCMNotFlexlist;
	}

	public static List<PrioritisedAction> calculatePrioritisedActionsList(
			List<MarketAction> inputvalidActionsOfSessionList, PayloadMEMO2MCMNotFlex inputPayloadMEMO2MCMNotFlex) {
		log.debug("-> calculatePrioritisedActionsList");
		log.debug("inputvalidActionsOfSessionList: " + new Gson().toJson(inputvalidActionsOfSessionList));
		log.debug("inputPayloadMEMO2MCMNotFlex: " + new Gson().toJson(inputPayloadMEMO2MCMNotFlex));
		List<PrioritisedAction> returnPrioritisedActionsList = new ArrayList<PrioritisedAction>();

		// esegue un ciclo sulla lista delle azioni in input
		// per ogni azione
		for (Iterator<MarketAction> iterator = inputvalidActionsOfSessionList.iterator(); iterator.hasNext();) {
			MarketAction myMarketAction = iterator.next();
			Integer myPriority = null;
			PrioritisedAction myPrioritisedAction = new PrioritisedAction();
			myPrioritisedAction.setMarketaction(myMarketAction);

			// esegue una select su MarketactionCorrelation con ibId, sessionId e
			// marketactionId )
			MarketactionCorrelation myMarketactionCorrelation = MarketactionCorrelationFacade
					.findEntityByIbSessionAction(inputPayloadMEMO2MCMNotFlex.getInformationBrokerId(),
							inputPayloadMEMO2MCMNotFlex.getMarketSession().getId(), myMarketAction.getId());
			// Se la count = 0
			// allora imposta la priorità a 0
			// Altrimenti
			if (myMarketactionCorrelation == null) {
				myPriority = 0;
			} else {
				// usando correlationId legge la riga della tabella Correlation
				// usando constraintId legge la riga della tabella Constraint
//				Constraint myConstraint = ConstraintFacade.findEntityByIbSessionAction(
//						inputPayloadMEMO2MCMNotFlex.getInformationBrokerId(),
//						inputPayloadMEMO2MCMNotFlex.getMarketSession().getId(),
//						myMarketAction.getId());
				Constraint myConstraint = ConstraintFacade
						.findEntityByCorrelationId(myMarketactionCorrelation.getCorrelationId());
				// Se la colonna description = "AT LEAST ONE"
				// allora imposta la priorità a 1
				if (myConstraint.getDescription().equals(Utilities.CONSTRAINT_OR)) {
					myPriority = 1;
				} else {
					// esegue una count sulla tabella MarketactionCorrelation con correlationId
					// imposta la priorità alla count ritornata (> 1)
					List<MarketactionCorrelation> myMarketactionCorrelationList = MarketactionCorrelationFacade
							.findByCorrelationId(myMarketactionCorrelation.getCorrelationId());
					myPriority = myMarketactionCorrelationList.size();

				}
			}

			myPrioritisedAction.setPriority(myPriority);

			// accoda la market action corrente e la priorità calcolata su
			// returnPrioritisedActionsList
			returnPrioritisedActionsList.add(myPrioritisedAction);

		}
		// fine Ciclo (lista delle azioni in input)
		log.debug("returnPrioritisedActionsList: " + new Gson().toJson(returnPrioritisedActionsList));
		log.debug("<- calculatePrioritisedActionsList");
		return returnPrioritisedActionsList;

	}

	public static PayloadMEMO2MCMNotFlex invokePostClearingProcess(PayloadMEMO2MCMNotFlex inputPayloadMEMO2MCMNotFlex) {

		log.debug("-> invokePostClearingProcess");
		PayloadMEMO2MCMNotFlex returnPayloadMEMO2MCMNotFlex = new PayloadMEMO2MCMNotFlex();
		try {

			// Chiama il servizio ClearingProcess del componente MCM, passando come
			// parametri url e token dell IB e la lista delle marketaction to clearing con
			// la priorità

			URL url = new URL(inputPayloadMEMO2MCMNotFlex.getMarketClearingManagerUrl() + "/postClearingProcess/");

			log.debug("Rest url: " + url.toString());

			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");
			GsonBuilder gb = new GsonBuilder();

			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();

			String record = gson.toJson(inputPayloadMEMO2MCMNotFlex);

			log.debug("Rest Request: " + record);

			OutputStream os = connService.getOutputStream();
			os.write(record.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<PayloadMEMO2MCMNotFlex>() {
			}.getType();
			gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			gson = gb.create();

			returnPayloadMEMO2MCMNotFlex = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(returnPayloadMEMO2MCMNotFlex));

			br.close();
			os.flush();
			os.close();
			connService.disconnect();

		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- invokePostClearingProcess");
		return returnPayloadMEMO2MCMNotFlex;
	}

	public static PayloadMEMO2MCMNotFlex invokeClearingProcess(PayloadMEMO2MCMNotFlex inputPayloadMEMO2MCMNotFlex) {

		log.debug("-> invokeClearingProcess");
		PayloadMEMO2MCMNotFlex returnPayloadMEMO2MCMNotFlex = new PayloadMEMO2MCMNotFlex();
		try {

			// Chiama il servizio ClearingProcess del componente MCM, passando come
			// parametri url e token dell IB e la lista delle marketaction to clearing con
			// la priorità

			URL url = new URL(inputPayloadMEMO2MCMNotFlex.getMarketClearingManagerUrl() + "/clearingProcess/");

			log.debug("Rest url: " + url.toString());

			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");
			GsonBuilder gb = new GsonBuilder();

			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();

			String record = gson.toJson(inputPayloadMEMO2MCMNotFlex);

			log.debug("Rest Request: " + record);

			OutputStream os = connService.getOutputStream();
			os.write(record.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<PayloadMEMO2MCMNotFlex>() {
			}.getType();
			gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			gson = gb.create();

			returnPayloadMEMO2MCMNotFlex = gson.fromJson(br, listType);

			log.debug("payload received: " + gson.toJson(returnPayloadMEMO2MCMNotFlex));

			os.flush();
			os.close();
			br.close();
			connService.disconnect();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- invokeClearingProcess");
		return returnPayloadMEMO2MCMNotFlex;
	}

	public static void invokeBillingProcess(PayloadMEMO2MBMNotFlex inputPayloadMEMO2MBMNotFlex) {

		log.debug("-> invokeBillingProcess");
		try {

			// Chiama il servizio ClearingProcess del componente MCM, passando come
			// parametri url e token dell IB e la lista delle marketaction to clearing con
			// la priorità

			URL url = new URL(inputPayloadMEMO2MBMNotFlex.getMarketBillingManagerUrl() + "/billingProcess/");

			log.debug("Rest url: " + url.toString());

			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");
			GsonBuilder gb = new GsonBuilder();

			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();

			String record = gson.toJson(inputPayloadMEMO2MBMNotFlex);

			log.debug("Rest Request: " + record);

			OutputStream os = connService.getOutputStream();
			os.write(record.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			os.flush();
			os.close();
			connService.disconnect();

		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- invokeClearingProcess");
		return;
	}

	static public List<MarketAction> invokeGetgetValidActionsOfSession(
			PayloadMEMO2MCMNotFlex myPayloadMEMO2MCMNotFlex) {

		log.debug("-> invokeGetgetValidActionsOfSession");
		ArrayList<MarketAction> returnMarketActionNotBilledList = new ArrayList<MarketAction>();

		try {

			URL url = new URL(myPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl() + "/marketplace/"
					+ myPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid().getId().intValue()
					+ "/marketsessions/" + myPayloadMEMO2MCMNotFlex.getMarketSession().getId().intValue()
					+ "/actions/valid/");
			log.debug("Start - Rest url: " + url.toString());
			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(false);
			connService.setRequestMethod("GET");
			connService.setRequestProperty("Content-Type", "application/json");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Authorization", myPayloadMEMO2MCMNotFlex.getTokenMEMO());

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<ArrayList<MarketAction>>() {
			}.getType();
			GsonBuilder gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();
			returnMarketActionNotBilledList = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(returnMarketActionNotBilledList));
			br.close();
			connService.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("<- invokeGetgetValidActionsOfSession");
		return returnMarketActionNotBilledList;
	}

	public static void normalizeMarketActions(List<PayloadMEMO2MCMNotFlex> inputPayloadMEMO2MCMNotFlexlist) {
		log.debug("-> normalizeMarketActions");

		for (Iterator<PayloadMEMO2MCMNotFlex> iterator = inputPayloadMEMO2MCMNotFlexlist.iterator(); iterator
				.hasNext();) {
			PayloadMEMO2MCMNotFlex myPayloadMEMO2MCMNotFlex = iterator.next();

			List<PrioritisedAction> myprioritisedActionsList = myPayloadMEMO2MCMNotFlex.getPrioritisedActionsList();
			for (Iterator<PrioritisedAction> iterator2 = myprioritisedActionsList.iterator(); iterator2.hasNext();) {
				PrioritisedAction myPrioritisedAction = iterator2.next();
				if (myPrioritisedAction.getMarketaction().getStatusid() == 20) { // finally rejected
					myPrioritisedAction.getMarketaction().setStatusid(10); // rejected
				}

			}
		}
		log.debug("<- normalizeMarketActions");
	}

}
