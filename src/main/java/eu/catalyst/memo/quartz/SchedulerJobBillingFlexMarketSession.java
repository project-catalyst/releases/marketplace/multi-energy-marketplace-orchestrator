package eu.catalyst.memo.quartz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.memo.payload.MarketSession;
import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.jpa.SystemParameterFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.payload.MarketAction;
import eu.catalyst.memo.payload.PayloadMEMO2MBM;
import eu.catalyst.memo.payload.PayloadMEMO2MCM;
import eu.catalyst.memo.utilities.Utilities;

public class SchedulerJobBillingFlexMarketSession implements Job {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(SchedulerJobBillingFlexMarketSession.class);

	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.debug("-> SchedulerJobBillingFlexMarketSession-execute");
		// estrai URL del MarketplaceComponent (type="ancillary_services",
		// component="MCM")
		List<MarketplaceComponent> myMarketplaceComponentListMCM = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentListMCM = MarketplaceComponentFacade.findEntitiesByTypeComponent(Utilities.FLEX_TYPE_NAME_1,
				Utilities.MCM_COMPONENT_NAME);

		String marketClearingManagerUrl = null;
		if (!myMarketplaceComponentListMCM.isEmpty()) {
			marketClearingManagerUrl = myMarketplaceComponentListMCM.get(0).getUrl();
		}

		// estrai URL del MarketplaceComponent (type="ancillary_services",
		// component="MBM")
		List<MarketplaceComponent> myMarketplaceComponentListMBM = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentListMBM = MarketplaceComponentFacade.findEntitiesByTypeComponent(Utilities.FLEX_TYPE_NAME_1,
				Utilities.MBM_COMPONENT_NAME);

		String marketBillingManagerUrl = null;
		if (!myMarketplaceComponentListMBM.isEmpty()) {
			marketBillingManagerUrl = myMarketplaceComponentListMBM.get(0).getUrl();
		}

		// estrai URL del MarketplaceComponent (type="ancillary_services",
		// component="IB")
		List<MarketplaceComponent> myMarketplaceComponentListIB = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentListIB = MarketplaceComponentFacade.findEntitiesByTypeComponent(Utilities.FLEX_TYPE_NAME_1,
				Utilities.IB_COMPONENT_NAME);

		String informationBrokerServerUrl = null;
		if (!myMarketplaceComponentListIB.isEmpty()) {
			informationBrokerServerUrl = myMarketplaceComponentListIB.get(0).getUrl();
		}

		if (marketClearingManagerUrl != null && marketBillingManagerUrl != null && informationBrokerServerUrl != null) {
			// estrai token relativo all IB
			// String keyIB =
			// Utilities.makeKeyIB("ancillary_services","ancillary_services");
			// String tokenIB = Utilities.TOKEN_AUTHORIZATION_IB.get(keyIB);
			String tokenMEMO = Utilities.getToken(Utilities.FLEX_TYPE_NAME_1, Utilities.FLEX_TYPE_NAME_1,
					Utilities.MEMO_COMPONENT_NAME);
			String tokenMCM = Utilities.getToken(Utilities.FLEX_TYPE_NAME_1, Utilities.FLEX_TYPE_NAME_1,
					Utilities.MCM_COMPONENT_NAME);

			// imposta PayloadMEMO2MCM
			PayloadMEMO2MCM inputPayloadMEMO2MCM = new PayloadMEMO2MCM();

			inputPayloadMEMO2MCM.setInformationBrokerServerUrl(informationBrokerServerUrl);
			inputPayloadMEMO2MCM.setMarketClearingManagerUrl(marketClearingManagerUrl);
			inputPayloadMEMO2MCM.setTokenMEMO(tokenMEMO);
			inputPayloadMEMO2MCM.setTokenMCM(tokenMCM);

			// invoca la fase di PostClearingProcess
			PayloadMEMO2MCM outputPayloadMEMO2MCM = invokePostClearingProcess(inputPayloadMEMO2MCM);

			String tokenMBM = Utilities.getToken(Utilities.FLEX_TYPE_NAME_1, Utilities.FLEX_TYPE_NAME_1,
					Utilities.MBM_COMPONENT_NAME);

			PayloadMEMO2MBM inputPayloadMEMO2MBM = new PayloadMEMO2MBM();
			inputPayloadMEMO2MBM.setInformationBrokerServerUrl(informationBrokerServerUrl);
			inputPayloadMEMO2MBM.setTokenMEMO(tokenMEMO);
			inputPayloadMEMO2MBM.setTokenMBM(tokenMBM);
			inputPayloadMEMO2MBM.setMarketBillingManagerUrl(marketBillingManagerUrl);
			inputPayloadMEMO2MBM.setMarketActionList(outputPayloadMEMO2MCM.getMarketActionList());
			inputPayloadMEMO2MBM
					.setMarketActionCounterOfferList(outputPayloadMEMO2MCM.getMarketActionCounterOfferList());

			String paramFixedFee = SystemParameterFacade.findEntityByName(Utilities.PARAM_FIXED_FEE).getValue();
			String paramPenalty = SystemParameterFacade.findEntityByName(Utilities.PARAM_PENALTY).getValue();
			inputPayloadMEMO2MBM.setFixedFee(new BigDecimal(paramFixedFee));
			inputPayloadMEMO2MBM.setPenalty(new BigDecimal(paramPenalty));

//			MarketSession myMarketSession = null;
//			try {
//				myMarketSession = Utilities.invokeGetMarketSession(informationBrokerServerUrl, tokenMEMO,
//						outputPayloadMEMO2MCM.getMarketActionList().get(0).getMarketSessionid().intValue());
//				inputPayloadMEMO2MBM.setMarketSession(myMarketSession);
//			} catch (
			//
//			Exception e) {
//				e.printStackTrace();
//			}
//			inputPayloadMEMO2MBM.setMarketSession(myMarketSession);
//			// invoca la fase di BillingProcess
			invokeBillingProcess(inputPayloadMEMO2MBM);
		}

		log.debug("<- SchedulerJobBillingFlexMarketSession-execute");
	}

	public PayloadMEMO2MCM invokePostClearingProcess(PayloadMEMO2MCM inputPayloadMEMO2MCM) {

		log.debug("-> invokePostClearingProcess");
		PayloadMEMO2MCM returnPayloadMEMO2MCM = new PayloadMEMO2MCM();
		try {

			// estrai le marketaction not billed

			List<MarketAction> marketActionNotBilledList = invokeGetActionNotBilled(inputPayloadMEMO2MCM);
			inputPayloadMEMO2MCM.setMarketActionList(marketActionNotBilledList);

			// Chiama il servizio postClearingProcess del componente MCM, passando come
			// parametri url e token dell IB e la lista delle marketaction not billed
			// Riceve la lista delle counteroffers per il processo di Billing successivo

			URL url = new URL(inputPayloadMEMO2MCM.getMarketClearingManagerUrl() + "/postClearingProcess/");

			log.debug("Rest url: " + url.toString());

			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");
			GsonBuilder gb = new GsonBuilder();

			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();

			String record = gson.toJson(inputPayloadMEMO2MCM);

			log.debug("Rest Request: " + record);

			OutputStream os = connService.getOutputStream();
			os.write(record.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<PayloadMEMO2MCM>() {
			}.getType();
			gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			gson = gb.create();

			returnPayloadMEMO2MCM = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(returnPayloadMEMO2MCM));
			os.flush();
			os.close();
			br.close();
			connService.disconnect();

			os.flush();
			os.close();

		} catch (

		Exception e) {
			e.printStackTrace();
		}
		log.debug("<- invokePostClearingProcess");
		return returnPayloadMEMO2MCM;
	}

	public List<MarketAction> invokeGetActionNotBilled(PayloadMEMO2MCM inputPayloadMEMO2MCM) {

		log.debug("-> invokeGetActionNotBilled");
		ArrayList<MarketAction> returnMarketActionNotBilledList = new ArrayList<MarketAction>();

		try {

			URL url = new URL(inputPayloadMEMO2MCM.getInformationBrokerServerUrl()
					+ "/marketplace/gam/marketsessions/completed/offers/notbilled/");
			log.debug("Start - Rest url: " + url.toString());
			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(false);
			connService.setRequestMethod("GET");
			connService.setRequestProperty("Content-Type", "application/json");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Authorization", inputPayloadMEMO2MCM.getTokenMEMO());

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<ArrayList<MarketAction>>() {
			}.getType();
			GsonBuilder gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();
			returnMarketActionNotBilledList = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(returnMarketActionNotBilledList));
			br.close();
			connService.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("<- invokeGetActionNotBilled");
		return returnMarketActionNotBilledList;
	}

	public void invokeBillingProcess(PayloadMEMO2MBM inputPayloadMEMO2MBM) {

		log.debug("-> invokeBillingProcess");

		try {

			URL url = new URL(inputPayloadMEMO2MBM.getMarketBillingManagerUrl() + "/billingProcess/");

			log.debug("Rest url: " + url.toString());

			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");
			GsonBuilder gb = new GsonBuilder();

			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();

			String record = gson.toJson(inputPayloadMEMO2MBM);

			log.debug("Rest Request: " + record);

			OutputStream os = connService.getOutputStream();
			os.write(record.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			connService.disconnect();

			os.flush();
			os.close();
			log.debug("<- invokeBillingProcess");
		} catch (

		Exception e) {
			e.printStackTrace();

		}

	}

}