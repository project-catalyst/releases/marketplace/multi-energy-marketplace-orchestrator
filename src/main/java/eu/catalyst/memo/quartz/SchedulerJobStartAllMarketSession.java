package eu.catalyst.memo.quartz;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.utilities.Utilities;

public class SchedulerJobStartAllMarketSession implements Job {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(SchedulerJobStartAllMarketSession.class);

	public void execute(JobExecutionContext context) throws JobExecutionException {

		try {

			List<MarketplaceComponent> myMarketplaceComponentList = new ArrayList<MarketplaceComponent>();
			myMarketplaceComponentList = MarketplaceComponentFacade.findEntitiesByComponent(Utilities.IB_COMPONENT_NAME);

			for (MarketplaceComponent myMarketplaceComponent : myMarketplaceComponentList) {
				String informationBrokerServerUrl = myMarketplaceComponent.getUrl();
				URL url = new URL(informationBrokerServerUrl + "/marketplace/all/update/marketsessions/active/");
				log.debug("Start - Rest url: " + url.toString());
				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(false);
				connService.setRequestMethod("PUT");
				connService.setRequestProperty("Content-Type", "application/json");
				connService.setRequestProperty("Accept", "application/json");
				String tokenMEMO = Utilities.getToken(myMarketplaceComponent.getType(),
						myMarketplaceComponent.getForm(), Utilities.MEMO_COMPONENT_NAME);
				log.debug("token MEMO: " + tokenMEMO);
				connService.setRequestProperty("Authorization", tokenMEMO);

				// -----------------------------------------------------------------------

				if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
				}
				connService.disconnect();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}