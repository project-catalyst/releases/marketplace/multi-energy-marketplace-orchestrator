package eu.catalyst.memo.quartz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.memo.internal.MultiMarketClearing;
import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.payload.MarketSession;
import eu.catalyst.memo.utilities.Utilities;

public class SchedulerJobEndAllMarketSession implements Job {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(SchedulerJobEndAllMarketSession.class);

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {

			List<MarketSession> marketSessionNotFlexibilityList = new ArrayList<MarketSession>();

			List<MarketplaceComponent> myMarketplaceComponentList = new ArrayList<MarketplaceComponent>();
			myMarketplaceComponentList = MarketplaceComponentFacade
					.findEntitiesByComponent(Utilities.IB_COMPONENT_NAME);

			List<MarketSession> myMarketSessionList = new ArrayList<MarketSession>();

			for (MarketplaceComponent myMarketplaceComponent : myMarketplaceComponentList) {
				String informationBrokerServerUrl = myMarketplaceComponent.getUrl();
				URL url = new URL(informationBrokerServerUrl + "/marketplace/all/update/marketsessions/closed/");
				log.debug("Start - Rest url: " + url.toString());
				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(false);
				connService.setRequestMethod("PUT");
				connService.setRequestProperty("Content-Type", "application/json");
				connService.setRequestProperty("Accept", "application/json");
				String tokenMEMO = Utilities.getToken(myMarketplaceComponent.getType(),
						myMarketplaceComponent.getForm(), Utilities.MEMO_COMPONENT_NAME);
				log.debug("token MEMO: " + tokenMEMO);
				connService.setRequestProperty("Authorization", tokenMEMO);
				// -------------------------
				// List<MarketSession> myMarketSessionList = new ArrayList<MarketSession>();

				// BufferedReader br = new BufferedReader(new
				// InputStreamReader((connService.getInputStream())));
				// Type listType = new TypeToken<ArrayList<MarketSession>>() {
				// }.getType();
				// GsonBuilder gb = new GsonBuilder();
				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				// Gson gson = gb.create();
				// myMarketSessionList = gson.fromJson(br, listType);
				// br.close();
				// ----------------------

				InputStreamReader isReader = new InputStreamReader(connService.getInputStream());
				// Creating a BufferedReader object
				BufferedReader reader = new BufferedReader(isReader);
				StringBuffer sb = new StringBuffer();
				String str;
				while ((str = reader.readLine()) != null) {
					sb.append(str);
				}

				log.debug("payload received: " + sb.toString());

				if (sb != null) {
					String stringInput = sb.toString();
					log.debug("stringInput: " + stringInput);
					String strFrom = "dSOid";
					String strTo = "dsoid";
					String stringOutput = stringInput.replaceAll(strFrom, strTo);
					log.debug("stringOutput: " + stringOutput);
					Type listType = new TypeToken<ArrayList<MarketSession>>() {
					}.getType();
					GsonBuilder gb = new GsonBuilder();
					gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
					Gson gson = gb.create();
					myMarketSessionList = gson.fromJson(stringInput, listType);
				}

				isReader.close();
				reader.close();
				connService.disconnect();

				// ------------------------------------------------------------------------
				// Manage the Sessions of Market not Flexibility
				// -----------------------------------------------------------------------

				// filtra le sessioni relative a Market not Flexibility

				for (Iterator<MarketSession> iterator = myMarketSessionList.iterator(); iterator.hasNext();) {
					MarketSession myMarketSession = iterator.next();
					log.debug("Session Type: " + myMarketSession.getMarketplaceid().getMarketServiceTypeid().getType());
					if (!myMarketSession.getMarketplaceid().getMarketServiceTypeid().getType().equals(Utilities.FLEX_TYPE_NAME_1)
						&& !myMarketSession.getMarketplaceid().getMarketServiceTypeid().getType().equals(Utilities.FLEX_TYPE_NAME_2)	) {
						marketSessionNotFlexibilityList.add(myMarketSession);
					}
				}

			}

			// chiama il servizio per la gestione del Clearing multi mercato

			if (!marketSessionNotFlexibilityList.isEmpty()) {
				MultiMarketClearing.executeMultiMarketClearing(marketSessionNotFlexibilityList);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
