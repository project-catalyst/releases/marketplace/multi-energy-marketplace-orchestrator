package eu.catalyst.memo.global;

import javax.ws.rs.core.Application;
import java.util.Set;

@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> resources = new java.util.HashSet<>();
		// following code can be used to customize Jersey 1.x JSON provider:
		try {
			Class jacksonProvider = Class.forName("org.codehaus.jackson.jaxrs.JacksonJsonProvider");
			resources.add(jacksonProvider);
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		addRestResourceClasses(resources);
		return resources;
	}

	/**
	 * Do not modify addRestResourceClasses() method. It is automatically populated
	 * with all resources defined in the project. If required, comment out calling
	 * this method in getClasses().
	 */
	private void addRestResourceClasses(Set<Class<?>> resources) {
		resources.add(eu.catalyst.memo.service.Marketplace.class);
		resources.add(eu.catalyst.memo.service.Login.class);
		resources.add(eu.catalyst.memo.service.ParticipantRegistration.class);
		resources.add(eu.catalyst.memo.service.CorrelatingActions.class);
		resources.add(eu.catalyst.memo.service.SystemParameterREST.class);
		resources.add(eu.catalyst.memo.service.ConstraintREST.class);
		resources.add(eu.catalyst.memo.service.ActiveMarketActions.class);
		resources.add(eu.catalyst.memo.service.ActiveSessions.class);
		resources.add(eu.catalyst.memo.service.ClearingPrices.class);
		resources.add(eu.catalyst.memo.service.CorrelatedMarkeActions.class);
		resources.add(eu.catalyst.memo.service.MarketResults.class);
		resources.add(eu.catalyst.memo.service.ReferencePrices.class);
	}

}
