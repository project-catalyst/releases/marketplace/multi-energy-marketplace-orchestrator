package eu.catalyst.memo.global;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.Date;

/**
 * *
 * * @author Engineering Ingegneria Informatica S.p.A.
 */
public class DateDeSerializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        return new DateTime(jp.getText()).toDate();
    }
}

