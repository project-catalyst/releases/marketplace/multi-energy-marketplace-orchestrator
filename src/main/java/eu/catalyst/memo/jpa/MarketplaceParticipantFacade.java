package eu.catalyst.memo.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import eu.catalyst.memo.model.MarketplaceParticipant;
import eu.catalyst.memo.payload.CorrelationActionOLD;
import eu.catalyst.memo.utilities.Utilities;

public class MarketplaceParticipantFacade {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(MarketplaceParticipantFacade.class);

	public static MarketplaceParticipant getMarketplaceParticipant(Integer id) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from "
													+ Utilities.doubleQuote("MarketplaceParticipant")
													+ " t1 where id = "
													+ String.valueOf(id), MarketplaceParticipant.class);
		log.debug("query: " + query);
		MarketplaceParticipant myMarketplaceComponent = (MarketplaceParticipant) query.getSingleResult();
		entitymanager.close();
		emfactory.close();
		return myMarketplaceComponent;
	}

	public static MarketplaceParticipant getMarketplaceParticipant(String username, Integer ibId) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from "
													+ Utilities.doubleQuote("MarketplaceParticipant")
													+ " t1 where t1.username = "
													+ Utilities.singleQuote(username)
													+ " and t1.ib_id = "
													+ String.valueOf(ibId), MarketplaceParticipant.class);
		log.debug("query: " + query);
		MarketplaceParticipant myMarketplaceComponent = (MarketplaceParticipant) query.getSingleResult();
		entitymanager.close();
		emfactory.close();
		return myMarketplaceComponent;
	}
	
	public static void setMarketplaceParticipant(MarketplaceParticipant marketplaceParticipant) {
		log.debug("-> setMarketplaceParticipant");
		log.debug("marketplaceParticipant: " + marketplaceParticipant);

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		entitymanager.persist(marketplaceParticipant);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static void setMarketplaceParticipant(String username, Integer ibId, Integer actorId) {
		log.debug("-> setMarketplaceParticipant");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		MarketplaceParticipant myMarketplaceParticipant = new MarketplaceParticipant();

		myMarketplaceParticipant.setUsername(username);
		myMarketplaceParticipant.setIbId(ibId);
		myMarketplaceParticipant.setActorId(actorId);

		entitymanager.persist(myMarketplaceParticipant);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static List<MarketplaceParticipant> findAllEntities() {
		log.debug("-> findAllEntities");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		Query query = entitymanager.createNativeQuery("Select * from "
													+ Utilities.doubleQuote("MarketplaceParticipant")
													+ " t1", MarketplaceParticipant.class);
		log.debug("query: " + query);
		List<MarketplaceParticipant> returnList = (List<MarketplaceParticipant>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static List<MarketplaceParticipant> findEntitiesByUsername(String username) {
		log.debug("-> findEntitiesByUsername");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		Query query = entitymanager.createNativeQuery("Select * from "
													+ Utilities.doubleQuote("MarketplaceParticipant")
													+ " t1 where t1.username = "
													+ Utilities.singleQuote(username), MarketplaceParticipant.class);
		log.debug("query: " + query);
		List<MarketplaceParticipant> returnList = (List<MarketplaceParticipant>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

}