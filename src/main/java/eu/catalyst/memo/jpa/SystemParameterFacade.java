package eu.catalyst.memo.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import eu.catalyst.memo.utilities.Utilities;

import eu.catalyst.memo.model.Correlation;
import eu.catalyst.memo.model.SystemParameter;

public class SystemParameterFacade {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SystemParameterFacade.class);

	public static SystemParameter getSystemParameter(Integer id) {
		log.debug("-> getSystemParameter");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from "
		                                            + Utilities.doubleQuote("SystemParameter")
				                                    + " t1 where id = "
		                                            + String.valueOf(id), SystemParameter.class);
		log.debug("query: " + query);
		SystemParameter myConstraint = (SystemParameter) query.getSingleResult();
		entitymanager.close();
		emfactory.close();
		return myConstraint;
	}

	public static void deleteSystemParameter(Integer id) {
		log.debug("-> deleteSystemParameter");
		log.debug("id: " + id);

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();
		SystemParameter mySystemParameter = entitymanager.find(SystemParameter.class, id);

		entitymanager.remove(mySystemParameter);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();
		return;
	}

	public static SystemParameter setSystemParameter(SystemParameter systemParameter) {
		log.debug("-> setSystemParameter");
		log.debug("systemParameter: " + systemParameter);

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		entitymanager.persist(systemParameter);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return systemParameter;
	}

	public static void setSystemParameter(String name, String value) {
		log.debug("-> setSystemParameter");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		SystemParameter mySystemParameter = new SystemParameter();

		mySystemParameter.setName(name);
		mySystemParameter.setValue(value);
		entitymanager.persist(mySystemParameter);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static List<SystemParameter> findAllEntities() {
		log.debug("-> findAllEntities");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		Query query = entitymanager.createNativeQuery("Select * from "
		                                            + Utilities.doubleQuote("SystemParameter")
				                                    + " t1", SystemParameter.class);
		log.debug("query: " + query);
		List<SystemParameter> returnList = (List<SystemParameter>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static SystemParameter findEntityByName(String name) {
		log.debug("-> findEntityByName");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from "
		                                            + Utilities.doubleQuote("SystemParameter")
				                                    + " t1 where t1.name = "
		                                            + Utilities.singleQuote(name), SystemParameter.class);
		log.debug("query: " + query);
		SystemParameter mySystemParameter = (SystemParameter) query.getSingleResult();

		entitymanager.close();
		emfactory.close();

		return mySystemParameter;

	}

	public static void updateSystemParameter(SystemParameter systemParameter) {
		log.debug("-> updateSystemParameter");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		SystemParameter mySystemParameter = entitymanager.find(SystemParameter.class, systemParameter.getId());
		mySystemParameter.setName(systemParameter.getName());
		mySystemParameter.setValue(systemParameter.getValue());

		entitymanager.persist(mySystemParameter);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;

	}

}