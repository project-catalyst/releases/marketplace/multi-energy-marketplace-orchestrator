package eu.catalyst.memo.jpa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import eu.catalyst.memo.utilities.Utilities;

import eu.catalyst.memo.model.Participant;
import eu.catalyst.memo.model.SystemParameter;

public class ParticipantFacade {
	
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ParticipantFacade.class);

	public static Participant getParticipant(Integer id) {
		log.debug("-> getParticipant");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from "
		                                            + Utilities.doubleQuote("Participant")
				                                    + " t1 where id = " + String.valueOf(id), Participant.class);
		log.debug("query: " + query);
		Participant myParticipant = (Participant) query.getSingleResult();
		entitymanager.close();
		emfactory.close();
		return myParticipant;
	}
	
	public static List<Participant>  findEntityByUsername(String username) {
		log.debug("-> findEntityByUsername");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from "
		                                            + Utilities.doubleQuote("Participant")
				                                    + " t1 where t1.username = "
		                                            + Utilities.singleQuote(username), Participant.class);
		log.debug("query: " + query);
		List<Participant> myParticipantList = (List<Participant>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return myParticipantList;

	}
	

	public static void setParticipant(Participant participant) {
		log.debug("-> setParticipant");
		log.debug("participant: " + participant);

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		entitymanager.persist(participant);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static void setParticipant(
			String firstName,
			String lastName,
			String username,
			String email,
			String phone,
			String vat,
			String companyName,
			Integer actorTypeId,
			String status) {
		log.debug("-> setParticipant");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		Participant myParticipant = new Participant();

		myParticipant.setFirstName(firstName);
		myParticipant.setLastName(lastName);
		myParticipant.setUsername(username);
		myParticipant.setEmail(email);
		myParticipant.setPhone(phone);
		myParticipant.setVat(vat);
		myParticipant.setCompanyName(companyName);
		myParticipant.setActorTypeId(actorTypeId);
		myParticipant.setStatus(status);

		entitymanager.persist(myParticipant);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static void modifyParticipant(
			String firstName,
			String lastName,
			String username,
			String email,
			String phone,
			String vat,
			String companyName,
			Integer actorTypeId,
			String status) {
		log.debug("-> modifyParticipant");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		// ----
		Query query = entitymanager.createNativeQuery("Select * from "
		                                            + Utilities.doubleQuote("Participant")
				                                    + " t1 where t1.username = "
		                                            + Utilities.singleQuote(username),	Participant.class);
		log.debug("query: " + query);
		Participant myParticipant = (Participant) query.getSingleResult();

		myParticipant.setFirstName(firstName);
		myParticipant.setLastName(lastName);
		myParticipant.setUsername(username);
		myParticipant.setEmail(email);
		myParticipant.setPhone(phone);
		myParticipant.setVat(vat);
		myParticipant.setCompanyName(companyName);
		myParticipant.setActorTypeId(actorTypeId);
		myParticipant.setStatus(status);

		entitymanager.persist(myParticipant);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static List<Participant> findAllEntities() {
		log.debug("-> findAllEntities");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		Query query = entitymanager.createNativeQuery("Select * from "
		                                            + Utilities.doubleQuote("Participant")
				                                    + " t1", Participant.class);
		log.debug("query: " + query);
		List<Participant> returnList = (List<Participant>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

}