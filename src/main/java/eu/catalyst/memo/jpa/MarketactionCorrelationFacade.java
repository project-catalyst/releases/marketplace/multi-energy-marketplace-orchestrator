package eu.catalyst.memo.jpa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.google.gson.Gson;

import eu.catalyst.memo.utilities.Utilities;

import eu.catalyst.memo.model.Correlation;
import eu.catalyst.memo.model.MarketactionCorrelation;

public class MarketactionCorrelationFacade {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(MarketactionCorrelationFacade.class);

	public static MarketactionCorrelation getMarketactionCorrelation(Integer id) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from "
				+ Utilities.doubleQuote("MarketactionCorrelation") + " t1 where id = " + String.valueOf(id),
				MarketactionCorrelation.class);
		log.debug("query: " + query);
		MarketactionCorrelation myMarketactionCorrelation = (MarketactionCorrelation) query.getSingleResult();
		entitymanager.close();
		emfactory.close();
		return myMarketactionCorrelation;
	}

	public static void setMarketactionCorrelation(MarketactionCorrelation marketactionCorrelation) {
		log.debug("-> setMarketactionCorrelation");
		log.debug("marketactionCorrelation: " + marketactionCorrelation);

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		entitymanager.persist(marketactionCorrelation);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static void setMarketactionCorrelation(String username, Integer informationBrokerId, Integer sessionId,
			Integer actionId, Integer correlationId) {
		log.debug("-> setMarketactionCorrelation");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		MarketactionCorrelation myMarketactionCorrelation = new MarketactionCorrelation();

		myMarketactionCorrelation.setUsername(username);
		myMarketactionCorrelation.setInformationBrokerId(informationBrokerId);
		myMarketactionCorrelation.setSessionId(sessionId);
		myMarketactionCorrelation.setActionId(actionId);
		myMarketactionCorrelation.setCorrelationId(correlationId);

		entitymanager.persist(myMarketactionCorrelation);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static List<MarketactionCorrelation> findAllEntities() {
		log.debug("-> findAllEntities");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery(
				"Select * from " + Utilities.doubleQuote("MarketactionCorrelation") + " t1",
				MarketactionCorrelation.class);
		log.debug("query: " + query);
		List<MarketactionCorrelation> returnList = (List<MarketactionCorrelation>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static List<MarketactionCorrelation> findByCorrelationId(Integer correlationId) {
		log.debug("-> findByCorrelationId");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager
				.createNativeQuery(
						"Select * from " + Utilities.doubleQuote("MarketactionCorrelation")
								+ " t1 where t1.correlation_id = " + String.valueOf(correlationId),
						MarketactionCorrelation.class);
		log.debug("query: " + query);
		List<MarketactionCorrelation> returnList = (List<MarketactionCorrelation>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;
	}

	public static MarketactionCorrelation findEntityByIbSessionAction(Integer informationBrokerId, Integer sessionId,
			Integer actionId) {
		log.debug("-> findEntityByIbSessionAction");
		log.debug("informationBrokerId: " + informationBrokerId);
		log.debug("sessionId: " + sessionId);
		log.debug("actionId: " + actionId);

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		String queryString = "Select * from " + Utilities.doubleQuote("MarketactionCorrelation")
				+ " t1 where t1.information_broker_id = " + String.valueOf(informationBrokerId)
				+ " and t1.session_id = " + String.valueOf(sessionId) + " and t1.action_id = "
				+ String.valueOf(actionId);
		log.debug("queryString: " + queryString);

		Query query = entitymanager.createNativeQuery(queryString, MarketactionCorrelation.class);

		MarketactionCorrelation myMarketactionCorrelation = null;
		try {
			myMarketactionCorrelation = (MarketactionCorrelation) query.getSingleResult();
		} catch (NoResultException nre) {
			// Ignore this because as per your logic this is ok!
		}

		entitymanager.close();
		emfactory.close();

		// if (myMarketactionCorrelation == null) return null;

		return myMarketactionCorrelation;
	}

	public static void deleteByCorrelationId(Integer correlationId) {
		log.debug("-> deleteByCorrelationId");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		int countUpdated = entitymanager
				.createNativeQuery("Delete from " + Utilities.doubleQuote("MarketactionCorrelation")
						+ " t1 where t1.correlation_id = " + String.valueOf(correlationId))
				.executeUpdate();

		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;

	}

	public static void updateActionOld(Integer id, Integer informationBrokerId, Integer sessionId, Integer actionId) {
		log.debug("-> updateActionOld");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		// int countUpdated = entitymanager.createNativeQuery(
		// "Delete from Correlation t1 where id = " + String.valueOf(id))
		// .executeUpdate();

		MarketactionCorrelation myMarketactionCorrelation = entitymanager.find(MarketactionCorrelation.class, id);
		myMarketactionCorrelation.setInformationBrokerId(informationBrokerId);
		myMarketactionCorrelation.setSessionId(sessionId);
		myMarketactionCorrelation.setActionId(actionId);

		entitymanager.persist(myMarketactionCorrelation);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;

	}

}