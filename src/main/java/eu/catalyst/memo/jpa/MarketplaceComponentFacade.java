package eu.catalyst.memo.jpa;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import eu.catalyst.memo.payload.MarketplaceComponentOLD;
import eu.catalyst.memo.payload.MarketplaceComponentOLD2;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.model.MarketplaceParticipant;
import eu.catalyst.memo.utilities.Utilities;

public class MarketplaceComponentFacade {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(MarketplaceComponentFacade.class);

	public static MarketplaceComponent getMarketplaceComponent(Integer id) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from " + Utilities.doubleQuote("MarketplaceComponent")
				+ " t1 where id = " + String.valueOf(id), MarketplaceComponent.class);
		log.debug("query: " + query);
		MarketplaceComponent myMarketplaceComponent = (MarketplaceComponent) query.getSingleResult();
		entitymanager.close();
		emfactory.close();
		return myMarketplaceComponent;
	}

	public static MarketplaceComponent getMarketplaceComponent(String type, String form, String component) {

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from " + Utilities.doubleQuote("MarketplaceComponent")
				+ " t1 where t1.type = " + Utilities.singleQuote(type) + " and t1.form = " + Utilities.singleQuote(form)
				+ " and t1.component = " + Utilities.singleQuote(component), MarketplaceComponent.class);
		log.debug("query: " + query);
		MarketplaceComponent myMarketplaceComponent = (MarketplaceComponent) query.getSingleResult();
		entitymanager.close();
		emfactory.close();
		return myMarketplaceComponent;

	}

	public static void setMarketplaceComponent(MarketplaceComponent marketplaceComponent) {
		log.debug("-> setMarketplaceComponent");
		log.debug("marketplaceComponent: " + marketplaceComponent);

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		entitymanager.persist(marketplaceComponent);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static void setMarketplaceComponent(String type, String form, String component, String url, String username,
			String password) {
		log.debug("-> setMarketplaceComponent");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		MarketplaceComponent myMarketplaceComponent = new MarketplaceComponent();

		myMarketplaceComponent.setType(type);
		myMarketplaceComponent.setForm(form);
		myMarketplaceComponent.setComponent(component);
		myMarketplaceComponent.setUrl(url);
		myMarketplaceComponent.setUsername(username);
		myMarketplaceComponent.setPassword(password);

		entitymanager.persist(myMarketplaceComponent);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static void insertUpdateMarketplaceComponent(String type, String form, String component, String url,
			String username, String password) {
		log.debug("-> insertUpdateMarketplaceComponent");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		List<MarketplaceComponent> myMarketplaceComponentList = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentList = findEntitiesByTypeFormComponent(type, form, component);
		if (!myMarketplaceComponentList.isEmpty()) {
			Integer myId = myMarketplaceComponentList.get(0).getId();
			//MarketplaceComponent myMarketplaceComponent = getMarketplaceComponent(myId);
			
			String queryString = "Select * from " + Utilities.doubleQuote("MarketplaceComponent") + " t1 where id = " + String.valueOf(myId);
			
			Query query = entitymanager.createNativeQuery(queryString, MarketplaceComponent.class);
	        log.debug("queryString: " + queryString);
	        MarketplaceComponent myMarketplaceComponent = (MarketplaceComponent) query.getSingleResult();
			
//			MarketplaceComponent myMarketplaceComponent = myMarketplaceComponentList.get(0);
			myMarketplaceComponent.setUrl(url);
			myMarketplaceComponent.setUsername(username);
			myMarketplaceComponent.setPassword(password);
			entitymanager.persist(myMarketplaceComponent);

		} else {

			MarketplaceComponent myMarketplaceComponent = new MarketplaceComponent();
			myMarketplaceComponent.setType(type);
			myMarketplaceComponent.setForm(form);
			myMarketplaceComponent.setComponent(component);
			myMarketplaceComponent.setUrl(url);
			myMarketplaceComponent.setUsername(username);
			myMarketplaceComponent.setPassword(password);
			entitymanager.persist(myMarketplaceComponent);

		}

		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static List<MarketplaceComponent> findEntitiesByComponent(String component) {
		log.debug("-> findEntitiesByComponent");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		Query query = entitymanager.createNativeQuery("Select * from " + Utilities.doubleQuote("MarketplaceComponent")
				+ " t1 where t1.component = " + Utilities.singleQuote(component), MarketplaceComponent.class);
		log.debug("query: " + query);
		List<MarketplaceComponent> returnList = (List<MarketplaceComponent>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static List<MarketplaceComponentOLD2> findEntitiesOldByComponent(String component) {
		log.debug("-> findEntitiesOldByComponent");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
//		Query query = entitymanager.createNativeQuery(
//				"Select id, type,form, component, url from " + Utilities.doubleQuote("MarketplaceComponent")
//						+ " t1 where t1.component = " + Utilities.singleQuote(component),
//				MarketplaceComponentOLD2.class);

		Query query = entitymanager.createNativeQuery("Select * from " + Utilities.doubleQuote("MarketplaceComponent")
				+ " t1 where t1.component = " + Utilities.singleQuote(component), MarketplaceComponent.class);

		log.debug("query: " + query);
		List<MarketplaceComponent> resultQueryList = (List<MarketplaceComponent>) query.getResultList();

		List<MarketplaceComponentOLD2> returnList = new ArrayList<MarketplaceComponentOLD2>();

		for (Iterator<MarketplaceComponent> iterator = resultQueryList.iterator(); iterator.hasNext();) {
			MarketplaceComponent myMarketplaceComponent = iterator.next();
			MarketplaceComponentOLD2 myMarketplaceComponentOLD2 = new MarketplaceComponentOLD2();
			myMarketplaceComponentOLD2.setId(myMarketplaceComponent.getId());
			myMarketplaceComponentOLD2.setType(myMarketplaceComponent.getType());
			myMarketplaceComponentOLD2.setForm(myMarketplaceComponent.getForm());
			myMarketplaceComponentOLD2.setComponent(myMarketplaceComponent.getComponent());
			myMarketplaceComponentOLD2.setUrl(myMarketplaceComponent.getUrl());
			returnList.add(myMarketplaceComponentOLD2);
		}

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static List<MarketplaceComponent> findEntitiesByTypeComponent(String type, String component) {
		log.debug("-> findEntitiesByComponent");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		Query query = entitymanager.createNativeQuery(
				"Select * from " + Utilities.doubleQuote("MarketplaceComponent") + " t1 where t1.type = "
						+ Utilities.singleQuote(type) + " and t1.component = " + Utilities.singleQuote(component),
				MarketplaceComponent.class);
		log.debug("query: " + query);
		List<MarketplaceComponent> returnList = (List<MarketplaceComponent>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;
	}

	public static List<MarketplaceComponent> findEntitiesByFormComponent(String form, String component) {
		log.debug("-> findEntitiesByFormComponent");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		Query query = entitymanager.createNativeQuery(
				"Select * from " + Utilities.doubleQuote("MarketplaceComponent") + " t1 where t1.form = "
						+ Utilities.singleQuote(form) + " and t1.component = " + Utilities.singleQuote(component),
				MarketplaceComponent.class);
		log.debug("query: " + query);
		List<MarketplaceComponent> returnList = (List<MarketplaceComponent>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static List<MarketplaceComponent> findEntitiesByTypeFormComponent(String type, String form,
			String component) {
		log.debug("-> findEntitiesByTypeFormComponent");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		String queryString = "Select * from " + Utilities.doubleQuote("MarketplaceComponent") + " t1 where t1.type = "
				+ Utilities.singleQuote(type) + " and t1.form = " + Utilities.singleQuote(form) + " and t1.component = "
				+ Utilities.singleQuote(component);
		log.debug("queryString: " + queryString);

		Query query = entitymanager.createNativeQuery(queryString, MarketplaceComponent.class);
		log.debug("query: " + query);
		List<MarketplaceComponent> returnList = (List<MarketplaceComponent>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static List<MarketplaceComponent> findEntitiesByComponentUsername(String component, String username) {
		log.debug("-> findEntitiesByComponentUsername");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery("Select * from " + Utilities.doubleQuote("MarketplaceComponent")
				+ " t1 where t1.component = " + Utilities.singleQuote(component) + " and t1.id in (Select ib_id from "
				+ Utilities.doubleQuote("MarketplaceParticipant") + " t2 where t2.username = "
				+ Utilities.singleQuote(username) + ")", MarketplaceComponent.class);
		log.debug("query: " + query);
		List<MarketplaceComponent> returnList = (List<MarketplaceComponent>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static List<MarketplaceComponentOLD> findEntitiesOldByComponentUsername(String component, String username) {
		log.debug("-> findEntitiesOldByComponentUsername");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		String queryString = "Select * from " + Utilities.doubleQuote("MarketplaceComponent")
				+ " t1 where t1.component = " + Utilities.singleQuote(component) + " and t1.id in (Select ib_id from "
				+ Utilities.doubleQuote("MarketplaceParticipant") + " t2 where t2.username = "
				+ Utilities.singleQuote(username) + ")";
		log.debug("queryString: " + queryString);

		Query query = entitymanager.createNativeQuery(queryString, MarketplaceComponent.class);

		List<MarketplaceComponent> resultQueryList = (List<MarketplaceComponent>) query.getResultList();
		List<MarketplaceComponentOLD> returnList = new ArrayList<MarketplaceComponentOLD>();

		for (Iterator<MarketplaceComponent> iterator = resultQueryList.iterator(); iterator.hasNext();) {
			MarketplaceComponent myMarketplaceComponent = iterator.next();
			MarketplaceComponentOLD myMarketplaceComponentOLD = new MarketplaceComponentOLD();
			myMarketplaceComponentOLD.setId(myMarketplaceComponent.getId());
			MarketplaceParticipant myMarketplaceParticipant = MarketplaceParticipantFacade
					.getMarketplaceParticipant(username, myMarketplaceComponent.getId());
			myMarketplaceComponentOLD.setActorId(myMarketplaceParticipant.getActorId());
			myMarketplaceComponentOLD.setType(myMarketplaceComponent.getType());
			myMarketplaceComponentOLD.setForm(myMarketplaceComponent.getForm());
			myMarketplaceComponentOLD.setComponent(myMarketplaceComponent.getComponent());
			myMarketplaceComponentOLD.setUrl(myMarketplaceComponent.getUrl());
			returnList.add(myMarketplaceComponentOLD);
		}

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

}