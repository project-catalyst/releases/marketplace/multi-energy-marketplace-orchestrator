package eu.catalyst.memo.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import eu.catalyst.memo.utilities.Utilities;

import eu.catalyst.memo.model.Constraint;

public class ConstraintFacade {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ConstraintFacade.class);

	public static Constraint getConstraint(Integer id) {
		log.debug("-> getConstraint");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery(
				"Select * from " + Utilities.doubleQuote("Constraint") + " t1 where id = " + String.valueOf(id),
				Constraint.class);
		log.debug("query: " + query);
		Constraint myConstraint = (Constraint) query.getSingleResult();
		entitymanager.close();
		emfactory.close();
		return myConstraint;
	}

	public static void setConstraint(Constraint constraint) {
		log.debug("-> setConstraint");
		log.debug("constraint: " + constraint);

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		entitymanager.persist(constraint);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static void setConstraint(String description) {
		log.debug("-> setConstraint");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		Constraint myConstraint = new Constraint();

		myConstraint.setDescription(description);
		entitymanager.persist(myConstraint);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static List<Constraint> findAllEntities() {
		log.debug("-> findAllEntities");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		Query query = entitymanager.createNativeQuery("Select * from " + Utilities.doubleQuote("Constraint") + " t1",
				Constraint.class);
		log.debug("query: " + query);
		List<Constraint> returnList = (List<Constraint>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static Constraint findEntityByIbSessionAction(Integer informationBrokerId, Integer sessionId,
			Integer actionId) {
		log.debug("-> findEntityByIbSessionAction");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();
		String queryString = "Select * from " + Utilities.doubleQuote("Constraint") + " t1 where t1.id = "
				+ "(SELECT t2.constraint_id FROM " + Utilities.doubleQuote("Correlation") + " t2 where t2.id = "
				+ "(SELECT t3.correlation_id FROM " + Utilities.doubleQuote("MarketactionCorrelation")
				+ " t3 where t3.ib_id = " + String.valueOf(informationBrokerId) + " and t3.session_id = "
				+ String.valueOf(sessionId) + " and t3.action_id = " + String.valueOf(actionId) + "))";
		log.debug("queryString: " + queryString);

		Query query = entitymanager.createNativeQuery(queryString, Constraint.class);
		log.debug("query: " + query);
		Constraint myConstraint = (Constraint) query.getSingleResult();

		entitymanager.close();
		emfactory.close();

		return myConstraint;

	}

	public static Constraint findEntityByCorrelationId(Integer correlationId) {
		log.debug("-> findEntityByIbSessionAction");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		String queryString = "Select * from " + Utilities.doubleQuote("Constraint") + " t1 where t1.id = "
				+ "(SELECT t2.constraint_id FROM " + Utilities.doubleQuote("Correlation") + " t2 where t2.id = "
				+ String.valueOf(correlationId) + ")";
		log.debug("queryString: " + queryString);
		Query query = entitymanager.createNativeQuery(queryString, Constraint.class);

		Constraint myConstraint = (Constraint) query.getSingleResult();

		entitymanager.close();
		emfactory.close();

		return myConstraint;

	}

}