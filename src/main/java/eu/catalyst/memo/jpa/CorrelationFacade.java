package eu.catalyst.memo.jpa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import eu.catalyst.memo.utilities.Utilities;

import eu.catalyst.memo.model.Correlation;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.model.Participant;

public class CorrelationFacade {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CorrelationFacade.class);

	public static Correlation getCorrelation(Integer id) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		Query query = entitymanager.createNativeQuery(
				"Select * from " + Utilities.doubleQuote("Correlation") + " t1 where id = " + String.valueOf(id),
				Correlation.class);
		log.debug("query: " + query);
		Correlation myCorrelation = (Correlation) query.getSingleResult();
		entitymanager.close();
		emfactory.close();
		return myCorrelation;
	}

	public static void setCorrelation(Correlation correlation) {
		log.debug("-> setCorrelation");
		log.debug("correlation: " + correlation);

		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		entitymanager.persist(correlation);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;
	}

	public static Correlation setCorrelation(Integer constraintId, String timeframe, Long timestamp) {
		log.debug("-> setCorrelation");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		Correlation myCorrelation = new Correlation();

		myCorrelation.setConstraintId(constraintId);
		myCorrelation.setTimeframe(timeframe);
		myCorrelation.setTimestamp(timestamp);
		entitymanager.persist(myCorrelation);
		// Integer myCorrelationId = myCorrelation.getId();
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return myCorrelation;
	}

	public static List<Correlation> findAllEntities() {
		log.debug("-> findAllEntities");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
		Query query = entitymanager.createNativeQuery("Select * from " + Utilities.doubleQuote("Correlation") + " t1",
				Correlation.class);
		log.debug("query: " + query);
		List<Correlation> returnList = (List<Correlation>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static List<Correlation> findEntitiesByTimeframeUsername(String timeframe, String username) {
		log.debug("-> findEntitiesByTimeframe");
		log.debug("timeframe: " + timeframe);
		log.debug("username: " + username);
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
//		Query query = entitymanager.createNativeQuery("Select * from " 
//													 + Utilities.doubleQuote("Correlation")
//													 + " t1 where t1.timeframe = "
//													 + Utilities.singleQuote(timeframe), Correlation.class);
//		
		String queryString = "";

		// Cerco il Participant
		// Se non lo trovo allora presumo sia admin
		List<Participant> myParticipantList = ParticipantFacade.findEntityByUsername(username);
		if (myParticipantList.isEmpty()) {
//		if (username.equals(Utilities.USER_ADMIN)) {
			queryString = "Select * from " + Utilities.doubleQuote("Correlation") + " t1 where t1.timeframe = "
					+ Utilities.singleQuote(timeframe);
		} else {
			queryString = "Select * from " + Utilities.doubleQuote("Correlation") + " t1 where t1.timeframe = "
					+ Utilities.singleQuote(timeframe) + " and exists (select * from "
					+ Utilities.doubleQuote("MarketactionCorrelation")
					+ " t2 where t2.correlation_id = t1.id and t2.username = " + Utilities.singleQuote(username) + ")";
		}

		log.debug("queryString: " + queryString);
		Query query = entitymanager.createNativeQuery(queryString, Correlation.class);

		List<Correlation> returnList = (List<Correlation>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static List<Correlation> findEntitiesByTimeframeStartEndUsername(String timeframe, Long start, Long end,
			String username) {
		log.debug("-> findEntitiesByTimeframeStartEnd");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");

		EntityManager entitymanager = emfactory.createEntityManager();

		// Select DSO profile for the selected id_dso_request_profile
//		Query query = entitymanager.createNativeQuery("Select * from "
//													+ Utilities.doubleQuote("Correlation")
//													+ " t1 where t1.timeframe = "
//													+ Utilities.singleQuote(timeframe)
//													+ " and t1.timestamp between "
//													+ String.valueOf(start)
//													+ " and "
//													+ String.valueOf(end), Correlation.class);

		// Cerco il Participant
		// Se non lo trovo allora presumo sia admin

		String queryString = "";

		List<Participant> myParticipantList = ParticipantFacade.findEntityByUsername(username);
		if (myParticipantList.isEmpty()) {
			queryString = "Select * from " + Utilities.doubleQuote("Correlation") + " t1 where t1.timeframe = "
					+ Utilities.singleQuote(timeframe) + " and t1.timestamp between " + String.valueOf(start) + " and "
					+ String.valueOf(end);
		} else {
			queryString = "Select * from " + Utilities.doubleQuote("Correlation") + " t1 where t1.timeframe = "
					+ Utilities.singleQuote(timeframe) + " and t1.timestamp between " + String.valueOf(start) + " and "
					+ String.valueOf(end) + " and exists (select * from "
					+ Utilities.doubleQuote("MarketactionCorrelation")
					+ " t2 where t2.correlation_id = t1.id and t2.username = " + Utilities.singleQuote(username) + ")";
		}

		log.debug("queryString: " + queryString);
		Query query = entitymanager.createNativeQuery(queryString, Correlation.class);

		List<Correlation> returnList = (List<Correlation>) query.getResultList();

		entitymanager.close();
		emfactory.close();

		return returnList;

	}

	public static void deleteById(Integer id) {
		log.debug("-> deleteById");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		// int countUpdated = entitymanager.createNativeQuery(
		// "Delete from Correlation t1 where id = " + String.valueOf(id))
		// .executeUpdate();

		Correlation myCorrelation = entitymanager.find(Correlation.class, id);

		entitymanager.remove(myCorrelation);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;

	}

	public static void updateConstraintId(Integer id, Integer constraintId, String timeframe, Long timestamp) {
		log.debug("-> updateConstraintId");
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("catalystJPA");
		EntityManager entitymanager = emfactory.createEntityManager();
		entitymanager.getTransaction().begin();

		// int countUpdated = entitymanager.createNativeQuery(
		// "Delete from Correlation t1 where id = " + String.valueOf(id))
		// .executeUpdate();

		Correlation myCorrelation = entitymanager.find(Correlation.class, id);
		myCorrelation.setConstraintId(constraintId);
		myCorrelation.setTimeframe(timeframe);
		myCorrelation.setTimestamp(timestamp);
		entitymanager.persist(myCorrelation);
		entitymanager.getTransaction().commit();
		entitymanager.close();
		emfactory.close();

		return;

	}

}