package eu.catalyst.memo.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.memo.payload.Form;
import eu.catalyst.memo.payload.MarketAction;
import eu.catalyst.memo.payload.MarketActionCounterOffer;
import eu.catalyst.memo.payload.MarketSession;
import eu.catalyst.memo.payload.Marketplace;
import eu.catalyst.memo.payload.ReferencePricePreviousDaysItem;
import eu.catalyst.memo.payload.ClearingPricePreviousDaysItem;
import eu.catalyst.memo.jpa.MarketplaceComponentFacade;
import eu.catalyst.memo.model.MarketplaceComponent;
import eu.catalyst.memo.payload.ActionStatus;
import eu.catalyst.memo.payload.ActionType;

public class Utilities {
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Utilities.class);

	public static Map<String, String> TOKEN_AUTHORIZATION = new HashMap<String, String>();

	public static final long SLEEP_TIME = 5000;
	public static final String KEY_SEPARATOR = ",";
	public static final String VALUE_PREFIX = "token ";
	public static final String PATH_POST_TOKENS = "/tokens/";
	public static final String PARAM_FLEX_SESSION_COMPLETE_TIME = "FlexSessionsCompleteTime";
	public static final String PARAM_FLEX_SESSION_BILLING_TIME = "FlexSessionsBillingTime";
	public static final String PARAM_FIXED_FEE = "fixedFee";
	public static final String PARAM_PENALTY = "penalty";

	public static final String IB_COMPONENT_NAME = "IB";
	public static final String MCM_COMPONENT_NAME = "MCM";
	public static final String MBM_COMPONENT_NAME = "MBM";
	public static final String MEMO_COMPONENT_NAME = "MEMO";
	public static final String MPCM_COMPONENT_NAME = "MPCM";

	public static final String FLEX_TYPE_NAME_1 = "ancillary_services";
	public static final String FLEX_FORM_NAME_1 = "ancillary_services";
	public static final String FLEX_TYPE_NAME_2 = "ancillary-services";
	public static final String FLEX_FORM_NAME_2 = "ancillary-services";
	
	

	public static final String TIMEFRAME_TYPE_INTRA_DAY = "intra_day";
	public static final String TIMEFRAME_TYPE_DAY_AHEAD = "day_ahead";

	public static final String CONSTRAINT_AND = "ALL OR NOTHING";
	public static final String CONSTRAINT_OR = "AT LEAST ONE";

	public static final String USER_ADMIN = "admin";

	public static final String DOP = "DoP";
	public static final String UNCHECKED = "unchecked";
	public static final String COMPLETED = "completed";

	public static final int SCALE_BIG_DECIMAL = 3;

	private static final String SINGLE_QUOTE_ESCAPE = "\'";
	private static final String DOUBLE_QUOTE_ESCAPE = "\"";

	static HashSet<String> setOfFormsFlexibility = new HashSet<>();
	static {
		setOfFormsFlexibility.add("congestion_management");
		setOfFormsFlexibility.add("reactive_power_compensation");
		setOfFormsFlexibility.add("spinning_reserve");
		setOfFormsFlexibility.add("scheduling");
	}

	public static String doubleQuote(String input) {
		return DOUBLE_QUOTE_ESCAPE + input + DOUBLE_QUOTE_ESCAPE;
	}

	public static String singleQuote(String input) {
		return SINGLE_QUOTE_ESCAPE + input + SINGLE_QUOTE_ESCAPE;
	}

	public static String makeKey(String type, String form, String component) {
		if (type.equals(FLEX_TYPE_NAME_2)) {
			type = FLEX_TYPE_NAME_1;
		}
		if (form.equals(FLEX_FORM_NAME_2)) {
			form = FLEX_FORM_NAME_1;
		}
		
		return type + KEY_SEPARATOR + form + KEY_SEPARATOR + component;
	}

	public static String normalizeForm(String form) {
		String normalizedForm = "";
		log.debug("form: " + form);
		if (setOfFormsFlexibility.contains(form)) {
			normalizedForm = FLEX_FORM_NAME_1;
		} else {
			normalizedForm = form;
		}

		log.debug("normalizedForm: " + normalizedForm);
		return normalizedForm;
	}

	public static String getToken(String type, String form, String component) {
		String key = makeKey(type, form, component);
		log.debug("key: " + key);
		String token = TOKEN_AUTHORIZATION.get(key);
		log.debug("token: " + token);
		return token;
	}

	public static void putToken(String type, String form, String component, String token) {
		String key = makeKey(type, form, component);
		String value = VALUE_PREFIX + token;
		log.debug("key: " + key);
		log.debug("token: " + token);
		TOKEN_AUTHORIZATION.put(key, value);
		return;
	}

	public Marketplace invokeGetMarketplace(String ibURL, String ibToken, int marketplaceid) throws IOException {

		Marketplace result = null;
		URL url = new URL(ibURL + "/marketplace/" + marketplaceid + "/");

		log.debug("ibURL - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<Marketplace>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(result));
		br.close();
		connService.disconnect();
		return result;
	}

	public static MarketSession invokeGetMarketSession(String ibURL, String ibToken, int marketSessionId)
			throws IOException {

		MarketSession result = null;
		URL url = new URL(ibURL + "/marketsessions/" + marketSessionId + "/");

		log.debug("invokeGetMarketSession - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<MarketSession>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(result));
		br.close();
		connService.disconnect();
		return result;
	}

	public static Form invokeGetForm(String ibURL, String ibToken, int formId) throws IOException {

		Form result = null;

		URL url = new URL(ibURL + "/form/" + formId + "/");

		log.debug("invokeGetForm - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<Form>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(result));
		br.close();
		connService.disconnect();
		return result;
	}

	public static List<ReferencePricePreviousDaysItem> invokeGetReferencePricesPreviousDays(String ibURL,
			String ibToken, int marketplaceId, String form, long milliseconds) throws IOException {

		List<ReferencePricePreviousDaysItem> result = null;

		URL url = new URL(ibURL + "/marketplace/" + marketplaceId + "/form/" + form
				+ "/reference-prices-previous-days/date/" + milliseconds + "/");

		log.debug("invokeGetReferencePricesPreviousDays - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<ReferencePricePreviousDaysItem>>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(result));
		br.close();
		connService.disconnect();

		return result;
	}

	public static List<ClearingPricePreviousDaysItem> invokeGetClearingPricesPreviousDays(String ibURL, String ibToken,
			int marketplaceId, String form, long milliseconds) throws IOException {

		List<ClearingPricePreviousDaysItem> result = null;

		URL url = new URL(ibURL + "/marketplace/" + marketplaceId + "/form/" + form
				+ "/clearing-prices-previous-days/date/" + milliseconds + "/");

		log.debug("invokeGetReferencePricesPreviousDays - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<ClearingPricePreviousDaysItem>>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(result));
		br.close();
		connService.disconnect();

		return result;
	}

	public static int invokeGetMarketplaceByFormTimeframe(String ibURL, String ibToken, String form, String timeframe)
			throws IOException {

		List<Integer> resultList = null;
		int result = 0;

		URL url = new URL(ibURL + "/select/marketplace/" + form + "/" + timeframe + "/");

		log.debug("invokeGetMarketplaceByFormTimeframe - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<Integer>>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		resultList = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(resultList));
		br.close();
		connService.disconnect();
		if (resultList != null) {
			result = resultList.get(0);
		}
		return result;
	}

	public static List<MarketSession> invokeGetActiveSessions(String ibURL, String ibToken, int marketplaceId)
			throws IOException {

		List<MarketSession> resultList = null;

		URL url = new URL(ibURL + "/marketplace/" + marketplaceId + "/marketsessions/active/");

		log.debug("invokeGetActiveSessions - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<MarketSession>>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		resultList = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(resultList));
		br.close();
		connService.disconnect();

		return resultList;
	}

	public static List<MarketSession> invokeGetAllSessions(String ibURL, String ibToken, int marketplaceId)
			throws IOException {

		List<MarketSession> resultList = null;

		URL url = new URL(ibURL + "/marketplace/" + marketplaceId + "/marketsessions/");

		log.debug("invokeGetActiveSessions - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<MarketSession>>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		resultList = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(resultList));
		br.close();
		connService.disconnect();

		return resultList;
	}

	public static MarketSession invokeGetSession(String ibURL, String ibToken, int marketsessionId) throws IOException {

		MarketSession result = null;

		URL url = new URL(ibURL + "/marketsessions/" + marketsessionId + "/");

		log.debug("invokeGetSession - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<MarketSession>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(result));
		br.close();
		connService.disconnect();

		return result;
	}

	public static List<MarketAction> invokeGetMarketActionsByMarketPlaceSessionId(String ibURL, String ibToken,
			int marketplaceId, int marketSessionId) throws IOException {

		List<MarketAction> resultList = null;

		// URL url = new URL(ibURL + "/marketplace/" + marketplaceId +
		// "/marketsessions/" + marketSessionId + "/actions/");
		URL url = new URL(ibURL + "/marketplace/" + marketplaceId + "/marketsessions/" + marketSessionId
				+ "/actions-simple-format/");

		log.debug("invokeGetMarketActionByMarketPlaceSessionId - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<MarketAction>>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		resultList = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(resultList));
		br.close();
		connService.disconnect();

		return resultList;
	}

	public static List<MarketActionCounterOffer> invokeGetMarketActionCounterOfferByMarketPlaceSessionId(String ibURL,
			String ibToken, int marketplaceId, int marketSessionId) throws IOException {

		List<MarketActionCounterOffer> resultList = null;

		URL url = new URL(
				ibURL + "/marketplace/" + marketplaceId + "/marketsessions/" + marketSessionId + "/counteroffers/");

		log.debug("invokeGetMarketActionCounterOfferByMarketPlaceSessionId - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<MarketActionCounterOffer>>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		resultList = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(resultList));
		br.close();
		connService.disconnect();

		return resultList;
	}

	public static List<MarketAction> invokePostMarketActionsByMarketPlaceSessionId(String ibURL, String ibToken,
			int marketplaceId, int marketSessionId, List<MarketAction> myMarketActionList) throws IOException {

		List<MarketAction> resultList = null;

		URL url = new URL(ibURL + "/marketplace/" + marketplaceId + "/marketsessions/" + marketSessionId + "/actions/");

		log.debug("invokeGetMarketActionByMarketPlaceSessionId - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("POST");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		GsonBuilder gb = new GsonBuilder();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Gson gson = gb.create();
		String record = gson.toJson(myMarketActionList);
		log.debug("Rest Request: " + record);

		OutputStream os = connService.getOutputStream();
		os.write(record.getBytes());

		int myResponseCode = connService.getResponseCode();
		log.debug("HTTP error code :" + myResponseCode);

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<MarketAction>>() {
		}.getType();
		gb = new GsonBuilder();
		gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		resultList = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(resultList));
		br.close();
		connService.disconnect();

		return resultList;
	}

	public static ActionStatus invokeGetActionStatus(String ibURL, String ibToken, int statusId) throws IOException {

		ActionStatus result = null;

		URL url = new URL(ibURL + "/actionstatus/" + statusId + "/");

		log.debug("invokeGetActionStatus - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<ActionStatus>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		result = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(result));
		br.close();
		connService.disconnect();
		return result;
	}

	public static List<ActionStatus> invokeGetActionStatusList(String ibURL, String ibToken) throws IOException {

		List<ActionStatus> resultList = null;

		URL url = new URL(ibURL + "/actionstatus/");

		log.debug("invokeGetActionTypeList - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<ActionStatus>>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		resultList = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(resultList));
		br.close();
		connService.disconnect();
		return resultList;
	}

	public static List<ActionType> invokeGetActionTypeList(String ibURL, String ibToken) throws IOException {

		List<ActionType> resultList = null;

		URL url = new URL(ibURL + "/actiontypes/");

		log.debug("invokeGetActionTypeList - Rest url: " + url.toString());
		HttpURLConnection connService = (HttpURLConnection) url.openConnection();
		connService.setDoOutput(true);
		connService.setRequestMethod("GET");
		connService.setRequestProperty("Accept", "application/json");
		connService.setRequestProperty("Content-Type", "application/json");
		connService.setRequestProperty("Authorization", ibToken);

		if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
		Type listType = new TypeToken<List<ActionType>>() {
		}.getType();
		GsonBuilder gb = new GsonBuilder();
		Gson gson = gb.create();
		gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		resultList = gson.fromJson(br, listType);
		log.debug("payload received: " + gson.toJson(resultList));
		br.close();
		connService.disconnect();
		return resultList;
	}

	public static void setTokenMEMOComponents() {
		log.debug("-> setTokenMEMOComponents");

		List<MarketplaceComponent> myMarketplaceComponentList = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentList = MarketplaceComponentFacade.findEntitiesByComponent(Utilities.IB_COMPONENT_NAME);

		for (MarketplaceComponent myMarketplaceComponent : myMarketplaceComponentList) {

			// retrieve USER and PASSWORD for request payload
			String credentialUser = myMarketplaceComponent.getUsername();
			String credentialPassword = myMarketplaceComponent.getPassword();

			// create request payload
			JsonObject jsonRequestPayload = new JsonObject();
			jsonRequestPayload.addProperty("username", credentialUser);
			jsonRequestPayload.addProperty("password", credentialPassword);

			// execute Restful Call POST "/tokens/ to informationbroker
			String responsePayload = "";

			// Repeats until the service responds correctly

			boolean repeat_call_service = true;

			do {
				try {

					String informationBrokerServerUrl = myMarketplaceComponent.getUrl();
					URL url = new URL(informationBrokerServerUrl + Utilities.PATH_POST_TOKENS);
					log.debug("Rest url: " + url.toString());

					HttpURLConnection connService = (HttpURLConnection) url.openConnection();
					connService.setDoOutput(true);
					connService.setRequestMethod("POST");
					connService.setRequestProperty("Content-Type", "application/json");
					connService.setRequestProperty("Accept", "application/json");

					String requestPayload = jsonRequestPayload.toString();
					log.debug("requestPayload: " + requestPayload);

					OutputStream os = connService.getOutputStream();
					os.write(requestPayload.getBytes());
					os.flush();

					if (connService.getResponseCode() == HttpURLConnection.HTTP_OK) {
						repeat_call_service = false;
					}

					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));

					responsePayload = br.readLine();

					log.debug("responsePayload: " + responsePayload);

					br.close();
					connService.disconnect();
				} catch (ConnectException ex) {
					log.error("ConnectException : " + ex);
					repeat_call_service = true;
					try {
						Thread.sleep(SLEEP_TIME);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// ex.printStackTrace();
				} catch (Exception ex) {
					log.error("Exception : " + ex);
					ex.printStackTrace();
				}

			} while (repeat_call_service);

			// parsing JSON Response payload to extract "token" property
			JsonObject responseJson = (JsonObject) new JsonParser().parse(responsePayload);
			String responseToken = responseJson.get("token").toString().replace("\"", "");
			log.debug("responseToken: " + responseToken);

			Utilities.putToken(myMarketplaceComponent.getType(), myMarketplaceComponent.getForm(),
					Utilities.MEMO_COMPONENT_NAME, responseToken);
		}

		log.debug("<- setTokenMEMOComponents");
	}

	public static void setTokenMCMMBMComponents(String component) {
		log.debug("-> setTokenMCMMBMComponents");
		List<MarketplaceComponent> myMarketplaceComponentList = new ArrayList<MarketplaceComponent>();
		myMarketplaceComponentList = MarketplaceComponentFacade.findEntitiesByComponent(component);

		for (MarketplaceComponent myMarketplaceComponent : myMarketplaceComponentList) {

			// retrieve USER and PASSWORD for request payload
			String credentialUser = myMarketplaceComponent.getUsername();
			String credentialPassword = myMarketplaceComponent.getPassword();

			// create request payload
			JsonObject jsonRequestPayload = new JsonObject();
			jsonRequestPayload.addProperty("username", credentialUser);
			jsonRequestPayload.addProperty("password", credentialPassword);

			// execute Restful Call POST "/tokens/ to informationbroker
			String responsePayload = "";

			// Repeats until the service responds correctly

			boolean repeat_call_service = true;

			do {
				try {

					MarketplaceComponent myMarketplaceComponentIB = MarketplaceComponentFacade.getMarketplaceComponent(
							myMarketplaceComponent.getType(), myMarketplaceComponent.getForm(),
							Utilities.IB_COMPONENT_NAME);

					String informationBrokerServerUrl = myMarketplaceComponentIB.getUrl();
					URL url = new URL(informationBrokerServerUrl + Utilities.PATH_POST_TOKENS);
					log.debug("Rest url: " + url.toString());

					HttpURLConnection connService = (HttpURLConnection) url.openConnection();
					connService.setDoOutput(true);
					connService.setRequestMethod("POST");
					connService.setRequestProperty("Content-Type", "application/json");
					connService.setRequestProperty("Accept", "application/json");

					String requestPayload = jsonRequestPayload.toString();
					log.debug("requestPayload: " + requestPayload);

					OutputStream os = connService.getOutputStream();
					os.write(requestPayload.getBytes());
					os.flush();

					if (connService.getResponseCode() == HttpURLConnection.HTTP_OK) {
						repeat_call_service = false;
					}

					BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));

					responsePayload = br.readLine();

					log.debug("responsePayload: " + responsePayload);

					br.close();
					connService.disconnect();
				} catch (ConnectException ex) {
					log.error("ConnectException : " + ex);
					repeat_call_service = true;
					try {
						Thread.sleep(SLEEP_TIME);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// ex.printStackTrace();
				} catch (Exception ex) {
					log.error("Exception : " + ex);
					ex.printStackTrace();
				}

			} while (repeat_call_service);

			// parsing JSON Response payload to extract "token" property
			JsonObject responseJson = (JsonObject) new JsonParser().parse(responsePayload);
			String responseToken = responseJson.get("token").toString().replace("\"", "");
			log.debug("responseToken: " + responseToken);

			Utilities.putToken(myMarketplaceComponent.getType(), myMarketplaceComponent.getForm(), component,
					responseToken);
		}

		log.debug("<- setTokenMCMMBMComponents");

	}

	public static void setTokenMEMOSingleComponent(String type, String form, String component, String urlIB,
			String username, String password) {
		log.debug("-> setTokenMEMOSingleComponent");
		log.debug("type: " + type);
		log.debug("form: " + form);
		log.debug("component: " + component);
		log.debug("urlIB: " + urlIB);
		log.debug("username: " + username);
		log.debug("password: " + password);

		// create request payload
		JsonObject jsonRequestPayload = new JsonObject();
		jsonRequestPayload.addProperty("username", username);
		jsonRequestPayload.addProperty("password", password);

		// execute Restful Call POST "/tokens/ to informationbroker
		String responsePayload = "";

		// Repeats until the service responds correctly

		boolean repeat_call_service = true;

		do {
			try {

				String informationBrokerServerUrl = urlIB;
				URL url = new URL(informationBrokerServerUrl + Utilities.PATH_POST_TOKENS);
				log.debug("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("Content-Type", "application/json");
				connService.setRequestProperty("Accept", "application/json");

				String requestPayload = jsonRequestPayload.toString();
				log.debug("requestPayload: " + requestPayload);

				OutputStream os = connService.getOutputStream();
				os.write(requestPayload.getBytes());
				os.flush();

				if (connService.getResponseCode() == HttpURLConnection.HTTP_OK) {
					repeat_call_service = false;
				}

				BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));

				responsePayload = br.readLine();

				log.debug("responsePayload: " + responsePayload);

				br.close();
				connService.disconnect();
			} catch (ConnectException ex) {
				log.error("ConnectException : " + ex);
				repeat_call_service = true;
				try {
					Thread.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// ex.printStackTrace();
			} catch (Exception ex) {
				log.error("Exception : " + ex);
				ex.printStackTrace();
			}

		} while (repeat_call_service);

		// parsing JSON Response payload to extract "token" property
		JsonObject responseJson = (JsonObject) new JsonParser().parse(responsePayload);
		String responseToken = responseJson.get("token").toString().replace("\"", "");
		log.debug("responseToken: " + responseToken);

		Utilities.putToken(type, form, Utilities.MEMO_COMPONENT_NAME, responseToken);

		log.debug("<- setTokenMEMOSingleComponent");
	}

	public static void setTokenMCMMBMSingleComponent(String type, String form, String component, String urlIB,
			String username, String password) {
		log.debug("-> setTokenMCMMBMSingleComponent");
		log.debug("type: " + type);
		log.debug("form: " + form);
		log.debug("component: " + component);
		log.debug("urlIB: " + urlIB);
		log.debug("username: " + username);
		log.debug("password: " + password);

		// create request payload
		JsonObject jsonRequestPayload = new JsonObject();
		jsonRequestPayload.addProperty("username", username);
		jsonRequestPayload.addProperty("password", password);

		// execute Restful Call POST "/tokens/ to informationbroker
		String responsePayload = "";

		// Repeats until the service responds correctly

		boolean repeat_call_service = true;

		do {
			try {

				String informationBrokerServerUrl = urlIB;
				URL url = new URL(informationBrokerServerUrl + Utilities.PATH_POST_TOKENS);
				log.debug("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("Content-Type", "application/json");
				connService.setRequestProperty("Accept", "application/json");

				String requestPayload = jsonRequestPayload.toString();
				log.debug("requestPayload: " + requestPayload);

				OutputStream os = connService.getOutputStream();
				os.write(requestPayload.getBytes());
				os.flush();

				if (connService.getResponseCode() == HttpURLConnection.HTTP_OK) {
					repeat_call_service = false;
				}

				BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));

				responsePayload = br.readLine();

				log.debug("responsePayload: " + responsePayload);

				br.close();
				connService.disconnect();
			} catch (ConnectException ex) {
				log.error("ConnectException : " + ex);
				repeat_call_service = true;
				try {
					Thread.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// ex.printStackTrace();
			} catch (Exception ex) {
				log.error("Exception : " + ex);
				ex.printStackTrace();
			}

		} while (repeat_call_service);

		// parsing JSON Response payload to extract "token" property
		JsonObject responseJson = (JsonObject) new JsonParser().parse(responsePayload);
		String responseToken = responseJson.get("token").toString().replace("\"", "");
		log.debug("responseToken: " + responseToken);

		Utilities.putToken(type, form, component, responseToken);

		log.debug("<- setTokenMCMMBMSingleComponent");

	}
}
