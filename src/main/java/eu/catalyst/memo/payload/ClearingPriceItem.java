package eu.catalyst.memo.payload;

import eu.catalyst.memo.global.DateDeSerializer;
import eu.catalyst.memo.global.DateSerializer;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.math.BigDecimal;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

public class ClearingPriceItem {

	@JsonDeserialize(using = DateDeSerializer.class)
	@JsonSerialize(using = DateSerializer.class)
	private Date validityStartTime;
	@JsonDeserialize(using = DateDeSerializer.class)
	@JsonSerialize(using = DateSerializer.class)
	private Date validityEndTime;

	private BigDecimal clearingPrice;

	public ClearingPriceItem() {
	}
	
	public void initializeSlot(int slot) {
	    GregorianCalendar gc = new GregorianCalendar();
	    //gc.setTime( dayDate );
	    gc.add(Calendar.DAY_OF_YEAR, -1);
	    gc.set( Calendar.HOUR_OF_DAY, slot );
	    gc.set( Calendar.MINUTE, 0 );
	    gc.set( Calendar.SECOND, 0 );
	    gc.set( Calendar.MILLISECOND, 0 );
	    validityStartTime = gc.getTime();
	    gc.set( Calendar.HOUR_OF_DAY, slot );
	    gc.set( Calendar.MINUTE, 59 );
	    gc.set( Calendar.SECOND, 59);
	    gc.set( Calendar.MILLISECOND, 999 );
	    validityEndTime = gc.getTime();
	    clearingPrice = BigDecimal.ZERO; 
	}

	public Date getValidityStartTime() {
		return validityStartTime;
	}

	public void setValidityStartTime(Date validityStartTime) {
		this.validityStartTime = validityStartTime;
	}

	public Date getValidityEndTime() {
		return validityEndTime;
	}

	public void setValidityEndTime(Date validityEndTime) {
		this.validityEndTime = validityEndTime;
	}

	public BigDecimal getClearingPrice() {
		return clearingPrice;
	}

	public void setClearingPrice(BigDecimal clearingPrice) {
		this.clearingPrice = clearingPrice;
	}

}
