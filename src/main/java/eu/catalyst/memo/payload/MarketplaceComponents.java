package eu.catalyst.memo.payload;
import java.io.Serializable;
public class MarketplaceComponents implements Serializable {
    private static final long serialVersionUID = 1L;
	private MarketplaceComponentItem ib;

	private MarketplaceComponentItem mbm;

	private MarketplaceComponentItem mcm;

	public MarketplaceComponents() {
	}

	public MarketplaceComponentItem getIb() {
		return ib;
	}

	public void setIb(MarketplaceComponentItem ib) {
		this.ib = ib;
	}

	public MarketplaceComponentItem getMbm() {
		return mbm;
	}

	public void setMbm(MarketplaceComponentItem mbm) {
		this.mbm = mbm;
	}

	public MarketplaceComponentItem getMcm() {
		return mcm;
	}

	public void setMcm(MarketplaceComponentItem mcm) {
		this.mcm = mcm;
	}

}
