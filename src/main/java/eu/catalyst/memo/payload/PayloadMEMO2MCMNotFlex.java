package eu.catalyst.memo.payload;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class PayloadMEMO2MCMNotFlex implements Serializable {
    private static final long serialVersionUID = 1L;

	public PayloadMEMO2MCMNotFlex() {
	}

	private BigDecimal clearingPrice;
	private MarketSession marketSession;
	private List<PrioritisedAction> prioritisedActionsList;
	private List<MarketActionCounterOffer> marketActionCounterOfferList;

	private Integer informationBrokerId;
	private String informationBrokerServerUrl;
	private String tokenMEMO;
	private String marketClearingManagerUrl;
	private String tokenMCM;

	public MarketSession getMarketSession() {
		return marketSession;
	}

	public void setMarketSession(MarketSession marketSession) {
		this.marketSession = marketSession;
	}

	public List<PrioritisedAction> getPrioritisedActionsList() {
		return prioritisedActionsList;
	}

	public void setPrioritisedActionsList(List<PrioritisedAction> prioritisedActionsList) {
		this.prioritisedActionsList = prioritisedActionsList;
	}

	public List<MarketActionCounterOffer> getMarketActionCounterOfferList() {
		return marketActionCounterOfferList;
	}

	public void setMarketActionCounterOfferList(List<MarketActionCounterOffer> marketActionCounterOfferList) {
		this.marketActionCounterOfferList = marketActionCounterOfferList;
	}

	public String getInformationBrokerServerUrl() {
		return informationBrokerServerUrl;
	}

	public void setInformationBrokerServerUrl(String informationBrokerServerUrl) {
		this.informationBrokerServerUrl = informationBrokerServerUrl;
	}

	public String getTokenMEMO() {
		return tokenMEMO;
	}

	public void setTokenMEMO(String tokenMEMO) {
		this.tokenMEMO = tokenMEMO;
	}

	public String getTokenMCM() {
		return tokenMCM;
	}

	public void setTokenMCM(String tokenMCM) {
		this.tokenMCM = tokenMCM;
	}

	public String getMarketClearingManagerUrl() {
		return marketClearingManagerUrl;
	}

	public void setMarketClearingManagerUrl(String marketClearingManagerUrl) {
		this.marketClearingManagerUrl = marketClearingManagerUrl;
	}

	public BigDecimal getClearingPrice() {
		return clearingPrice;
	}

	public void setClearingPrice(BigDecimal clearingPrice) {
		this.clearingPrice = clearingPrice;
	}

	public Integer getInformationBrokerId() {
		return informationBrokerId;
	}

	public void setInformationBrokerId(Integer informationBrokerId) {
		this.informationBrokerId = informationBrokerId;
	}

}