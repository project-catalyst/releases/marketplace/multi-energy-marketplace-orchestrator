package eu.catalyst.memo.payload;

import eu.catalyst.memo.global.DateDeSerializer;
import eu.catalyst.memo.global.DateSerializer;

import java.math.BigDecimal;
import java.util.Date;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

public class ReferencePricePreviousDaysItem {

	private Integer id;

	private BigDecimal price;
	@JsonDeserialize(using = DateDeSerializer.class)
	@JsonSerialize(using = DateSerializer.class)
	private Date validityStartTime;
	@JsonDeserialize(using = DateDeSerializer.class)
	@JsonSerialize(using = DateSerializer.class)
	private Date validityEndTime;
	private Integer formId;
	private Integer marketplaceId;

	public ReferencePricePreviousDaysItem() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getValidityStartTime() {
		return validityStartTime;
	}

	public void setValidityStartTime(Date validityStartTime) {
		this.validityStartTime = validityStartTime;
	}

	public Date getValidityEndTime() {
		return validityEndTime;
	}

	public void setValidityEndTime(Date validityEndTime) {
		this.validityEndTime = validityEndTime;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getFormId() {
		return formId;
	}

	public void setFormId(Integer formId) {
		this.formId = formId;
	}

	public Integer getMarketplaceId() {
		return marketplaceId;
	}

	public void setMarketplaceId(Integer marketplaceId) {
		this.marketplaceId = marketplaceId;
	}

}
