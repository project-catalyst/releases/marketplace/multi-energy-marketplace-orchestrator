package eu.catalyst.memo.payload;

import eu.catalyst.memo.global.DateDeSerializer;
import eu.catalyst.memo.global.DateSerializer;

import java.math.BigDecimal;
import java.util.Date;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

public class ClearingPricePreviousDaysItem {

	private BigDecimal price;
	@JsonDeserialize(using = DateDeSerializer.class)
	@JsonSerialize(using = DateSerializer.class)
	private Date startTime;
	@JsonDeserialize(using = DateDeSerializer.class)
	@JsonSerialize(using = DateSerializer.class)
	private Date endTime;
	private Integer formId;
	private Integer marketplaceId;
	private Integer sessionId;

	public ClearingPricePreviousDaysItem() {
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getFormId() {
		return formId;
	}

	public void setFormId(Integer formId) {
		this.formId = formId;
	}

	public Integer getMarketplaceId() {
		return marketplaceId;
	}

	public void setMarketplaceId(Integer marketplaceId) {
		this.marketplaceId = marketplaceId;
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}
}
