package eu.catalyst.memo.payload;

import java.io.Serializable;
import java.util.List;

public class CorrelatedMarketActionsRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	private String constraintType;
	private List<CorrelatedMarketActionsItem> marketActions;

	public String getConstraintType() {
		return constraintType;
	}

	public void setConstraintType(String constraintType) {
		this.constraintType = constraintType;
	}

	public List<CorrelatedMarketActionsItem> getMarketActions() {
		return marketActions;
	}

	public void setMarketActions(List<CorrelatedMarketActionsItem> marketActions) {
		this.marketActions = marketActions;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
