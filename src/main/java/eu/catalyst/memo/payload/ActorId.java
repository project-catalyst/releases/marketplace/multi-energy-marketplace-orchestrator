package eu.catalyst.memo.payload;

import java.io.Serializable;

public class ActorId implements Serializable {
    private static final long serialVersionUID = 1L;


	private Integer actorId;

	public Integer getActorId() {
		return actorId;
	}

	public void setActorId(Integer actorId) {
		this.actorId = actorId;
	}




}
