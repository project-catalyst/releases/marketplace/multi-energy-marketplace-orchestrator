package eu.catalyst.memo.payload;

import java.io.Serializable;

public class ComponentMPCMRegistration implements Serializable {
	private static final long serialVersionUID = 1L;

	private String type;

	private String form;

	private MPCMComponent component;

	public ComponentMPCMRegistration() {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public MPCMComponent getComponent() {
		return component;
	}

	public void setComponent(MPCMComponent component) {
		this.component = component;
	}

}
