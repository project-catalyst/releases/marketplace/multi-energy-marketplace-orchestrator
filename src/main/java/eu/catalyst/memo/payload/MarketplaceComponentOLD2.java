/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.memo.payload;
import java.io.Serializable;
public class MarketplaceComponentOLD2 implements Serializable {
    private static final long serialVersionUID = 1L;
	private Integer id;

	private String type;

	private String form;

	private String component;

	private String url;

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public MarketplaceComponentOLD2() {
	}

	public MarketplaceComponentOLD2(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

}
