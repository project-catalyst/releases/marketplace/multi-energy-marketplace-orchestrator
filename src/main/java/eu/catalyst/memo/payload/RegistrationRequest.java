package eu.catalyst.memo.payload;

import java.util.List;
import java.io.Serializable;
public class RegistrationRequest implements Serializable {
    private static final long serialVersionUID = 1L;


	private String firstName;
	private String lastName;
	private String username;
	private String email;
	private String phone;
	private String vat;
	private String companyName;
	private Integer actorTypeId;
	private List<Integer> marketRegistered;
	private String status;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getActorTypeId() {
		return actorTypeId;
	}

	public void setActorTypeId(Integer actorTypeId) {
		this.actorTypeId = actorTypeId;
	}

	public List<Integer> getMarketRegistered() {
		return marketRegistered;
	}

	public void setMarketRegistered(List<Integer> marketRegistered) {
		this.marketRegistered = marketRegistered;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
