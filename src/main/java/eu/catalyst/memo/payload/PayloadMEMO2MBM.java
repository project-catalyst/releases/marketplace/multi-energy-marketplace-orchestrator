package eu.catalyst.memo.payload;

import java.math.BigDecimal;

import java.util.List;
import java.io.Serializable;
public class PayloadMEMO2MBM implements Serializable {
    private static final long serialVersionUID = 1L;

	public PayloadMEMO2MBM() {
	}

	//private MarketSession marketSession;
	private List<MarketAction> marketActionList;
	private List<MarketActionCounterOffer> marketActionCounterOfferList;

	private String informationBrokerServerUrl;
	private String tokenMEMO;
	private String marketBillingManagerUrl;
	private String tokenMBM;
	private BigDecimal fixedFee;
	private BigDecimal penalty;

//	public MarketSession getMarketSession() {
//		return marketSession;
//	}
//
//	public void setMarketSession(MarketSession marketSession) {
//		this.marketSession = marketSession;
//	}

	public String getInformationBrokerServerUrl() {
		return informationBrokerServerUrl;
	}

	public void setInformationBrokerServerUrl(String informationBrokerServerUrl) {
		this.informationBrokerServerUrl = informationBrokerServerUrl;
	}

	public String getTokenMEMO() {
		return tokenMEMO;
	}

	public void setTokenMEMO(String tokenMEMO) {
		this.tokenMEMO = tokenMEMO;
	}

	public String getTokenMBM() {
		return tokenMBM;
	}

	public void setTokenMBM(String tokenMBM) {
		this.tokenMBM = tokenMBM;
	}

	public BigDecimal getFixedFee() {
		return fixedFee;
	}

	public void setFixedFee(BigDecimal fixedFee) {
		this.fixedFee = fixedFee;
	}

	public BigDecimal getPenalty() {
		return penalty;
	}

	public void setPenalty(BigDecimal penalty) {
		this.penalty = penalty;
	}

	public String getMarketBillingManagerUrl() {
		return marketBillingManagerUrl;
	}

	public void setMarketBillingManagerUrl(String marketBillingManagerUrl) {
		this.marketBillingManagerUrl = marketBillingManagerUrl;
	}

	public List<MarketAction> getMarketActionList() {
		return marketActionList;
	}

	public void setMarketActionList(List<MarketAction> marketActionList) {
		this.marketActionList = marketActionList;
	}

	public List<MarketActionCounterOffer> getMarketActionCounterOfferList() {
		return marketActionCounterOfferList;
	}

	public void setMarketActionCounterOfferList(List<MarketActionCounterOffer> marketActionCounterOfferList) {
		this.marketActionCounterOfferList = marketActionCounterOfferList;
	}

}