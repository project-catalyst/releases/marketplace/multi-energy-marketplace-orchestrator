
package eu.catalyst.memo.payload;
import java.io.Serializable;
public class MarketplaceOLD implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer marketServiceTypeId;
	private Integer dSOId;
	private Integer marketOperatorId;

	public MarketplaceOLD() {
	}

	public MarketplaceOLD(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMarketServiceTypeId() {
		return marketServiceTypeId;
	}

	public void setMarketServiceTypeId(Integer marketServiceTypeId) {
		this.marketServiceTypeId = marketServiceTypeId;
	}

	public Integer getDSOId() {
		return dSOId;
	}

	public void setDSOId(Integer dSOId) {
		this.dSOId = dSOId;
	}

	public Integer getMarketOperatorId() {
		return marketOperatorId;
	}

	public void setMarketOperatorId(Integer marketOperatorId) {
		this.marketOperatorId = marketOperatorId;
	}

}