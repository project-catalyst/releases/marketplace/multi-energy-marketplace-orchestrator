package eu.catalyst.memo.payload;

import java.io.Serializable;
import java.util.List;

public class CorrelatedMarketActionsResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String constraintType;
	private List<CorrelatedMarketActionsItem> marketActions;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getConstraintType() {
		return constraintType;
	}

	public void setConstraintType(String constraintType) {
		this.constraintType = constraintType;
	}

	public List<CorrelatedMarketActionsItem> getMarketActions() {
		return marketActions;
	}

	public void setMarketActions(List<CorrelatedMarketActionsItem> marketActions) {
		this.marketActions = marketActions;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
