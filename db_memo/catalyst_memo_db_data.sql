INSERT INTO public."Constraint" (id, description) VALUES (1, 'ALL OR NOTHING');
INSERT INTO public."Constraint" (id, description) VALUES (2, 'AT LEAST ONE');


INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (11, 'it_load', 'it_load', 'MCM', 'http://192.168.1.108:8021/mcm_it/rest', 'mcm', 'catalystMCM');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (12, 'ancillary-services', 'ancillary-services', 'MCM', 'http://192.168.1.108:8022/mcm_fl/rest', 'mcm', 'catalystMCM');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (1, 'energy', 'electric_energy', 'IB', 'http://192.168.1.108:5000', 'memo', 'catalystMEMO');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (2, 'thermal_energy', 'thermal_energy', 'IB', 'http://192.168.1.108:5001', 'memo', 'catalystMEMO');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (3, 'it_load', 'it_load', 'IB', 'http://192.168.1.108:5002', 'memo', 'catalystMEMO');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (4, 'ancillary-services', 'ancillary-services', 'IB', 'http://192.168.1.108:5003', 'memo', 'catalystMEMO');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (5, 'energy', 'electric_energy', 'MBM', 'http://192.168.1.108:8013/mbm_el/rest', 'mbm', 'catalystMBM');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (6, 'thermal_energy', 'thermal_energy', 'MBM', 'http://192.168.1.108:8014/mbm_th/rest', 'mbm', 'catalystMBM');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (7, 'it_load', 'it_load', 'MBM', 'http://192.168.1.108:8011/mbm_it/rest', 'mbm', 'catalystMBM');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (8, 'ancillary-services', 'ancillary-services', 'MBM', 'http://192.168.1.108:8012/mbm_fl/rest', 'mbm', 'catalystMBM');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (9, 'energy', 'electric_energy', 'MCM', 'http://192.168.1.108:8023/mcm_el/rest', 'mcm', 'catalystMCM');
INSERT INTO public."MarketplaceComponent" (id, type, form, component, url, username, password) VALUES (10, 'thermal_energy', 'thermal_energy', 'MCM', 'http://192.168.1.108:8024/mcm_th/rest', 'mcm', 'catalystMCM');


INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (1, 'dso', 1, 3);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (2, 'dso', 2, 3);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (3, 'dso', 3, 3);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (4, 'dso', 4, 3);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (5, 'admin', 1, 2);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (6, 'admin', 2, 2);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (7, 'admin', 3, 2);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (8, 'admin', 4, 2);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (9, 'participant1', 1, 4);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (10, 'participant1', 2, 4);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (11, 'participant1', 3, 4);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (12, 'participant1', 4, 4);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (13, 'participant2', 1, 6);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (14, 'participant2', 2, 6);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (15, 'participant2', 3, 6);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (16, 'participant2', 4, 6);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (17, 'participant3', 1, 10);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (18, 'participant3', 2, 10);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (19, 'participant3', 3, 10);
INSERT INTO public."MarketplaceParticipant" (id, username, ib_id, actor_id) VALUES (20, 'participant3', 4, 10);


INSERT INTO public."Participant" (id, first_name, last_name, username, email, phone, vat, company_name, actor_type_id, status) VALUES (1, 'Nick', 'Carter', 'dso', 'dso@dso.com', '123456798', '123123123', 'DSO Inc', 2, 'active');
INSERT INTO public."Participant" (id, first_name, last_name, username, email, phone, vat, company_name, actor_type_id, status) VALUES (2, 'John', 'Doe', 'admin', 'operator@localauthority.com', '123456798', '132456798', 'Local Authority', 1, 'active');
INSERT INTO public."Participant" (id, first_name, last_name, username, email, phone, vat, company_name, actor_type_id, status) VALUES (3, 'Mary', 'Poppins', 'participant1', 'generic.participant@dc.com', '123456798', '132456798', 'Data Center', 3, 'active');
INSERT INTO public."Participant" (id, first_name, last_name, username, email, phone, vat, company_name, actor_type_id, status) VALUES (4, 'John', 'Devereux', 'participant2', 'generic.participant@dc.com', '123456798', '132456798', 'Data Center', 3, 'active');



INSERT INTO public."SystemParameter" (id, name, value) VALUES (1, 'FlexSessionsCompleteTime', '23:00');
INSERT INTO public."SystemParameter" (id, name, value) VALUES (2, 'FlexSessionsBillingTime', '23:00');
INSERT INTO public."SystemParameter" (id, name, value) VALUES (3, 'fixedFee', '200.00');
INSERT INTO public."SystemParameter" (id, name, value) VALUES (4, 'penalty', '200.00');


