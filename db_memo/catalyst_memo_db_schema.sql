CREATE TABLE public."MarketplaceComponent" (
  id SERIAL PRIMARY KEY,
  type character varying(50) NOT NULL,
  form character varying(50) NOT NULL,
  component character varying(50) NOT NULL,
  url character varying(50) NOT NULL,
  username character varying(50) NOT NULL,
  password character varying(50) NOT NULL
);

CREATE UNIQUE INDEX ON "MarketplaceComponent"(id);

CREATE TABLE public."Participant" (
  id SERIAL PRIMARY KEY,
  first_name character varying(50) NOT NULL,
  last_name character varying(50) NOT NULL,
  username character varying(50) NOT NULL,
  email character varying(50) NOT NULL,
  phone character varying(20) NOT NULL,
  vat character varying(20) NOT NULL,
  company_name character varying(50) NOT NULL,
  actor_type_id integer NOT NULL,
  status character varying(20) NOT NULL
);

CREATE UNIQUE INDEX ON "Participant"(id);

CREATE TABLE public."MarketplaceParticipant" (
  id SERIAL PRIMARY KEY,
  username character varying(50) NOT NULL,
  ib_id integer NOT NULL,
  actor_id integer NOT NULL
);

CREATE UNIQUE INDEX ON "MarketplaceParticipant"(id);

CREATE TABLE public."Constraint" (
  id SERIAL PRIMARY KEY,
  description character varying(50) NOT NULL
);

CREATE UNIQUE INDEX ON "Constraint"(id);

CREATE TABLE public."Correlation" (
  id SERIAL PRIMARY KEY,
  constraint_id integer NOT NULL,
  timeframe character varying(20) NOT NULL,
  timestamp bigint NOT NULL
);

CREATE UNIQUE INDEX ON "Correlation"(id);

CREATE TABLE public."MarketactionCorrelation" (
  id SERIAL PRIMARY KEY,
  username character varying(50) NOT NULL,
  information_broker_id integer NOT NULL,
  session_id integer NOT NULL,
  action_id integer NOT NULL,
  correlation_id integer NOT NULL
);

CREATE UNIQUE INDEX ON "MarketactionCorrelation"(id);

CREATE TABLE public."SystemParameter" (
  id SERIAL PRIMARY KEY,
  name character varying(50) NOT NULL,
  value character varying(50) NOT NULL
);

CREATE UNIQUE INDEX ON "SystemParameter"(id);
